#!/usr/bin/env bash

# Generate a watermarked montage of a PDF document's first 9 pages.
#
# Requires convert, montage, composite:
# `brew install imagemagick`
#
# Usage: pdf-cover <input> <watermark_text:SAMPLE> <output_file_name:input{.png}> [-h]
#
#   options:
#       -h, --help           Print help message.
#
#   examples:
#       pdf-cover document.pdf
#       pdf-cover document.pdf "Copyright"
#       pdf-cover document.pdf "Copyright" "cover_image"

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  # shellcheck disable=2088
  args+=(
    "$(basename "$0")"
    "Generate a watermarked montage of a PDF document's first 9 pages."
    "convert, montage, composite: brew install imagemagick"
    "<input> <watermark_text:SAMPLE> <output_file_name:input{.png}>"
    "$short_options"
    "$long_options"
    "Print help message."
    'document.pdf;document.pdf "Copyright";document.pdf "Copyright" "cover_image"'
  )

  generate_help "${args[@]}"
}

short_options="h"
long_options="--help"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

if ! command -v convert &> /dev/null; then
  print-error "convert must be installed to use pdf-cover:" \
    "brew install imagemagick"
  exit 1
elif ! command -v montage &> /dev/null; then
  print-error "montage must be installed to use pdf-cover:" \
    "brew install imagemagick"
  exit 1
elif ! command -v magick &> /dev/null; then
  print-error "composite must be installed to use pdf-cover:" \
    "brew install imagemagick"
  exit 1
elif [[ $# -lt 1 || $# -gt 3 ]]; then
  print-error "1-3 arguments required."
  help_message
  exit 1
fi

# BEGIN shift options {{{
# Split short and long options into arrays
mapfile -t short_opts < <(echo "$short_options" | fold -w1)
IFS=' ' read -r -a long_opts <<< "$long_options"

# Create an associative array to map short options to long ones
declare -A option_map
for i in "${!short_opts[@]}"; do
  short_opt="${short_opts[i]}"
  long_opt="${long_opts[i]}"

  option_map["$long_opt"]=$short_opt
done

# Transform long options to short ones dynamically using option_map
transformed_args=()
for arg in "$@"; do
  if [[ ${option_map["$arg"]} ]]; then
    # Long option: map to short option
    transformed_args+=("-${option_map["$arg"]}")
  else
    # Positional arguments and short options remain unchanged
    transformed_args+=("$arg")
  fi
done

# Set the positional parameters to the transformed arguments
set -- "${transformed_args[@]}"

# Dynamically set OPTIND based on the number of options passed
OPTIND=1 # Reset OPTIND to start getopts from the first argument
# shellcheck disable=2034
while getopts ":$short_options" opt; do
  # Process each option (do nothing here, let getopts process them)
  :
done

# Shift all processed options, leaving only the positional arguments
shift "$((OPTIND - 1))"
# END shift options }}}

input="$1"
input_file="$(basename "$input")"
input_file_name="${input_file%.*}"
output_file_extension="png"
output_file_name="${3:-"${input_file_name}"}"
output_file="$output_file_name.$output_file_extension"
watermark_text="${2:-"SAMPLE"}"
watermark_size=180

# TODO: Rotate & resize or repage in case of landscape pages.
# convert thumbnail.png -rotate -90 thumbnail.png
# resize thumbnail.png 140 100 thumbnail

convert -thumbnail x200 -background white -alpha remove "$input"[{0..8}] miff:- |
  montage - -geometry +0+0 miff:- |
  convert - -fill white -colorize 30% "$output_file"

convert -size ${watermark_size}x${watermark_size} \
  -density ${watermark_size}x${watermark_size} \
  -background transparent \
  -gravity center \
  -pointsize 16 \
  -fill "#00000070" \
  -font "Helvetica-Bold" \
  label:"$watermark_text" \
  -rotate -45 +repage \
  -crop ${watermark_size}x${watermark_size}+0+0 +repage miff:- |
  composite -tile - "$output_file" "$output_file"
