#!/usr/bin/env bash

function set_up_before_script() {
  mock tput echo
}

function test_no_arguments() {
  assert_contains ':: EMPTY ::' "$(cl)"
}

function test_starts_with_line() {
  assert_string_starts_with '─' "$(cl)"
}

function test_ends_with_line() {
  assert_string_ends_with '─' "$(cl)"
}

function test_has_3_lines_with_no_values() {
  # NOTE: 5 lines because mocking tput in this way will output a newline
  assert_line_count 5 "$(cl)"
}

# data_provider provider_value
function test_prints_only_value() {
  assert_contains ':: value' "$(cl "$1")"
}

# data_provider provider_label_value
function test_prints_value_and_label() {
  local label="$1" value="${*:2}" result
  result="$(cl "$label" "$value")"

  assert_contains ':: label :: value' "$result"
}

# data_provider provider_label_value
function test_snapshot_value_and_label() {
  local label="$1" value="${*:2}" result
  result="$(cl "$label" "$value")"

  assert_match_snapshot "$result"
}

# data_provider provider_array
function test_prints_array() {
  assert_contains ':: foo bar baz' "$(cl "$*")"
}

# data_provider provider_label_array
function test_prints_array_and_label() {
  local label="$1" array="${*:2}" result
  result="$(cl "$label" "$array")"

  assert_contains ':: label :: foo bar baz' "$result"
}

# data_provider provider_label_array
function test_has_3_lines_with_values() {
  local label="$1" array="${*:2}" result
  result="$(cl "$label" "$array")"

  # NOTE: 5 lines because mocking tput in this way will output a newline
  assert_line_count 5 "$result"
}

# data_provider provider_label_array
function test_snapshot_array_and_label() {
  local label="$1" array="${*:2}" result
  result="$(cl "$label" "$array")"

  assert_match_snapshot "$result"
}

function provider_value() {
  echo 'value'
}

function provider_label_value() {
  echo 'label' 'value'
}

function provider_array() {
  local array=('foo' 'bar' 'baz')
  echo "${array[*]}"
}

function provider_label_array() {
  local array=('foo' 'bar' 'baz')
  echo 'label' "${array[*]}"
}
