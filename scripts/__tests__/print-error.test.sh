#!/usr/bin/env bash

function set_up_before_script() {
  mock tput echo
}

function test_no_arguments_error_code() {
  assert_general_error "$(print-error)"
}

function test_no_arguments_message() {
  assert_contains 'Usage:' "$(print-error)"
}

function test_short_options_help() {
  assert_contains 'Usage:' "$(print-error -h)"
}

function test_long_options_help() {
  assert_contains 'Usage:' "$(print-error --help)"
}

# data_provider provider_message
function test_message() {
  local message="$1" result
  result="$(print-error "$message" 2>&1)"

  assert_string_starts_with 'Error Missing!' "$result"
}

# data_provider provider_message
function test_message_line_count() {
  local message="$1" result
  result="$(print-error "$message" 2>&1)"

  assert_line_count 1 "$result"
}

# data_provider provider_message
function test_snapshot_with_message() {
  local message="$1" result
  result="$(print-error "$message" 2>&1)"

  assert_match_snapshot "$result"
}

# data_provider provider_message_details
function test_message_and_details() {
  local message="$1" details="${*:2}" result
  result="$(print-error "$message" "$details" 2>&1)"

  assert_string_ends_with '
    brew install package' "$result"
}

# data_provider provider_message_details
function test_message_and_details_line_count() {
  local message="$1" details="${*:2}" result
  result="$(print-error "$message" "$details" 2>&1)"

  assert_line_count 3 "$result"
}

# data_provider provider_message_details
function test_snapshot_with_message_and_details() {
  local message="$1" details="${*:2}" result
  result="$(print-error "$message" "$details" 2>&1)"

  assert_match_snapshot "$result"
}

function provider_message() {
  echo 'Missing!'
}

function provider_message_details() {
  echo 'Missing!' 'brew install package'
}
