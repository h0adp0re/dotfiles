#!/usr/bin/env bash

function test_no_arguments_error_code() {
  assert_general_error "$(repeat_text)"
}

function test_no_arguments_message() {
  assert_contains 'Usage:' "$(repeat_text)"
}

function test_short_options_help() {
  assert_contains 'Usage:' "$(repeat_text -h)"
}

function test_long_options_help() {
  assert_contains 'Usage:' "$(repeat_text --help)"
}

function test_repeats_five_times() {
  assert_matches 'XXXXX' "$(repeat_text 'X' 5)"
}
