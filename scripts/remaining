#!/usr/bin/env bash

# Print remaining time to track based on already tracked time.
#
# Usage: remaining [-hsw]
#
#   options:
#       -h, --help           Print help message.
#       -s, --seconds        Print time in seconds.
#       -w, --week           Use 40h in calculation instead of 8h.

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  args+=(
    "$(basename "$0")"
    "Print remaining time to track based on already tracked time."
    ""
    ""
    "$short_options"
    "$long_options"
    "Print help message.;Print time in seconds.;Use 40h in calculation instead of 8h."
  )

  generate_help "${args[@]}"
}

short_options="hsw"
long_options="--help --seconds --week"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help print_seconds use_week <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

eight_hours=28800

if $use_week; then
  meetings=$(meetings -sw)
  tracked=$(tracked -sw)
  hours=$((eight_hours * 5))
else
  meetings=$(meetings -s)
  tracked=$(tracked -s)
  hours=$eight_hours

  # shellcheck disable=2091
  if $(weekend); then
    hours=0
  fi
fi

result=$((hours - meetings - tracked))

if $print_seconds; then
  echo "$result"
else
  print-message \
    --check \
    --no-newlines \
    "Remaining:" \
    "$(hms "$result")"
fi
