#!/usr/bin/env bash

# Print a colorful feedback message.
#
# Usage: print-message <message> <highlighted_message> <details>(optional) [-hiSucselt]
#
#   options:
#       -h, --help                  Print help message.
#       -i, --installed             ✔ prefix & bold green highlights.
#       -S, --success               ✔ prefix & green highlights.
#       -u, --update                → prefix & yellow highlights.
#       -c, --check                 ↴ prefix & blue highlights.
#       -s, --skip                  ← prefix & yellow highlights.
#       -e, --error                 ✘ prefix & red highlights.
#       -l, --no-newline-leading    Don't print leading newline.
#       -t, --no-newline-trailing   Don't print trailing newline.
#       -n, --no-newlines           Don't print either newline.
#
#   examples:
#       print-message --update "Updating" "plugins"
#       print-message --error "Missing" "package" "brew install package"

Normal="$(tput sgr0)"
Bold="$(tput bold)"
Red="$(tput setaf 1)"
Green="$(tput setaf 2)"
Yellow="$(tput setaf 3)"
Blue="$(tput setaf 4)"
BGreen="$Bold$Green"

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  args+=(
    "$(basename "$0")"
    "Print a colorful feedback message."
    ""
    "<message> <highlighted_message> <details>(optional)"
    "$short_options"
    "$long_options"
    "Print help message.;✔ prefix & bold green highlights.;✔ prefix & green highlights.;→ prefix & yellow highlights.;↴ prefix & blue highlights.;← prefix & yellow highlights.;✘ prefix & red highlights.;Don't print leading newline.;Don't print trailing newline.;Don't print either newline."
    '--update "Updating" "plugins";--success "Wrote" "backup-file.json";--error "Missing" "package" "brew install package"'
  )

  generate_help "${args[@]}"
}

short_options="hiSucseltn"
long_options="--help --installed --success --update --check --skip --error --no-newline-leading --no-newline-trailing --no-newlines"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help installed success update check skip error no_newline_leading no_newline_trailing no_newlines <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

if [[ $# -eq 0 ]]; then
  print-error "No arguments provided"
  help_message
  exit 1
fi

# BEGIN shift options {{{
# Split short and long options into arrays
mapfile -t short_opts < <(echo "$short_options" | fold -w1)
IFS=' ' read -r -a long_opts <<< "$long_options"

# Create an associative array to map short options to long ones
declare -A option_map
for i in "${!short_opts[@]}"; do
  short_opt="${short_opts[i]}"
  long_opt="${long_opts[i]}"

  option_map["$long_opt"]=$short_opt
done

# Transform long options to short ones dynamically using option_map
transformed_args=()
for arg in "$@"; do
  if [[ ${option_map["$arg"]} ]]; then
    # Long option: map to short option
    transformed_args+=("-${option_map["$arg"]}")
  else
    # Positional arguments and short options remain unchanged
    transformed_args+=("$arg")
  fi
done

# Set the positional parameters to the transformed arguments
set -- "${transformed_args[@]}"

# Dynamically set OPTIND based on the number of options passed
OPTIND=1 # Reset OPTIND to start getopts from the first argument
# shellcheck disable=2034
while getopts ":$short_options" opt; do
  # Process each option (do nothing here, let getopts process them)
  :
done

# Shift all processed options, leaving only the positional arguments
shift "$((OPTIND - 1))"
# END shift options }}}

message="$1"
highlighted_message="$2"
details="$3"

if $installed; then
  icon="✔"
  highlight="$BGreen"
elif $success; then
  icon="✔"
  highlight="$Green"
elif $update; then
  icon="→"
  highlight="$Yellow"
elif $check; then
  icon="↴"
  highlight="$Blue"
elif $skip; then
  icon="←"
  highlight="$Yellow"
elif $error; then
  icon="✘"
  highlight="$Red"
fi

if $no_newlines; then
  no_newline_leading=true
  no_newline_trailing=true
fi

# Print the result, handling newlines and message formatting
if ! $no_newline_leading; then
  echo
fi

# Print the message and highlighted_message
echo -e "$Normal$highlight$icon $Normal$Bold$message $Normal$highlight$highlighted_message$Normal"

if ! $no_newline_trailing; then
  echo
fi

# Print details if available
if [[ -n $details ]]; then
  echo -e "  $Normal$Yellow$details$Normal"
fi
