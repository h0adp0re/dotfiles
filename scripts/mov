#!/usr/bin/env bash

# Convert .mov to .mp4.
# https://superuser.com/a/1555285
#
# Requires ffmpeg:
# `brew install ffmpeg`
#
# Usage: mov <input> <output_file_name:input{.mp4}> [-h]
#
#   options:
#       -h, --help           Print help message.
#
#   examples:
#       mov input.mov
#       mov input.mov output_file_name

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  args+=(
    "$(basename "$0")"
    "Convert .mov to .mp4."
    "ffmpeg: brew install ffmpeg"
    "<input> <output_file_name:input{.mp4}>"
    "$short_options"
    "$long_options"
    "Print help message."
    "input.mov;input.mov output_file_name"
  )

  generate_help "${args[@]}"
}

short_options="h"
long_options="--help"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

if ! command -v ffmpeg &> /dev/null; then
  print-error "ffmpeg must be installed to use mov:" \
    "brew install ffmpeg"
  exit 1
elif [[ ($# -lt 1 || $# -gt 2) && $1 != '-h' && $1 != '--help' ]]; then
  print-error "1-2 arguments required."
  help_message
  exit 1
fi

input="$1"
input_file_directory="$(dirname "$input")"
input_file="$(basename "$input")"
input_file_name="${input_file%.*}"
input_file_extension="${input_file##*.}"
output_file_extension="mp4"
output_file_name="${2:-"${input_file_name}"}"
output_file="$output_file_name.$output_file_extension"
output="$input_file_directory/$output_file"

if [[ $input_file_extension != "mov" ]]; then
  print-message --error "Input file needs to be" ".mov"
else
  ffmpeg -v warning -stats -i "$input" -c:v libx265 -c:a aac -vf format=yuv420p -movflags +faststart "$output"
fi
