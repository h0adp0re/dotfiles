#!/usr/bin/env bash

# Convert video to another format.
# https://superuser.com/a/1555285
#
# Requires ffmpeg:
# `brew install ffmpeg`
#
# Usage: conv <input> <output_file_extension> [-h]
#
#   options:
#       -h, --help           Print help message.
#
#   examples:
#       conv input.mov mp4
#       conv input.mp4 mkv

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  args+=(
    "$(basename "$0")"
    "Convert video to another format."
    "ffmpeg: brew install ffmpeg"
    "<input> <output_file_extension>"
    "$short_options"
    "$long_options"
    "Print help message."
    "input.mov mp4;input.mp4 mkv"
  )

  generate_help "${args[@]}"
}

short_options="h"
long_options="--help"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

if ! command -v ffmpeg &> /dev/null; then
  print-error "ffmpeg must be installed to use conv:" \
    "brew install ffmpeg"
  exit 1
elif [[ $# -ne 2 && $1 != '-h' && $1 != '--help' ]]; then
  print-error "2 arguments required."
  help_message
  exit 1
fi

input="$1"
input_file_directory="$(dirname "$input")"
input_file="$(basename "$input")"
input_file_name="${input_file%.*}"
input_file_extension="${input_file#*.}"
output_file_extension="$2"
output_file="$input_file_name.$output_file_extension"
output="$input_file_directory/$output_file"
list=(
  "mkv"
  "mp4"
)

if [[ $(array-includes "$input_file_extension" "${list[*]}") && $(array-includes "$output_file_extension" "${list[*]}") ]]; then
  ffmpeg -v warning -stats -i "$input" -vcodec copy -acodec copy -scodec mov_text -movflags +faststart "$output"
elif [[ $input_file_extension == "mkv" && $output_file_extension == "mov" ]]; then
  ffmpeg -i "$input" -c:v prores_ks -profile:v 3 -c:a pcm_s24le "$output"
else
  ffmpeg -v warning -stats -i "$input" -c:v libx265 -c:a aac -vf format=yuv420p -movflags +faststart "$output"
fi
