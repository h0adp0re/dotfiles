#!/usr/bin/env bash

# Resize an image.
#
# Requires magick:
# `brew install imagemagick`
#
# Usage: resize <input> <width:800> <quality:90> <output_file_name:input{_RESIZED}> [-h]
#
#   options:
#       -h, --help           Print help message.
#
#   examples:
#       resize input.png
#       resize input.png 800
#       resize input.png 800 50
#       resize input.png 800 50 output.png

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  args+=(
    "$(basename "$0")"
    "Resize an image."
    "magick: brew install imagemagick"
    "<input> <width:800> <quality:90> <output_file_name:input{_RESIZED}>"
    "$short_options"
    "$long_options"
    "Print help message."
    "input.png;input.png 800;input.png 800 50;input.png 800 50 output.png"
  )

  generate_help "${args[@]}"
}

short_options="h"
long_options="--help"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

if ! command -v magick &> /dev/null; then
  print-error "magick must be installed to use resize:" \
    "brew install imagemagick"
  exit 1
elif [[ ($# -lt 1 || $# -gt 4) && $1 != '-h' && $1 != '--help' ]]; then
  print-error "1-4 arguments required."
  help_message
  exit 1
fi

input="$1"
input_file_directory="$(dirname "$input")"
input_file="$(basename "$input")"
input_file_name="${input_file%.*}"
input_file_extension="${input_file##*.}"
output_file_name="${4:-"${input_file_name}_RESIZED"}"
output_file="$output_file_name.$input_file_extension"
output="$input_file_directory/$output_file"
width="${2:-800}"
quality="${3:-90}"

magick "$input" -quality "$quality%" -resize "$width" "$output"
