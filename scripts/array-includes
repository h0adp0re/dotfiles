#!/usr/bin/env bash

# Check whether an array contains a string.
#
# Usage: array-includes <string> <expanded_array> [-h]
#
#   options:
#       -h, --help           Print help message.
#
#   examples:
#       array-includes "query" "${array[*]}"

declare -a array

function help_message() {
  local args=()

  if [ -n "$1" ]; then
    args+=("$1")
  fi

  # shellcheck disable=2016
  args+=(
    "$(basename "$0")"
    "Check whether an array contains a string."
    ""
    "<string> <expanded_array>"
    "$short_options"
    "$long_options"
    "Print help message."
    '"query" "${array[*]}"'
  )

  generate_help "${args[@]}"
}

short_options="h"
long_options="--help"

# Capture options and error status
if ! options=$(parse_options "$short_options" "$long_options" -- "$@"); then
  help_message
  exit 1
fi

# Read the captured options
read -r print_help <<< "$options"

if $print_help; then
  help_message --description
  exit
fi

if [[ $# -lt 2 ]]; then
  print-error "No arguments provided"
  help_message
  exit 1
fi

query=$1
shift

array=("$@")

echo "${array[*]}" | tr ' ' '\n' | grep -Fx "$query"
