#!/usr/bin/env bash

# Colors
# Inspired by https://gist.github.com/vratiu/9780109

# The ANSI/VT100 terminals and terminal emulators can display colors
# and formatted text using escape sequences. Instead of using the raw
# ANSI escape sequences, we use the `tput` command which ensures
# portability between various terminals and terminal emulators
# by looking up escape sequences in the terminfo database.

# Ignore unused variables in this file
# shellcheck disable=SC2034

# General
Normal="$(tput sgr0)"     # Normal text
Bold="$(tput bold)"       # Start bold
Italic="$(tput smso)"     # Start standout (italic)
SItalic="$(tput rmso)"    # Stop standout (italic)
Underline="$(tput smul)"  # Start underline
SUnderline="$(tput rmul)" # Stop underline
Inverted="$(tput rev)"    # Inverted

# Regular Foreground Colors
Black="$(tput setaf 0)"
Red="$(tput setaf 1)"
Green="$(tput setaf 2)"
Yellow="$(tput setaf 3)"
Blue="$(tput setaf 4)"
Purple="$(tput setaf 5)"
Cyan="$(tput setaf 6)"
White="$(tput setaf 7)"

# Bold Foreground Colors
BBlack="${Bold}${Black}"
BRed="${Bold}${Red}"
BGreen="${Bold}${Green}"
BYellow="${Bold}${Yellow}"
BBlue="${Bold}${Blue}"
BPurple="${Bold}${Purple}"
BCyan="${Bold}${Cyan}"
BWhite="${Bold}${White}"

# Background Colors
On_Black="$(tput setab 0)"
On_Red="$(tput setab 1)"
On_Green="$(tput setab 2)"
On_Yellow="$(tput setab 3)"
On_Blue="$(tput setab 4)"
On_Purple="$(tput setab 5)"
On_Cyan="$(tput setab 6)"
On_White="$(tput setab 7)"

# Bright Foreground Colors
IBlack="$(tput setaf 8)"
IRed="$(tput setaf 9)"
IGreen="$(tput setaf 10)"
IYellow="$(tput setaf 11)"
IBlue="$(tput setaf 12)"
IPurple="$(tput setaf 13)"
ICyan="$(tput setaf 14)"
IWhite="$(tput setaf 15)"

# Bold Bright Foreground Colors
BIBlack="${Bold}${IBlack}"
BIRed="${Bold}${IRed}"
BIGreen="${Bold}${IGreen}"
BIYellow="${Bold}${IYellow}"
BIBlue="${Bold}${IBlue}"
BIPurple="${Bold}${IPurple}"
BICyan="${Bold}${ICyan}"
BIWhite="${Bold}${IWhite}"

# Bright Background Colors
On_IBlack="$(tput setab 8)"
On_IRed="$(tput setab 9)"
On_IGreen="$(tput setab 10)"
On_IYellow="$(tput setab 11)"
On_IBlue="$(tput setab 12)"
On_IPurple="$(tput setab 13)"
On_ICyan="$(tput setab 14)"
On_IWhite="$(tput setab 15)"

# Check whether a line exists in a file
_lineExistsInFile() {
  local line file

  line="$1"
  file="$2"

  if grep -qxF "$line" "$file"; then
    return 0
  else
    return 1
  fi
}

# Print a message prompting the user for input.
_promptMessage() {
  local YES_NO="${Normal}(${BGreen}Y${Normal}/${BRed}n${Normal})"
  local withoutYesNo=false
  local simple=false

  # Transform long options to short ones.
  for arg in "$@"; do
    shift
    case "$arg" in
      "--withoutYesNo") set -- "$@" "-w" ;;
      "--simple") set -- "$@" "-s" ;;
      *) set -- "$@" "$arg" ;;
    esac
  done

  # Parse short options.
  OPTIND=1
  while getopts "ws" opt; do
    case "$opt" in
      "w") withoutYesNo=true ;;
      "s") simple=true ;;
      *) break ;;
    esac
  done
  shift "$((OPTIND - 1))" # remove options from positional parameters

  if $withoutYesNo; then
    echo -ne "\n${Bold}$1${Normal}${Purple}$2${Normal}${Bold}$3${Normal} "
  elif $simple; then
    echo -ne "\n${Bold}$1 ${YES_NO} "
  else
    echo -ne "\n${Bold}$1${Normal}${Purple}$2${Normal}${Bold}$3 ${YES_NO} "
  fi
}

# Print a message prompting the user for correct input.
_rePrompt() {
  local singleWord=false

  # Transform long options to short ones.
  for arg in "$@"; do
    shift
    case "$arg" in
      "--singleWord") set -- "$@" "-s" ;;
      *) set -- "$@" "$arg" ;;
    esac
  done

  # Parse short options.
  OPTIND=1
  while getopts "s" opt; do
    case "$opt" in
      "s") singleWord=true ;;
      *) break ;;
    esac
  done
  shift "$((OPTIND - 1))" # remove options from positional parameters

  if $singleWord; then
    echo -ne "Please write ${Bold}$1${Normal}: "
  else
    echo -ne "Please answer (y)es or (n)o: "
  fi
}

# Print a message after a successful operation.
_statusMessage() {
  local bootstrap=false
  local finish=false
  local error=false
  local skip=false

  # Transform long options to short ones.
  for arg in "$@"; do
    shift
    case "$arg" in
      "--bootstrap") set -- "$@" "-b" ;;
      "--finish") set -- "$@" "-f" ;;
      "--error") set -- "$@" "-e" ;;
      "--skip") set -- "$@" "-s" ;;
      *) set -- "$@" "$arg" ;;
    esac
  done

  # Parse short options.
  OPTIND=1
  while getopts "bfes" opt; do
    case "$opt" in
      "b") bootstrap=true ;;
      "f") finish=true ;;
      "e") error=true ;;
      "s") skip=true ;;
      *) break ;;
    esac
  done
  shift "$((OPTIND - 1))" # remove options from positional parameters

  if $bootstrap; then
    echo -e "${BBlue}==> ${BGreen}$1 ${Normal}${Bold}setup successful!${Normal}"
  elif $finish; then
    echo -e "${Green}✔ ${Normal}${Bold}$1${Normal} ✨"
  elif $error; then
    echo -e "${Red}✘ ${Normal}$1 ${Red}$2${Normal}$3"
  elif $skip; then
    echo -e "${Yellow}← ${Normal}$1 ${Yellow}$2${Normal}$3"
  else
    echo -e "$1${Green}✔ ${Normal}$2 ${Green}$3${Normal}$4"
  fi
}

# Ask for the administrator password upfront.
_askForSudo() {
  if sudo -nv &> /dev/null; then
    echo
    _statusMessage --skip "Using cached" "sudo credentials" "."
  else
    _promptMessage --withoutYesNo "Enter your " "password" " so the script can run uninterrupted."
    sudo -v
  fi
}

# Keep-alive: update existing `sudo` time stamp until the script has finished.
_keepSudoAlive() {
  while true; do
    sudo -n true
    sleep 60
    kill -0 "$$" || exit
  done 2> /dev/null &
}

_preventSleeping() {
  if [[ $OSTYPE == "darwin"* ]]; then
    # Prevent sleeping during script execution, as long as the machine is on AC power.
    # TODO: caffeinate hasn't worked on macOS since at least 10.5. Find a replacement
    caffeinate -s -w "$$" &
  fi
}

# Give the user an opportunity to cancel the script if their user belongs to the "admin" group.
_checkMacAdmin() {
  if [[ $OSTYPE == "darwin"* ]]; then
    local isAdmin=false

    if groups "$(whoami)" | grep -q -w "admin"; then
      isAdmin=true
    fi

    while $isAdmin; do
      _promptMessage "You're running this script as an " "admin" " user.\n\nNormally this should not happen. Did you do this intentionally?"
      read -r yn
      case $yn in
        [Yy]*) break ;;
        [Nn]*)
          echo -e "\nPlease ${Green}exit${Normal} the admin user and try again."
          exit 1
          ;;
        *) _rePrompt ;;
      esac
    done
  fi
}

# Get computer name.
_getComputerName() {
  if [[ $OSTYPE == "darwin"* ]]; then
    sudo scutil --get ComputerName
  fi
}

# Set computer name (as done via System Preferences > Sharing).
_setComputerName() {
  if [[ $OSTYPE == "darwin"* ]]; then
    sudo scutil --set ComputerName "$1"
    sudo scutil --set HostName "$1"
    sudo scutil --set LocalHostName "$1"
    sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string "$1"
  fi
}

# Prompt the user to change the computer name.
_promptChangingComputerName() {
  if [[ $OSTYPE == "darwin"* ]]; then
    local oldName
    oldName="$(_getComputerName)"

    _promptMessage "This computer's name is currently " "$oldName" ". Do you wish to change it?"
    while read -r yn; do
      case $yn in
        [Yy]*)
          read -rp "New name: " computerName
          _setComputerName "$computerName"
          _statusMessage "\n" "Set computer name to" "${computerName}" "."
          break
          ;;
        [Nn]*)
          echo
          _statusMessage --skip "Keeping the name" "$oldName" "."
          break
          ;;
        *) _rePrompt ;;
      esac
    done
  fi
}

# Prompt for reboot.
_promptReboot() {
  if [[ $OSTYPE == "darwin"* ]]; then
    _promptMessage "Some of these changes require a " "restart" " to take effect. Do you wish to restart now?"
    while read -r yn; do
      case $yn in
        [Yy]*)
          sudo shutdown -r now
          break
          ;;
        [Nn]*) break ;;
        *) _rePrompt ;;
      esac
    done
  fi
}

# Ask the user to log in to the mac App Store.
_promptAppStoreLogin() {
  if [[ $OSTYPE == "darwin"* ]]; then
    _promptMessage --withoutYesNo "Sign in to the App Store. Type " "done" " when you've done that:"
    sleep 2 &&
      open '/System/Applications/App Store.app'
    while read -r confirmation; do
      case $confirmation in
        [dD][oO][nN][eE]) break ;;
        *) _rePrompt --singleWord "done" ;;
      esac
    done
  fi
}
