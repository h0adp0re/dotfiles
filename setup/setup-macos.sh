#!/usr/bin/env bash

# More settings can be found here:
# https://macos-defaults.com

# Shellcheck doesn't understand non-constant source.
# Ignore undefined variables.
# shellcheck disable=SC1091,SC2154

source "$HOME/.dotfiles/setup/set-variables.sh"
declare _DOTFILES_LOCAL_WORK_COMPUTER

_promptWorkComputer() {
  _promptMessage --simple "Is this a work computer?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_LOCAL_WORK_COMPUTER=true
        break
        ;;
      [Nn]*)
        _DOTFILES_LOCAL_WORK_COMPUTER=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done

  if ! $_DOTFILES_LOCAL_WORK_COMPUTER; then
    _promptChangingComputerName
  fi
}

_setVariables() {
  if [[ $_DOTFILES_SETUP_BOOTSTRAP == true ]]; then
    _DOTFILES_LOCAL_WORK_COMPUTER="$_DOTFILES_SETUP_WORK_COMPUTER"
  else
    _promptWorkComputer
  fi
}

# Copy mac-specific settings.
_copyMacConfigFiles() {
  # Folder actions
  cp -r "$HOME/.dotfiles/mac/folderactions/." "$HOME/Library/Workflows/Applications/Folder Actions"

  _statusMessage "" "Copied" "macOS config files" "."
}

#######################################################################
# Notifications                                                       #
#######################################################################
_notificationSettings() {
  # System Preferences > Notifications > Show previews: When Unlocked
  defaults write com.apple.ncprefs content_visibility -int 2

  _statusMessage "" "Changed" "Notifications" " settings."
}

#######################################################################
# General                                                             #
#######################################################################
_generalSettings() {
  # System Preferences > Appearance > Appearance: Dark
  # Requires logout, prefer AppleScript.
  # defaults write NSGlobalDomain AppleInterfaceStyle -string "Dark"
  osascript -e 'tell application "System Events" to tell appearance preferences to set dark mode to 1'

  # System Preferences > Appearance > Sidebar icon size: Medium
  defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 2

  # System Preferences > Appearance > Show scroll bars: When scrolling
  defaults write NSGlobalDomain AppleShowScrollBars -string "WhenScrolling"

  # Display an "Open Recent" menu item in the "File" menu
  defaults write NSGlobalDomain NSRecentDocumentsLimit -int 0

  # Disable Siri
  defaults write com.apple.assistant.support "Assistant Enabled" -int 0

  _statusMessage "" "Changed" "General" " settings."
}

#######################################################################
# Accessibility                                                       #
#######################################################################
_accessibilitySettings() {
  # Show toolbar button shapes: on
  defaults write com.apple.universalaccess showToolbarButtonShapes -int 1

  # Shake mouse pointer to locate: off
  defaults write NSGlobalDomain CGDisableCursorLocationMagnification -int 1

  _statusMessage "" "Changed" "Accessibility" " settings."
}

#######################################################################
# Dock                                                                #
#######################################################################
_dockSettings() {
  # System Preferences > Desktop & Dock > Size:
  defaults write com.apple.dock tilesize -int 50

  # System Preferences > Desktop & Dock > Magnification:
  defaults write com.apple.dock magnification -int 0

  # Lock icon size.
  defaults write com.apple.dock size-immutable -int 1

  # System Preferences > Desktop & Dock > Minimize windows using: Scale effect
  defaults write com.apple.dock mineffect -string "scale"

  # System Preferences > Desktop & Dock > Automatically hide and show the Dock:
  defaults write com.apple.dock autohide -int 1

  # Remove the Dock autohide animation.
  defaults write com.apple.dock autohide-time-modifier -int 0

  # Remove the autohide delay so the Dock appears instantly.
  defaults write com.apple.dock autohide-delay -float 0

  # System Preferences > Desktop & Dock > Animate opening applications:
  defaults write com.apple.dock launchanim -int 0

  # System Preferences > Desktop & Dock > Show suggested and recent apps in Dock:
  defaults write com.apple.dock show-recents -int 0

  # Dim hidden apps.
  defaults write com.apple.dock showhidden -bool true

  # Wipe all (default) app icons from the Dock.
  # This is only really useful when setting up a new Mac, or if you don’t use
  # the Dock to launch apps.
  defaults write com.apple.dock persistent-apps -array

  # Write only desired items to the Dock.
  dockutil --section apps --no-restart --add '/System/Applications/Music.app' 1> /dev/null
  dockutil --section apps --no-restart --add '/Applications/Pocket Casts.app' 1> /dev/null
  dockutil --section apps --no-restart --add '/Applications/Firefox.app' 1> /dev/null
  dockutil --section apps --no-restart --add '/System/Volumes/Preboot/Cryptexes/App/System/Applications/Safari.app' 1> /dev/null
  dockutil --section apps --no-restart --add '' --type small-spacer 1> /dev/null
  dockutil --section apps --no-restart --add '/Applications/Alacritty.app' 1> /dev/null
  dockutil --section apps --no-restart --add '/Applications/Firefox Developer Edition.app' 1> /dev/null
  dockutil --section apps --no-restart --add '/Applications/Google Chrome.app' 1> /dev/null
  dockutil --section apps --no-restart --add '' --type small-spacer 1> /dev/null
  # shellcheck disable=2088
  dockutil --add '~/Downloads' --view list --display folder --replacing 'Downloads' 1> /dev/null

  _statusMessage "" "Changed" "Dock" " settings."
}

#######################################################################
# Menu Bar                                                            #
#######################################################################
_menuBarSettings() {
  # Set the timezone; see `sudo systemsetup -listtimezones` for other values
  sudo systemsetup -settimezone "Europe/Tallinn" &> /dev/null

  # System Preferences > Date & Time > Clock
  defaults write com.apple.menuextra.clock DateFormat "EEE d. MMM  HH:mm"

  defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.airplay" -bool true
  defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.airport" -bool true
  defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.battery" -bool true
  defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.bluetooth" -bool true
  defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.clock" -bool true
  defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.volume" -bool true
  defaults write com.apple.TextInputMenu visible -bool true
  defaults write com.apple.systemuiserver menuExtras -array "/System/Library/CoreServices/Menu Extras/Clock.menu" "/System/Library/CoreServices/Menu Extras/AirPort.menu" "/System/Library/CoreServices/Menu Extras/Battery.menu" "/System/Library/CoreServices/Menu Extras/Volume.menu" "/System/Library/CoreServices/Menu Extras/Bluetooth.menu" "/System/Library/CoreServices/Menu Extras/Displays.menu"
  defaults write com.apple.menuextra.battery ShowPercent -bool true
  defaults write com.apple.airplay showInMenuBarIfPresent -bool true

  _statusMessage "" "Changed" "Menu Bar" " settings."
}

#######################################################################
# Screenshot                                                          #
#######################################################################
_screenshotSettings() {
  # Display a shadow in window captures
  defaults write com.apple.screencapture disable-shadow -int 1

  # Show the floating thumbnail
  defaults write com.apple.screencapture show-thumbnail -int 0

  # Screenshots path
  # shellcheck disable=2088
  defaults write com.apple.screencapture location -string "~/Pictures/_screenshots"

  _statusMessage "" "Changed" "Screenshot" " settings."
}

#######################################################################
# Mission Control                                                     #
#######################################################################
_missionControlSettings() {
  # System Preferences > Mission Control > Automatically rearrange Spaces based on most recent use:
  defaults write com.apple.dock mru-spaces -bool false

  _statusMessage "" "Changed" "Mission Control" " settings."
}

#######################################################################
# Spotlight                                                           #
#######################################################################
_spotlightSettings() {
  # System Preferences > Spotlight > Search Results
  defaults write com.apple.spotlight orderedItems -array \
    '{enabled = 1;name = "APPLICATIONS";}' \
    '{enabled = 0;name = "MENU_SPOTLIGHT_SUGGESTIONS";}' \
    '{enabled = 1;name = "MENU_CONVERSION";}' \
    '{enabled = 1;name = "MENU_EXPRESSION";}' \
    '{enabled = 1;name = "MENU_DEFINITION";}' \
    '{enabled = 1;name = "SYSTEM_PREFS";}' \
    '{enabled = 0;name = "DOCUMENTS";}' \
    '{enabled = 0;name = "DIRECTORIES";}' \
    '{enabled = 0;name = "PRESENTATIONS";}' \
    '{enabled = 0;name = "SPREADSHEETS";}' \
    '{enabled = 0;name = "PDF";}' \
    '{enabled = 0;name = "MESSAGES";}' \
    '{enabled = 0;name = "CONTACT";}' \
    '{enabled = 0;name = "EVENT_TODO";}' \
    '{enabled = 0;name = "IMAGES";}' \
    '{enabled = 0;name = "BOOKMARKS";}' \
    '{enabled = 0;name = "MUSIC";}' \
    '{enabled = 0;name = "MOVIES";}' \
    '{enabled = 0;name = "FONTS";}' \
    '{enabled = 0;name = "MENU_OTHER";}'

  _statusMessage "" "Changed" "Spotlight" " settings."
}

#######################################################################
# Language & Region                                                   #
#######################################################################
_languageRegionSettings() {
  # System Preferences > Language & Region
  defaults write NSGlobalDomain AppleLanguages -array "en_EE"
  defaults write NSGlobalDomain AppleLocale -string "en_EE"
  defaults write NSGlobalDomain AppleMeasurementUnits -string "Centimeters"
  defaults write NSGlobalDomain AppleMetricUnits -bool true
  defaults write NSGlobalDomain AppleTemperatureUnit -string "Celsius"

  _statusMessage "" "Changed" "Language & Region" " settings."
}

#######################################################################
# Security & Privacy                                                  #
#######################################################################
_securityPrivacySettings() {
  # System Preferences > Security & Privacy
  defaults write com.apple.AdLib forceLimitAdTracking -bool true
  defaults write com.apple.assistant.support "Siri Data Sharing Opt-In Status" -int 2

  _statusMessage "" "Changed" "Security & Privacy" " settings."
}

#######################################################################
# Sound                                                               #
#######################################################################
_soundSettings() {
  # Disable the boot chime.
  sudo nvram SystemAudioVolume=" "

  # Don't play feedback when volume is changed.
  defaults write NSGlobalDomain com.apple.sound.beep.feedback -bool false

  _statusMessage "" "Changed" "Sound" " settings."
}

#######################################################################
# Keyboard                                                            #
#######################################################################
_keyboardSettings() {
  # System Preferences > Keyboard > Keyboard > Key Repeat:
  # Slow                 fast
  # 120, 90, 60, 30, 12, 6, 2
  defaults write NSGlobalDomain KeyRepeat -int 2

  # System Preferences > Keyboard > Keyboard > Delay Until Repeat:
  # Long              short
  # 120, 94, 68, 35, 25, 15
  defaults write NSGlobalDomain InitialKeyRepeat -int 15

  # Turn off press and hold for alternative characters
  defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

  # Turn off automatic text completion:
  defaults write NSGlobalDomain NSAutomaticTextCompletionEnabled -bool false

  # System Preferences > Keyboard > Text > Correct spelling automatically:
  defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

  # System Preferences > Keyboard > Text > Capitalise words automatically:
  defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

  # System Preferences > Keyboard > Text > Add full stop with double space:
  defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false

  # System Preferences > Keyboard > Text > Use smart quotes and dashes:
  defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false
  defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

  # Full Keyboard Access
  # In windows and dialogs, press Tab to move keyboard focus between:
  # 1 : Text boxes and lists only
  # 3 : All controls
  defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

  _statusMessage "" "Changed" "Keyboard" " settings."
}

#######################################################################
# Trackpad                                                            #
#######################################################################
_trackpadSettings() {
  # System Preferences > Trackpad > Silent clicking:
  defaults write com.apple.AppleMultitouchTrackpad ActuationStrength -int 0

  _statusMessage "" "Changed" "Trackpad" " settings."
}

#######################################################################
# Mac App Store                                                       #
#######################################################################
_macAppStoreSettings() {
  # Enable the automatic update check
  defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true

  # Check for software updates daily, not just once per week
  defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

  # Download newly available updates in background
  defaults write com.apple.SoftwareUpdate AutomaticDownload -int 1

  # Install System data files & security updates
  defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 1

  _statusMessage "" "Changed" "Mac App Store" " settings."
}

#######################################################################
# Finder                                                              #
#######################################################################
_finderSettings() {
  # Show Path bar in Finder
  defaults write com.apple.finder ShowPathbar -bool true

  # Show Status bar in Finder
  defaults write com.apple.finder ShowStatusBar -bool true

  # View as Columns in Finder
  defaults write com.apple.finder FXPreferredViewStyle -string "clmv"

  # Group by Kind in Finder
  defaults write com.apple.finder FXPreferredGroupBy -string "Kind"

  # Keep folders on top when sorting by name
  defaults write com.apple.finder _FXSortFoldersFirst -bool true

  # Keep folders on top on the desktop
  defaults write com.apple.finder _FXSortFoldersFirstOnDesktop -bool "true"

  # When performing a search, search the current folder by default
  defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

  # Show hidden files & folders
  defaults write com.apple.finder AppleShowAllFiles -int 1

  # Finder > Preferences > Show all filename extensions
  defaults write NSGlobalDomain AppleShowAllExtensions -bool true

  # Remove the delay when hovering the toolbar title
  defaults write NSGlobalDomain NSToolbarTitleViewRolloverDelay -float 0

  # Set Downloads as the default location for new Finder windows
  defaults write com.apple.finder NewWindowTarget -string "PfLo"
  defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/Downloads/"

  # Avoid creating .DS_Store files on network or USB volumes
  defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
  defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

  # Show icons for hard drives, servers, and removable media on the desktop
  defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
  defaults write com.apple.finder ShowHardDrivesOnDesktop -bool false
  defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
  defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true

  # Enable snap-to-grid for icons on the desktop and in other icon views
  /usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
  /usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist

  # Limit recent places list to 1
  defaults write -g NSNavRecentPlacesLimit -int 1

  _statusMessage "" "Changed" "Finder" " settings."
}

#######################################################################
# Safari                                                              #
#######################################################################
_safariSettings() {
  # Privacy: don’t send search queries to Apple
  defaults write com.apple.Safari UniversalSearchEnabled -bool false
  defaults write com.apple.Safari SuppressSearchSuggestions -bool true

  # Show the full URL.
  defaults write com.apple.Safari ShowFullURLInSmartSearchField -bool true

  # Set Safari’s home page to `about:blank` for faster loading
  defaults write com.apple.Safari HomePage -string "about:blank"

  # Prevent Safari from opening ‘safe’ files automatically after downloading
  defaults write com.apple.Safari AutoOpenSafeDownloads -bool false

  # Hide Safari’s bookmarks bar by default
  defaults write com.apple.Safari ShowFavoritesBar -bool false

  # Hide Safari’s sidebar in Top Sites
  defaults write com.apple.Safari ShowSidebarInTopSites -bool false

  # Disable Safari’s thumbnail cache for History and Top Sites
  defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2

  # Make Safari’s search banners default to Contains instead of Starts With
  defaults write com.apple.Safari FindOnPageMatchesWordStartsOnly -bool false

  # Remove useless icons from Safari’s bookmarks bar
  defaults write com.apple.Safari ProxiesInBookmarksBar "()"

  # Enable the Develop menu and the Web Inspector in Safari
  defaults write com.apple.Safari IncludeDevelopMenu -bool true
  defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
  defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true

  # Add a context menu item for showing the Web Inspector in web views
  defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

  # Disable auto-correct
  defaults write com.apple.Safari WebAutomaticSpellingCorrectionEnabled -bool false

  # Disable AutoFill
  defaults write com.apple.Safari AutoFillFromAddressBook -bool false
  defaults write com.apple.Safari AutoFillPasswords -bool false
  defaults write com.apple.Safari AutoFillCreditCardData -bool false
  defaults write com.apple.Safari AutoFillMiscellaneousForms -bool false

  # Enable "Do Not Track"
  defaults write com.apple.Safari SendDoNotTrackHTTPHeader -bool true

  # Warn about fraudulent websites
  defaults write com.apple.Safari WarnAboutFraudulentWebsites -bool true

  # Block pop-up windows
  defaults write com.apple.Safari WebKitJavaScriptCanOpenWindowsAutomatically -bool false
  defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaScriptCanOpenWindowsAutomatically -bool false

  # Disable auto-playing video
  defaults write com.apple.Safari WebKitMediaPlaybackAllowsInline -bool false
  defaults write com.apple.SafariTechnologyPreview WebKitMediaPlaybackAllowsInline -bool false
  defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2AllowsInlineMediaPlayback -bool false
  defaults write com.apple.SafariTechnologyPreview com.apple.Safari.ContentPageGroupIdentifier.WebKit2AllowsInlineMediaPlayback -bool false

  # Update extensions automatically
  defaults write com.apple.Safari InstallExtensionUpdatesAutomatically -bool true

  _statusMessage "" "Changed" "Safari" " settings."
}

#######################################################################
# Activity Monitor                                                    #
#######################################################################
_activityMonitorSettings() {
  # Show the main window when launching Activity Monitor
  defaults write com.apple.ActivityMonitor OpenMainWindow -bool true

  # Show all processes in Activity Monitor
  defaults write com.apple.ActivityMonitor ShowCategory -int 0

  # Sort Activity Monitor results by CPU usage
  defaults write com.apple.ActivityMonitor SortColumn -string "CPUUsage"
  defaults write com.apple.ActivityMonitor SortDirection -int 0

  # Refresh data every 2 seconds
  defaults write com.apple.ActivityMonitor UpdatePeriod -int 2

  _statusMessage "" "Changed" "Activity Monitor" " settings."
}

#######################################################################
# TextEdit                                                            #
#######################################################################
_textEditSettings() {
  # Use plain text mode for new TextEdit documents
  defaults write com.apple.TextEdit RichText -int 0

  # Open and save files as UTF-8 in TextEdit
  defaults write com.apple.TextEdit PlainTextEncoding -int 4
  defaults write com.apple.TextEdit PlainTextEncodingForWrite -int 4

  _statusMessage "" "Changed" "TextEdit" " settings."
}

#######################################################################
# iTerm2                                                              #
#######################################################################
_itermSettings() {
  # Specify the preferences directory
  # shellcheck disable=2317
  defaults write com.googlecode.iterm2.plist PrefsCustomFolder -string "$HOME/.dotfiles/mac/iterm"

  # Tell iTerm2 to use the custom preferences in the directory
  # shellcheck disable=2317
  defaults write com.googlecode.iterm2.plist LoadPrefsFromCustomFolder -bool true

  # shellcheck disable=2317
  _statusMessage "" "Changed" "iTerm" " settings."
}

#######################################################################
# Messages                                                            #
#######################################################################
_messagesSettings() {
  # Disable continuous spell checking
  defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "continuousSpellCheckingEnabled" -bool false

  _statusMessage "" "Changed" "Messages" " settings."
}

#######################################################################
# Transmission                                                        #
#######################################################################
_transmissionSettings() {
  # Don’t prompt for confirmation before downloading
  defaults write org.m0k.transmission DownloadAsk -bool false
  defaults write org.m0k.transmission MagnetOpenAsk -bool false

  # Hide the donate message
  defaults write org.m0k.transmission WarningDonate -bool false

  # Hide the legal disclaimer
  defaults write org.m0k.transmission WarningLegal -bool false

  _statusMessage "" "Changed" "Transmission" " settings."
}

#######################################################################
# Miscellaneous                                                       #
#######################################################################
_miscellaneousSettings() {
  # Disable the sudden motion sensor as it’s not useful for SSDs
  sudo pmset -a sms 0

  _statusMessage "" "Changed" "Miscellaneous" " settings."
}

# Close any open System Preferences panes, to prevent them from overriding
# settings we’re about to change.
osascript -e 'tell application "System Preferences" to quit'

if [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]]; then
  _checkMacAdmin &&
    _askForSudo &&
    _keepSudoAlive &&
    _preventSleeping
fi &&
  _setVariables &&
  [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]] && echo
_copyMacConfigFiles &&
  _notificationSettings &&
  _generalSettings &&
  _accessibilitySettings &&
  _dockSettings &&
  _menuBarSettings &&
  _screenshotSettings &&
  _missionControlSettings &&
  _spotlightSettings &&
  _languageRegionSettings &&
  _securityPrivacySettings &&
  _soundSettings &&
  _keyboardSettings &&
  _trackpadSettings &&
  _macAppStoreSettings &&
  _finderSettings &&
  _safariSettings &&
  _activityMonitorSettings &&
  _textEditSettings &&
  # _itermSettings &&
  if ! $_DOTFILES_LOCAL_WORK_COMPUTER; then
    _messagesSettings &&
      _transmissionSettings
  fi &&
  _miscellaneousSettings &&
  echo
_statusMessage --bootstrap "macOS"

if [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]]; then
  _promptReboot
fi
