#!/usr/bin/env bash

# Shellcheck doesn't understand non-constant source.
# Ignore undefined variables.
# shellcheck disable=SC1091,SC2154

make precommit/install

source "$HOME/.dotfiles/setup/set-variables.sh"
declare _DOTFILES_SETUP_BOOTSTRAP _DOTFILES_SETUP_WORK_COMPUTER \
  _DOTFILES_SETUP_WRITE_GITCONFIG \
  _DOTFILES_SETUP_GIT_NAME _DOTFILES_SETUP_GIT_EMAIL \
  _DOTFILES_SETUP_WALLPAPERS _DOTFILES_SETUP_GIFS

_promptWorkComputer() {
  _promptMessage --simple "Is this a work computer?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_SETUP_WORK_COMPUTER=true
        break
        ;;
      [Nn]*)
        _DOTFILES_SETUP_WORK_COMPUTER=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done

  if ! $_DOTFILES_SETUP_WORK_COMPUTER; then
    _promptChangingComputerName
  fi
}

_promptGitVariables() {
  if [[ -f "$HOME/.gitconfig" ]]; then
    _promptMessage --withoutYesNo "A global " "$HOME/.gitconfig" " file already exists:"
    echo
    echo
    cat "$HOME/.gitconfig" &&
      _promptMessage --simple "Do you want to overwrite it?"
    while read -r yn; do
      case $yn in
        [Yy]*)
          _DOTFILES_SETUP_WRITE_GITCONFIG=true
          break
          ;;
        [Nn]*)
          _DOTFILES_SETUP_WRITE_GITCONFIG=false
          break
          ;;
        *) _rePrompt ;;
      esac
    done
  else
    _DOTFILES_SETUP_WRITE_GITCONFIG=true
  fi

  if $_DOTFILES_SETUP_WRITE_GITCONFIG; then
    echo
    echo -en "${Bold}Name${Normal} for git config: "
    read -r _DOTFILES_SETUP_GIT_NAME
    echo -en "${Bold}Email${Normal} for git config: "
    read -r _DOTFILES_SETUP_GIT_EMAIL
  fi
}

_promptWallpapersVariables() {
  _promptMessage "Do you want to install some " "wallpapers" "?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_SETUP_WALLPAPERS=true
        break
        ;;
      [Nn]*)
        _DOTFILES_SETUP_WALLPAPERS=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done
}

_promptGifsVariables() {
  _promptMessage "Do you want to install some " "gifs" "?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_SETUP_GIFS=true
        break
        ;;
      [Nn]*)
        _DOTFILES_SETUP_GIFS=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done
}

_setVariables() {
  _DOTFILES_SETUP_BOOTSTRAP=true

  _promptWorkComputer
  _promptGitVariables
  _promptWallpapersVariables
  _promptGifsVariables

  if [[ "$(uname -m)" == "arm64" ]]; then
    export PATH="/opt/homebrew/bin:/opt/homebrew/sbin:$PATH"
  else
    export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
  fi

  export _DOTFILES_SETUP_BOOTSTRAP _DOTFILES_SETUP_WORK_COMPUTER \
    _DOTFILES_SETUP_WRITE_GITCONFIG \
    _DOTFILES_SETUP_GIT_NAME _DOTFILES_SETUP_GIT_EMAIL \
    _DOTFILES_SETUP_WALLPAPERS _DOTFILES_SETUP_GIFS
}

_unsetVariables() {
  unset _DOTFILES_SETUP_BOOTSTRAP _DOTFILES_SETUP_WORK_COMPUTER \
    _DOTFILES_SETUP_WRITE_GITCONFIG \
    _DOTFILES_SETUP_GIT_NAME _DOTFILES_SETUP_GIT_EMAIL \
    _DOTFILES_SETUP_WALLPAPERS _DOTFILES_SETUP_GIFS
}

_checkMacAdmin &&
  _askForSudo &&
  _keepSudoAlive &&
  _promptAppStoreLogin &&
  _preventSleeping &&
  _setVariables &&

  # Run partials.
  bash "$HOME/.dotfiles/setup/setup-directories.sh" &&
  bash "$HOME/.dotfiles/setup/setup-symlinks.sh" &&
  bash "$HOME/.dotfiles/setup/setup-software.sh" &&
  bash "$HOME/.dotfiles/setup/setup-zsh.sh" &&
  if [[ $OSTYPE == "darwin"* ]]; then
    bash "$HOME/.dotfiles/setup/setup-macos.sh"
  fi &&
  if $_DOTFILES_SETUP_WORK_COMPUTER; then
    bash "$HOME/.dotfiles/setup-work/install.sh"
  fi

echo
_statusMessage --finish "Setup completed!"
_promptReboot
_unsetVariables
