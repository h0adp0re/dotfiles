#!/usr/bin/env bash

# Shellcheck doesn't understand non-constant source.
# shellcheck disable=SC1091

source "$HOME/.dotfiles/setup/set-variables.sh"
declare _DOTFILES_LOCAL_WORK_COMPUTER

_promptWorkComputer() {
  _promptMessage --simple "Is this a work computer?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_LOCAL_WORK_COMPUTER=true
        break
        ;;
      [Nn]*)
        _DOTFILES_LOCAL_WORK_COMPUTER=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done
}

_setVariables() {
  if [[ $_DOTFILES_SETUP_BOOTSTRAP == true ]]; then
    _DOTFILES_LOCAL_WORK_COMPUTER="$_DOTFILES_SETUP_WORK_COMPUTER"
  else
    _promptWorkComputer
  fi
}

# Create config symlinks
_createConfigSymlinks() {
  # hushlogin
  ln -svfn "$HOME/.dotfiles/home/.hushlogin" "$HOME/.hushlogin"

  # ssh
  ln -svfn "$HOME/.dotfiles/home/.ssh/config" "$HOME/.ssh/config"

  # zsh
  cp -vr "$HOME/.dotfiles/home/.zshenv" "$HOME/.zshenv"
  find "$HOME/.dotfiles/.config/zsh" \
    \! -name "local.sh" \
    -mindepth 1 -maxdepth 1 \
    -exec ln -svfn "{}" "$HOME/.config/zsh" ";"
  find "$HOME/.dotfiles/setup-work/.config/zsh" \
    \! -name "local.sh" \
    -mindepth 1 -maxdepth 1 \
    -exec ln -svfn "{}" "$HOME/.config/zsh" ";"

  # tmux
  find "$HOME/.dotfiles/.config/tmux" \
    -mindepth 1 -maxdepth 1 \
    -exec ln -svfn "{}" "$HOME/.config/tmux" ";"

  # vim
  # find "$HOME/.dotfiles/.config/vim" \
  #   -mindepth 1 -maxdepth 1 \
  #   -exec ln -svfn "{}" "$HOME/.config/vim" ";"

  # neovim
  find "$HOME/.dotfiles/.config/nvim" \
    -mindepth 1 -maxdepth 1 \
    -exec ln -svfn "{}" "$HOME/.config/nvim" ";"

  # bat
  ln -svfn "$HOME/.dotfiles/.config/bat" "$HOME/.config/bat"

  # jrnl
  ln -svfn "$HOME/.dotfiles/.config/jrnl" "$HOME/.config/jrnl"

  # git
  find "$HOME/.dotfiles/.config/git" \
    -mindepth 1 -maxdepth 1 \
    -exec ln -svfn "{}" "$HOME/.config/git" ";"

  # tig
  ln -svfn "$HOME/.dotfiles/.config/tig" "$HOME/.config/tig"

  # yamlfmt
  ln -svfn "$HOME/.dotfiles/.config/yamlfmt" "$HOME/.config/yamlfmt"

  # Taskwarrior
  cp -vr "$HOME/.dotfiles/.config/task/taskrc" "$HOME/.config/task/taskrc"
  ln -svfn "$HOME/.dotfiles/.config/task/taskrc_preset.conf" "$HOME/.config/task/taskrc_preset.conf"
  ln -svfn "$HOME/.dotfiles/.config/task/themes" "$HOME/.config/task/themes"

  # Timewarrior
  ln -svfn "$HOME/.dotfiles/.config/timewarrior/timewarrior.cfg" "$HOME/.config/timewarrior/timewarrior.cfg"

  # Lynx
  ln -svfn "$HOME/.dotfiles/.config/lynx" "$HOME/.config/lynx"
  ln -svfn "$HOME/.dotfiles/home/.lynxrc" "$HOME/.lynxrc"

  # w3m
  ln -svfn "$HOME/.dotfiles/home/.w3m/config" "$HOME/.w3m/config"

  # newsboat
  ln -svfn "$HOME/.dotfiles/.config/newsboat" "$HOME/.config/newsboat"

  # ranger
  ln -svfn "$HOME/.dotfiles/.config/ranger/rc.conf" "$HOME/.config/ranger/rc.conf"
  ln -svfn "$HOME/.dotfiles/.config/ranger/colorschemes/custom.py" "$HOME/.config/ranger/colorschemes/custom.py"

  # vifm
  find "$HOME/.dotfiles/.config/vifm" \
    -mindepth 1 -maxdepth 1 \
    -exec ln -svfn "{}" "$HOME/.config/vifm" ";"

  # nnn bookmarks
  ln -svfn "$HOME/.config" "$HOME/.config/nnn/bookmarks/config"
  ln -svfn "$HOME/Desktop" "$HOME/.config/nnn/bookmarks/Desktop"
  ln -svfn "$HOME/Documents" "$HOME/.config/nnn/bookmarks/Documents"
  ln -svfn "$HOME/.dotfiles" "$HOME/.config/nnn/bookmarks/dotfiles"
  ln -svfn "$HOME/Downloads" "$HOME/.config/nnn/bookmarks/Downloads"
  ln -svfn "$HOME" "$HOME/.config/nnn/bookmarks/home"
  ln -svfn "$HOME/Library" "$HOME/.config/nnn/bookmarks/Library"
  ln -svfn "$HOME/Movies" "$HOME/.config/nnn/bookmarks/Movies"
  ln -svfn "$HOME/Music" "$HOME/.config/nnn/bookmarks/Music"
  ln -svfn "$HOME/Pictures" "$HOME/.config/nnn/bookmarks/Pictures"
  ln -svfn "$HOME/projects" "$HOME/.config/nnn/bookmarks/projects"

  # thyme
  ln -svfn "$HOME/.dotfiles/home/.thymerc" "$HOME/.thymerc"

  # Glamour
  ln -svfn "$HOME/.dotfiles/.config/glamour" "$HOME/.config/glamour"

  # alacritty
  ln -svfn "$HOME/.dotfiles/.config/alacritty" "$HOME/.config/alacritty"

  # Tridactyl
  ln -svfn "$HOME/.dotfiles/.config/tridactyl" "$HOME/.config/tridactyl"

  # WeeChat
  find "$HOME/.dotfiles/.config/weechat" \
    -mindepth 1 -maxdepth 1 -type f \
    -exec ln -svfn "{}" "$HOME/.config/weechat" ";"

  # yt-dlp
  ln -svfn "$HOME/.dotfiles/.config/yt-dlp" "$HOME/.config/yt-dlp"
}

# Create general-purpose symlinks
_createGeneralSymlinks() {
  ln -svfn "$HOME/.dotfiles" "$HOME/projects/dotfiles"
  ln -svfn "$HOME/.dotfiles/setup-work" "$HOME/projects/setup-work"

  ln -svfn "$HOME/.dotfiles/.editorconfig" "$HOME/.editorconfig"

  # Remap F4, F5, F6 on newer MacBooks
  # https://apple.stackexchange.com/a/451930
  if [[ $OSTYPE == "darwin"* ]] && [[ "$(uname -m)" == "arm64" ]]; then
    ln -svfn "$HOME/.dotfiles/mac/keyboard/com.local.KeyRemapping.plist" "$HOME/Library/LaunchAgents/com.local.KeyRemapping.plist"
  fi
}

# Create folders and/or symlinks for a professional environment
_createWorkSymlinks() {
  if [[ $OSTYPE == "darwin"* ]]; then
    # Brewfile with work-appropriate software
    ln -svfn "$HOME/.dotfiles/setup-work/home/Brewfile" "$HOME/.Brewfile"
  fi

  # Work binaries
  ln -svfn "$HOME/.dotfiles/setup-work/home/bin" "$HOME/bin"

  # jira-cli
  ln -svfn "$HOME/.dotfiles/setup-work/.config/.jira/templates" "$HOME/.config/.jira/templates"

  # tmuxinator
  find "$HOME/.dotfiles/.config/tmuxinator" \
    -mindepth 1 -maxdepth 1 -type f \
    -exec ln -svfn "{}" "$HOME/.config/tmuxinator" ";"
  find "$HOME/.dotfiles/setup-work/.config/tmuxinator" \
    -mindepth 1 -maxdepth 1 -type f \
    -exec ln -svfn "{}" "$HOME/.config/tmuxinator" ";"

  # vim
  ln -svfn "$HOME/.dotfiles/setup-work/.config/vim/env.vim" "$HOME/.config/vim/env.vim"

  # zsh
  ln -svfn "$HOME/.dotfiles/setup-work/.config/zsh/local.sh" "$HOME/.config/zsh/local.sh"
}

# Create folders and/or symlinks for a private environment
_createPrivateSymlinks() {
  if [[ $OSTYPE == "darwin"* ]]; then
    # Brewfile with non-work software
    ln -svfn "$HOME/.dotfiles/home/Brewfile" "$HOME/.Brewfile"
  fi

  # Non-work aliases & functions
  ln -svfn "$HOME/.dotfiles/.config/zsh/local.sh" "$HOME/.config/zsh/local.sh"
}

if [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]]; then
  _checkMacAdmin &&
    _preventSleeping
fi &&
  _setVariables &&
  [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]] && echo
_createConfigSymlinks &&
  _createGeneralSymlinks &&

  # Brewfile depending on environment
  if $_DOTFILES_LOCAL_WORK_COMPUTER; then
    _createWorkSymlinks
  else
    _createPrivateSymlinks
  fi &&
  echo
_statusMessage --bootstrap "Symbolic Links"
[[ $_DOTFILES_SETUP_BOOTSTRAP == true ]] && echo
