#!/usr/bin/env bash

# Shellcheck doesn't understand non-constant source.
# shellcheck disable=SC1091

source "$HOME/.dotfiles/setup/set-variables.sh"

_setZshAsDefault() {
  local shellsFile desiredShell shellIsListed

  shellsFile="/etc/shells"
  desiredShell="$(command -v zsh)"
  _lineExistsInFile "$desiredShell" "$shellsFile"
  shellIsListed="$?"

  if [[ $shellIsListed -eq 0 && $SHELL == "$desiredShell" ]]; then
    [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]] && echo
    _statusMessage --skip "Default shell already is" "$desiredShell" ".\n"
  else
    [[ $shellIsListed -eq 0 ]] || echo "$desiredShell" | sudo tee -a "$shellsFile" &> /dev/null
    [[ $shellIsListed -eq 0 ]] && sudo chsh -s "$desiredShell" "$(whoami)"

    _statusMessage "\n" "Set" "$desiredShell" " as the default shell.\n"
  fi
}

_installZshPlugin() {
  local folder="plugins"
  local authorAndPlugin="$1"
  local plugin="${authorAndPlugin##*/}"

  if [[ $1 == "--theme" ]]; then
    folder="themes"
    authorAndPlugin="$2"
    plugin="${authorAndPlugin##*/}"
  fi

  if [[ -d "${ZDOTDIR:-$HOME/.config/zsh}/$folder/$plugin" ]]; then
    _statusMessage --skip "Skipped" "$plugin" ": already installed.\n"
  else
    git -C "${ZDOTDIR:-$HOME/.config/zsh}/$folder" clone --depth=1 "https://github.com/$authorAndPlugin.git"

    _statusMessage "\n" "Installed" "$plugin" ".\n"
  fi
}

if [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]]; then
  _checkMacAdmin &&
    _askForSudo &&
    _keepSudoAlive &&
    _preventSleeping
fi &&
  _setZshAsDefault &&
  # Suggests commands as you type based on history and completions.
  _installZshPlugin "zsh-users/zsh-autosuggestions" &&
  # Additional completion definitions for Zsh.
  _installZshPlugin "zsh-users/zsh-completions" &&
  # Highlights commands whilst they are typed at a Zsh prompt into an interactive terminal.
  _installZshPlugin "zsh-users/zsh-syntax-highlighting" &&
  # Reminds if an alias exists for a command you just typed.
  _installZshPlugin "MichaelAquilina/zsh-you-should-use" &&
  # Zsh plugin for installing, updating and loading nvm.
  _installZshPlugin "lukechilds/zsh-nvm" &&
  # Measure how long it takes for your Zsh prompt to render.
  _installZshPlugin "romkatv/zsh-prompt-benchmark" &&
  # Theme for Zsh prompt.
  _installZshPlugin --theme "romkatv/powerlevel10k" &&
  _statusMessage --bootstrap "Zsh"
[[ $_DOTFILES_SETUP_BOOTSTRAP == true ]] && echo
