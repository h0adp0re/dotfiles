#!/usr/bin/env bash

# Shellcheck doesn't understand non-constant source.
# shellcheck disable=SC1091

source "$HOME/.dotfiles/setup/set-variables.sh"
declare _DOTFILES_LOCAL_WORK_COMPUTER

_promptWorkComputer() {
  _promptMessage --simple "Is this a work computer?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_LOCAL_WORK_COMPUTER=true
        break
        ;;
      [Nn]*)
        _DOTFILES_LOCAL_WORK_COMPUTER=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done
}

_setVariables() {
  if [[ $_DOTFILES_SETUP_BOOTSTRAP == true ]]; then
    _DOTFILES_LOCAL_WORK_COMPUTER="$_DOTFILES_SETUP_WORK_COMPUTER"
  else
    _promptWorkComputer
  fi
}

# Create general-purpose folders
_createGeneralDirectories() {
  mkdir -p "$HOME/projects"
}

# Create config folders (to avoid the prompt to create them)
_createConfigDirectories() {
  # ssh
  mkdir -m 700 "$HOME/.ssh"

  # zsh
  mkdir -p "$HOME/.config/zsh/plugins"
  mkdir -p "$HOME/.config/zsh/themes"
  mkdir -p "$HOME/.local/share/zsh"
  mkdir -p "$HOME/.cache/zsh"

  # tmux
  mkdir -p "$HOME/.config/tmux"

  # tmuxinator
  mkdir -p "$HOME/.config/tmuxinator"

  # vim
  mkdir -p "$HOME/.config/vim/backup"
  mkdir -p "$HOME/.config/vim/undo"
  mkdir -p "$HOME/.config/vim/session"

  # neovim
  mkdir -p "$HOME/.local/share/nvim/backup"
  mkdir -p "$HOME/.local/share/nvim/undo"
  mkdir -p "$HOME/.local/share/nvim/sessions"
  mkdir -p "$HOME/.config/nvim/backup"

  # git
  mkdir -p "$HOME/.config/git"

  # bashunit
  mkdir -p "$HOME/.cache/bashunit"
  touch "$HOME/.cache/bashunit/dev.log"

  # Taskwarrior
  mkdir -p "$HOME/.config/task"

  # Timewarrior
  mkdir -p "$HOME/.config/timewarrior"

  # w3m
  mkdir -p "$HOME/.w3m"

  # ranger
  mkdir -p "$HOME/.config/ranger/colorschemes"

  # vifm
  mkdir -p "$HOME/.config/vifm"

  # nnn
  mkdir -p "$HOME/.config/nnn/bookmarks"

  # WeeChat
  mkdir -p "$HOME/.config/weechat"
  mkdir -p "$HOME/.config/weechat/scripts"
}

# Create mac-specific directories
_createMacDirectories() {
  mkdir -p "$HOME/Pictures/_screenshots"
  mkdir -p "$HOME/Library/Workflows/Applications/Folder Actions"
}

# Create work folders
_createWorkDirectories() {
  # Work projects
  mkdir -p "$HOME/work"

  # jira-cli
  mkdir -p "$HOME/.config/.jira"
}

if [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]]; then
  _checkMacAdmin &&
    _preventSleeping
fi &&
  _setVariables &&
  [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]] && echo
_createGeneralDirectories &&
  _createConfigDirectories &&
  if [[ $OSTYPE == "darwin"* ]]; then
    _createMacDirectories
  fi &&
  if $_DOTFILES_LOCAL_WORK_COMPUTER; then
    _createWorkDirectories
  fi &&
  [[ $_DOTFILES_SETUP_BOOTSTRAP == true ]] && echo
_statusMessage --bootstrap "Directories"
[[ $_DOTFILES_SETUP_BOOTSTRAP == true ]] && echo
