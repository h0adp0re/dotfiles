#!/usr/bin/env bash

# Shellcheck doesn't understand non-constant source.
# Ignore undefined variables.
# shellcheck disable=SC1091,SC2154

source "$HOME/.dotfiles/setup/set-variables.sh"
declare _DOTFILES_LOCAL_WRITE_GITCONFIG \
  _DOTFILES_LOCAL_GIT_NAME _DOTFILES_LOCAL_GIT_EMAIL \
  _DOTFILES_LOCAL_WALLPAPERS _DOTFILES_LOCAL_GIFS

_promptGitVariables() {
  if [[ -f "$HOME/.gitconfig" ]]; then
    _promptMessage --withoutYesNo "A global " "$HOME/.gitconfig" " file already exists:"
    echo
    echo
    cat "$HOME/.gitconfig" &&
      _promptMessage --simple "Do you want to overwrite it?"
    while read -r yn; do
      case $yn in
        [Yy]*)
          _DOTFILES_LOCAL_WRITE_GITCONFIG=true
          break
          ;;
        [Nn]*)
          _DOTFILES_LOCAL_WRITE_GITCONFIG=false
          break
          ;;
        *) _rePrompt ;;
      esac
    done
  else
    _DOTFILES_LOCAL_WRITE_GITCONFIG=true
  fi

  if $_DOTFILES_LOCAL_WRITE_GITCONFIG; then
    echo
    echo -en "${Bold}Your name${Normal} for git config: "
    read -r _DOTFILES_LOCAL_GIT_NAME
    echo -en "${Bold}Your email${Normal} for git config: "
    read -r _DOTFILES_LOCAL_GIT_EMAIL
  fi
}

_promptWallpapersVariables() {
  _promptMessage "Do you want to install some " "wallpapers" "?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_LOCAL_WALLPAPERS=true
        break
        ;;
      [Nn]*)
        _DOTFILES_LOCAL_WALLPAPERS=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done
}

_promptGifsVariables() {
  _promptMessage "Do you want to install some " "gifs" "?"
  while read -r yn; do
    case $yn in
      [Yy]*)
        _DOTFILES_LOCAL_GIFS=true
        break
        ;;
      [Nn]*)
        _DOTFILES_LOCAL_GIFS=false
        break
        ;;
      *) _rePrompt ;;
    esac
  done
  echo
}

_setVariables() {
  if [[ $_DOTFILES_SETUP_BOOTSTRAP == true ]]; then
    _DOTFILES_LOCAL_WRITE_GITCONFIG="$_DOTFILES_SETUP_WRITE_GITCONFIG"
    _DOTFILES_LOCAL_GIT_NAME="$_DOTFILES_SETUP_GIT_NAME"
    _DOTFILES_LOCAL_GIT_EMAIL="$_DOTFILES_SETUP_GIT_EMAIL"
    _DOTFILES_LOCAL_WALLPAPERS="$_DOTFILES_SETUP_WALLPAPERS"
    _DOTFILES_LOCAL_GIFS="$_DOTFILES_SETUP_GIFS"
  else
    _promptGitVariables
    _promptWallpapersVariables
    _promptGifsVariables
  fi
}

_setupGit() {
  if $_DOTFILES_LOCAL_WRITE_GITCONFIG; then
    git config --global include.path "$HOME/.config/git/config_global" &&
      git config --global user.name "$_DOTFILES_LOCAL_GIT_NAME" &&
      git config --global user.email "$_DOTFILES_LOCAL_GIT_EMAIL" &&
      _statusMessage "" "Set up" "git config" ".\n"
  else
    _statusMessage --skip "Skipped" "git config" ".\n"
  fi
}

_installWallpapers() {
  if $_DOTFILES_LOCAL_WALLPAPERS; then
    local wallpapersDir="$HOME/Pictures/wallpapers"

    if [[ -d $wallpapersDir ]]; then
      _statusMessage --error "Directory" "$wallpapersDir" " already exists.\n"
    else
      git -C "$HOME/Pictures" clone git@gitlab.com:h0adp0re/wallpapers.git
      ln -svfn "$wallpapersDir" "$HOME/projects/wallpapers"

      _statusMessage "\n" "Installed" "wallpapers" ".\n"
    fi
  else
    _statusMessage --skip "Skipped" "wallpapers" ".\n"
  fi
}

_installGifs() {
  if $_DOTFILES_LOCAL_GIFS; then
    local gifsDir="$HOME/Pictures/gifs"

    if [[ -d $gifsDir ]]; then
      _statusMessage --error "Directory" "$gifsDir" " already exists.\n"
    else
      git -C "$HOME/Pictures" clone git@gitlab.com:h0adp0re/gifs.git
      ln -svfn "$gifsDir" "$HOME/projects/gifs"

      _statusMessage "\n" "Installed" "gifs" ".\n"
    fi
  else
    _statusMessage --skip "Skipped" "gifs" ".\n"
  fi
}

_installXcodeCLT() {
  if [[ "$(xcode-select --print-path)" == "/Library/Developer/CommandLineTools" ]]; then
    _statusMessage --skip "Skipped" "XCode CLT" ": already installed.\n"
  else
    # Install Xcode Command Line Tools.
    xcode-select --install &&
      _statusMessage "\n" "Installed" "Xcode Command Line Tools" ".\n"
  fi
}

_installHomebrew() {
  if command -v brew &> /dev/null; then
    _statusMessage --skip "Skipped" "Homebrew" ": already installed.\n"
  else
    # Install Homebrew.
    echo | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

    # Make sure Homebrew works.
    brew doctor &&
      _statusMessage "\n" "Installed" "Homebrew" ".\n"
  fi
}

_installBrewBundle() {
  # https://github.com/mas-cli/mas
  if command -v mas &> /dev/null; then
    _statusMessage --skip "Skipped" "mas" ": already installed.\n"
  else
    brew install mas
  fi

  if brew bundle check --global &> /dev/null; then
    _statusMessage --skip "Skipped" "Formulae, Casks and App Store apps" ": already installed.\n"
  else
    brew bundle install --global &&
      _statusMessage "\n" "Installed" "Formulae, Casks and App Store apps" ".\n"
  fi
}

_installPythonPackages() {
  local package pluralPackage
  local -a packages installedPackages missingPackages

  packages=(
    "pynvim"
  )

  for package in "${packages[@]}"; do
    if pip3 show "$package" &> /dev/null; then
      installedPackages+=("$package")
    else
      missingPackages+=("$package")
    fi
  done

  if (("${#missingPackages[@]}" != 0)); then
    pluralPackage="package"

    if (("${#missingPackages[@]}" > 1)); then
      pluralPackage+="s"
    fi

    # We need word splitting here
    # shellcheck disable=SC2048,SC2086
    pip3 install ${missingPackages[*]} &&
      _statusMessage "\n" "Installed Python ${pluralPackage}" "${missingPackages[*]}" ".\n"
  fi

  if (("${#installedPackages[@]}" != 0)); then
    pluralPackage="package"

    if (("${#installedPackages[@]}" > 1)); then
      pluralPackage+="s"
    fi

    _statusMessage --skip "Skipped Python ${pluralPackage}" "${installedPackages[*]}" ": already installed.\n"
  fi
}

_installNvm() {
  if [[ -d "$HOME/.nvm" ]]; then
    _statusMessage --skip "Skipped" "nvm" ": already installed.\n"
  else
    export NVM_DIR="$HOME/.nvm" && (
      git clone https://github.com/nvm-sh/nvm.git "$NVM_DIR"
      cd "$NVM_DIR"
      git checkout "$(git describe --abbrev=0 --tags --match "v[0-9]*" "$(git rev-list --tags --max-count=1)")"
    ) && /bin/bash "$NVM_DIR/nvm.sh" &&
      _statusMessage "\n" "Installed" "nvm" ".\n"
  fi
}

_installNode() {
  local latest_lts
  local current_node

  export NVM_DIR="$HOME/.nvm"
  [[ -s "$NVM_DIR/nvm.sh" ]] && \. "$NVM_DIR/nvm.sh" &&
    latest_lts="$(nvm version-remote --lts)"
  current_node="$(nvm current)"

  if [[ $latest_lts == "$current_node" ]]; then
    _statusMessage --skip "Skipped" "Node.js" ": $current_node already installed.\n"
  else
    nvm install "lts/*" &&
      _statusMessage "\n" "Installed" "LTS Node.js" ".\n"
  fi
}

_installNpmPackages() {
  local package pluralPackage
  local -a packages installedPackages missingPackages

  packages=(
    "neovim"
    "npm-check-updates"
  )

  for package in "${packages[@]}"; do
    if npm list -g -depth=0 | awk '{print $2}' | grep "$package" &> /dev/null; then
      installedPackages+=("$package")
    else
      missingPackages+=("$package")
    fi
  done

  if (("${#missingPackages[@]}" != 0)); then
    pluralPackage="package"

    if (("${#missingPackages[@]}" > 1)); then
      pluralPackage+="s"
    fi

    # We need word splitting here
    # shellcheck disable=SC2048,SC2086
    npm i --location=global npm@latest ${missingPackages[*]} &&
      _statusMessage "\n" "Installed global npm ${pluralPackage}" "${missingPackages[*]}" ".\n"
  fi

  if (("${#installedPackages[@]}" != 0)); then
    pluralPackage="package"

    if (("${#installedPackages[@]}" > 1)); then
      pluralPackage+="s"
    fi

    _statusMessage --skip "Skipped global npm ${pluralPackage}" "${installedPackages[*]}" ": already installed.\n"
  fi
}

_installWeeChatPlugins() {
  local script
  local configFolder="$HOME/.config/weechat"
  local scriptsFolder="$configFolder/scripts"
  local -a scripts=(
    "autosort"
    "bitlbee_completion"
    "colorize_nicks"
    "go"
    # "vimode"
  )

  if [[ -d "$scriptsFolder/weechat-scripts" ]]; then
    _statusMessage --skip "Skipped" "weechat-scripts" ": already installed.\n"
  else
    git -C "$scriptsFolder" clone "https://github.com/weechat/scripts.git" "weechat-scripts"

    for script in "${scripts[@]}"; do
      ln -svfn "$scriptsFolder/weechat-scripts/python/$script.py" \
        "$configFolder/python/autoload/$script.py"
    done

    _statusMessage "\n" "Installed" "weechat-scripts" ".\n"
  fi

  if [[ -d "$scriptsFolder/weechat-vimode" ]]; then
    _statusMessage --skip "Skipped" "GermainZ/weechat-vimode" ": already installed.\n"
  else
    git -C "$scriptsFolder" clone "https://github.com/GermainZ/weechat-vimode.git"
    ln -svfn "$scriptsFolder/weechat-vimode/vimode.py" "$configFolder/python/autoload/vimode.py"

    _statusMessage "\n" "Installed" "GermainZ/weechat-vimode" ".\n"
  fi
}

_installNnnPlugins() {
  if [[ -d "${XDG_CONFIG_HOME:-$HOME/.config}/nnn/plugins" ]]; then
    _statusMessage --skip "Skipped" "nnn plugins" ": already installed.\n"
  else
    curl -fsSL https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs | sh &&
      _statusMessage "\n" "Installed" "nnn plugins" ".\n"
  fi
}

_installBatThemes() {
  if [[ -f "${XDG_CACHE_HOME:-$HOME/.cache}/bat/themes.bin" ]]; then
    _statusMessage --skip "Skipped" "bat themes" ": already installed.\n"
  else
    (
      cd "$(bat --config-dir)/themes"
      bat cache --build &> /dev/null
    ) &&
      _statusMessage "" "Installed" "bat themes" ".\n"
  fi
}

_installTaskwTimewHolidays() {
  ./scripts/refresh_holidays
  _statusMessage "\n" "Refreshed" "Taskwarrior & Timewarrior holidays" ".\n"
}

if [[ -z $_DOTFILES_SETUP_BOOTSTRAP ]]; then
  _checkMacAdmin &&
    _askForSudo &&
    _keepSudoAlive &&
    _promptAppStoreLogin &&
    _preventSleeping
fi &&
  _setVariables &&
  _setupGit &&
  if [[ $OSTYPE == "darwin"* ]]; then
    _installWallpapers &&
      _installGifs &&
      _installXcodeCLT &&
      _installHomebrew &&
      _installBrewBundle
  fi &&
  _installPythonPackages &&
  _installNvm &&
  _installNode &&
  _installNpmPackages &&
  _installWeeChatPlugins &&
  _installNnnPlugins &&
  _installBatThemes &&
  _installTaskwTimewHolidays &&
  _statusMessage --bootstrap "Software"
[[ $_DOTFILES_SETUP_BOOTSTRAP == true ]] && echo
