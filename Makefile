shell=/bin/bash

static_analysis_check := $(shell which shellcheck 2> /dev/null)
formatter_check := $(shell which shfmt 2> /dev/null)
linter_check := $(shell which editorconfig-checker 2> /dev/null)
test_library_check := $(shell [[ -x "./lib/bashunit" ]] && echo true)
scripts_staged := $(shell [[ -n $$(git diff --name-only --staged --diff-filter=d -- "./scripts") ]] && echo true)

test_files := **/__tests__/*.test.sh
bashunit_version := 0.18.0
normal := $(shell tput sgr0)
yellow := $(shell tput setaf 3)

help:
	@./scripts/print-message --update "Usage: make" "<command>"
	@echo "    ${yellow}install${normal}            Install dotfiles (everything)"
	@echo "    ${yellow}install/dirs${normal}       Set up directories"
	@echo "    ${yellow}install/symlinks${normal}   Create/repair symlinks"
	@echo "    ${yellow}install/software${normal}   Install all software"
	@echo "    ${yellow}install/zsh${normal}        Set up Zsh"
	@echo "    ${yellow}install/macos${normal}      Set macOS settings"
	@echo "    ${yellow}all${normal}                Run all checks"
	@echo "    ${yellow}test${normal}               Run tests"
	@echo "    ${yellow}test/install${normal}       Install bashunit"
	@echo "    ${yellow}sa${normal}                 Run shellcheck static analysis tool"
	@echo "    ${yellow}format${normal}             Run shfmt formatter tool"
	@echo "    ${yellow}lint${normal}               Run editorconfig linter tool"
	@echo "    ${yellow}precommit${normal}          Run pre-commit hook function"
	@echo "    ${yellow}precommit/install${normal}  Install the pre-commit hook"
	@echo "    ${yellow}maintainer${normal}         Add \`user.name\` & \`user.email\` to git config"

install:
	@bash "./setup/install.sh"

install/dirs:
	@bash "./setup/setup-directories.sh"

install/symlinks:
	@bash "./setup/setup-symlinks.sh"

install/software:
	@bash "./setup/setup-software.sh"

install/zsh:
	@bash "./setup/setup-zsh.sh"

install/macos:
	@bash "./setup/setup-macos.sh"

all: test sa format lint

test:
ifndef test_library_check
	@./scripts/print-error "bashunit v${bashunit_version} must be installed:" "make test/install" && false
else
	@echo
	@./lib/bashunit ${test_files} --parallel
	@echo
endif

test/install:
	@curl -s https://bashunit.typeddevs.com/install.sh | bash -s lib ${bashunit_version}
	@./scripts/print-message --installed --no-newline-trailing "Installed" "bashunit v${bashunit_version}"

sa:
ifndef static_analysis_check
	@./scripts/print-error "ShellCheck must be installed:" "brew install shellcheck"
	@./scripts/print-message --error "ShellCheck not installed:" "Static analysis not performed!" && false
else
	@shellcheck ./**/*.sh -C
	@./scripts/print-message --installed --no-newlines "ShellCheck:" "OK"
endif

format:
ifndef formatter_check
	@./scripts/print-error "shfmt not installed:" "brew install shfmt"
	@./scripts/print-message --error "shfmt not installed:" "Formatting not performed!" && false
else
	@shfmt --list --simplify --write .
	@./scripts/print-message --installed --no-newlines "shfmt:" "OK"
endif

lint:
ifndef linter_check
	@./scripts/print-error "EditorConfig not installed:" "brew install editorconfig-checker"
	@./scripts/print-message --error "EditorConfig not installed:" "Linting not performed!" && false
else
	@editorconfig-checker -no-color
	@./scripts/print-message --installed --no-newlines "EditorConfig:" "OK"
endif

ifndef scripts_staged
precommit:
	@./scripts/print-message --check --no-newlines "Running pre-commit hook..."
	@${MAKE} sa format lint
	@echo
else
precommit:
	@./scripts/print-message --check --no-newlines "Running pre-commit hook..."
	@${MAKE} test sa format lint
	@echo
endif

precommit/install:
	@git config --add core.hooksPath .git_hooks
	@./scripts/print-message --installed --no-newlines "Installed" "pre-commit hooks"

maintainer:
	@git config --add user.name "h0adp0re"
	@git config --add user.email "1249378-h0adp0re@users.noreply.gitlab.com"
	@./scripts/print-message --installed --no-newlines "Installed" "maintainer Git credentials"
