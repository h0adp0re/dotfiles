local lsp_kinds = {
  Text = "",
  Method = "󰆧",
  Function = "󰊕",
  Constructor = "󰒓",
  Field = "󰓼",
  -- Field = "",
  Variable = "󰀫",
  Class = "󰠱",
  Interface = "",
  Module = "󰏗",
  Property = "󰌋",
  -- Property = "",
  Unit = "",
  Value = "󰎠",
  Enum = "",
  Keyword = "󰌋",
  Snippet = "󰆐",
  Color = "󰉦",
  File = "󰎛",
  Reference = "󰬳",
  Folder = "󰉋",
  EnumMember = "",
  Constant = "󰏿",
  Struct = "󰽘",
  -- Event = "󱐋",
  Event = "󰈽",
  -- Operator = "󰆕",
  Operator = "󰦒",
  TypeParameter = "󰊄",
  -- nvim-navic icons
  Namespace = "󰌗",
  Package = "",
  String = "󰀬",
  Number = "󰎠",
  Boolean = "◩",
  Array = "󰅪",
  Object = "",
  Key = "󰌋",
  Null = "󰟢",
  -- Custom addition
  Yank = "",
}

local diagnostics = {
  error = "",
  warn = "",
  info = "",
  hint = "󰌵",
  bug = "󰃤",
}

local file = {
  modified = "●",
  readonly = "󰌾",
}

local fold = {
  open = "",
  closed = "",
}

local box = {
  horizontal = "─",
  vertical = "│",
  top_right = "╮",
  bottom_left = "╰",
  bottom_right = "╯",
  left = "╴",
}

local glyphs = {
  vim = {
    listchars = {
      tab = "→ ",
      trail = "⋅",
      nbsp = "+",
      eol = "¬",
      extends = "❯",
      precedes = "❮",
    },
    fillchars = {
      fold = "·",
      diff = "",
    },
    showbreak = "↪",
  },
  diagnostics = {
    virtual_text_prefix = "▋",
    diagnostics = diagnostics,
  },
  lazy = {
    cmd = "",
    config = "󰒓",
    event = "󰈽",
    ft = "󰎛",
    init = "󰒓",
    import = "󰋺",
    keys = "󰥻",
    lazy = "󰒲 ",
    loaded = "●",
    not_loaded = "○",
    plugin = "󰏗",
    runtime = "",
    source = "󰈮",
    start = "󰼛",
    task = "✔",
    list = {
      "●",
      "➜",
      "‣",
      "━",
    },
  },
  telescope = {
    prompt_prefix = "❯ ",
    selection_caret = "▶ ",
    git_icons = {
      added = "A",
      changed = "M",
      copied = "C",
      deleted = "D",
      renamed = "R",
      unmerged = "U",
      untracked = "?",
    },
  },
  lsp_kinds = lsp_kinds,
  lsp_signature = {
    hint_prefix = "■ ",
  },
  goto_preview = {
    border = {
      "↖",
      box.horizontal,
      box.top_right,
      box.vertical,
      box.bottom_right,
      box.horizontal,
      box.bottom_left,
      box.vertical,
    },
  },
  tabline = {
    file = file,
    separator = {
      default = "",
      middle = "",
    },
  },
  statusline = {
    file = file,
    diff = {
      added = "+",
      modified = "~",
      removed = "-",
    },
    diagnostics = diagnostics,
    git = {
      branch = "",
    },
    separator = {
      component = {
        left = "",
        right = "",
      },
      section = {
        left = "",
        right = "",
      },
    },
  },
  indentline = {
    indentline = box.vertical,
  },
  neogit = {
    hunk = { fold.closed, fold.open },
    item = { fold.closed, fold.open },
    section = { fold.closed, fold.open },
  },
  gitsigns = {
    add = {
      text = box.vertical,
    },
    change = {
      text = box.vertical,
    },
    delete = {
      text = "_",
    },
    topdelete = {
      text = "‾",
    },
    changedelete = {
      text = box.vertical,
    },
  },
  diffview = {
    fold_open = fold.open,
    fold_closed = fold.closed,
    done = "✔",
  },
  todo = {
    NOTE = diagnostics.info .. " ",
    TODO = "󰸞 ",
    HACK = "󰈸 ",
    WARN = diagnostics.warn .. " ",
    PERF = "󱐋 ",
    FIX = diagnostics.bug .. " ",
  },
  trouble = {
    indent = {
      top = box.vertical .. " ",
      middle = "├╴",
      last = box.bottom_left .. box.left,
      fold_open = fold.open .. " ",
      fold_closed = fold.closed .. " ",
      ws = "  ",
    },
    folder_closed = "󰉋 ",
    folder_open = "󰝰 ",
    kinds = lsp_kinds,
  },
  fidget = {
    done = "󰸞",
  },
  notify = {
    DEBUG = diagnostics.bug,
    ERROR = diagnostics.error,
    INFO = diagnostics.info,
    TRACE = "󰲶",
    WARN = diagnostics.warn,
  },
  neotest = {
    child_indent = box.vertical,
    child_prefix = "├",
    collapsed = box.horizontal,
    expanded = box.top_right,
    final_child_indent = " ",
    final_child_prefix = box.bottom_left,
    non_collapsible = box.horizontal,
    failed = "✘",
    passed = "✔",
    running = "→",
    skipped = "󰜺",
    unknown = "?",
  },
  dap = {
    breakpoint = "󰧟",
    breakpoint_condition = "󰻂",
    breakpoint_rejected = "",
    log_point = "",
    stopped = "→",
  },
  whichkey = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and it's label
    group = "+", -- symbol prepended to a group
    mappings = false, -- don't render illustrative mapping icons
  },
}

return glyphs
