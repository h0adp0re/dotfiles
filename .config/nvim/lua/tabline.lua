local fn = vim.fn
local get_hl = require("utils").get_hl
local set_hl = require("utils").set_hl
local mod_hl = require("utils").mod_hl
local glyphs = require("constants.glyphs").tabline
local options = {
  separators_enabled = false,
  highlights = {
    background = "TabLine",
    background_selected = "TabLineSel",
    separator = "TabLineSeparator",
    separator_selected = "TabLineSeparatorSel",
    separator_middle = "TabLineSeparatorMiddle",
    separator_last = "TabLineSeparatorLast",
    separator_last_selected = "TabLineSeparatorLastSel",
  },
}

if not options.separators_enabled then
  glyphs.separator.default = ""
  glyphs.separator.middle = ""
end

-- Highlights
mod_hl("TabLine", { link = "lualine_b_normal" })
mod_hl("TabLineSel", { link = "lualine_a_normal" })
mod_hl("TabLineSel", { bold = true })
set_hl("TabLineSeparator", {
  bg = get_hl("lualine_a_normal").bg,
  fg = get_hl("lualine_b_normal").bg,
})
set_hl("TabLineSeparatorSel", {
  bg = get_hl("lualine_b_normal").bg,
  fg = get_hl("lualine_a_normal").bg,
})
set_hl("TabLineSeparatorMiddle", {
  bg = get_hl("lualine_b_normal").bg,
  fg = get_hl("lualine_a_normal").bg,
})
set_hl("TabLineSeparatorLast", {
  bg = get_hl("TabLineFill").bg,
  fg = get_hl("lualine_b_normal").bg,
})
set_hl("TabLineSeparatorLastSel", {
  bg = get_hl("TabLineFill").bg,
  fg = get_hl("lualine_a_normal").bg,
})

local function tabline()
  local tabcount = fn.tabpagenr("$")
  local line = {}

  for index = 1, tabcount do
    local current = index == fn.tabpagenr()
    local beforecurrent = index == fn.tabpagenr() - 1
    local last = index == tabcount

    local winnr = fn.tabpagewinnr(index)
    local winnrs = fn.tabpagewinnr(index, "$")
    local buflist = fn.tabpagebuflist(index)
    local bufnr = buflist[winnr]
    local bufname = fn.bufname(bufnr)
    local short_bufname = fn.fnamemodify(bufname, ":t")
    local tabname = short_bufname
    local modro = ""
    local modro_spacer = " "

    local separator = glyphs.separator.default
    local separator_hl = ""

    -- Highlight start
    table.insert(line, "%#")

    if current then
      table.insert(line, options.highlights.background_selected)

      if last then
        separator_hl = options.highlights.separator_last_selected
      else
        separator_hl = options.highlights.separator_selected
      end
    else
      table.insert(line, options.highlights.background)

      if last then
        separator_hl = options.highlights.separator_last
      elseif beforecurrent then
        separator_hl = options.highlights.separator
      else
        separator = glyphs.separator.middle
        separator_hl = options.highlights.separator_middle
      end
    end

    -- Highlight end
    table.insert(line, "#")

    -- Set tab name to filetype if not an actual file
    if tabname == "" then
      tabname = fn.getbufvar(bufnr, "&filetype")
    end

    -- Append symbols when buffer is modified or readonly
    if fn.getbufvar(bufnr, "&modified") == 1 then
      modro = glyphs.file.modified
    elseif fn.getbufvar(bufnr, "&readonly") == 1 then
      modro = glyphs.file.readonly
    else
      modro_spacer = ""
    end

    -- Start mouse click target region(s)
    table.insert(line, "%")
    table.insert(line, index)
    table.insert(line, "T")

    -- Tab index
    table.insert(line, " ")
    table.insert(line, index)

    -- Tab name
    table.insert(line, " ")
    table.insert(line, tabname)

    -- Number of windows in tab
    table.insert(line, " ")
    table.insert(line, "(")
    table.insert(line, winnrs)
    table.insert(line, ")")

    -- Modified/readonly symbol
    table.insert(line, " ")
    table.insert(line, modro)
    table.insert(line, modro_spacer)

    -- Tab separator
    table.insert(line, "%#")
    table.insert(line, separator_hl)
    table.insert(line, "#")
    table.insert(line, separator)
  end

  -- After the last tab fill with TabLineFill and reset tab page nr
  table.insert(line, "%#TabLineFill#")
  table.insert(line, "%T") -- End mouse click target region(s)

  return table.concat(line)
end

function _G.custom_tabline()
  return tabline()
end
