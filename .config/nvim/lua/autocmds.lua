local api = vim.api
local option = vim.o
local bufferoption = vim.bo
local keymap_opts = { noremap = true, silent = true }
local set = require("utils").set

local ignored_filetypes_linenumbers = set({
  "help",
  "netrw",
  "undotree",
  "twiggy",
  "DiffviewFiles",
  "mind",
  "trouble",
})

local group_notify = api.nvim_create_augroup("custom_notify", {})
local group_linenumbers = api.nvim_create_augroup("custom_linenumbers", {})
local group_syntax = api.nvim_create_augroup("custom_syntax", {})
local group_highlights = api.nvim_create_augroup("custom_highlights", {})
local group_quickfix = api.nvim_create_augroup("custom_quickfix", {})
local group_norg = api.nvim_create_augroup("custom_norg", {})
local group_lazy = api.nvim_create_augroup("custom_lazy", {})
local group_regexplainer = api.nvim_create_augroup("custom_regexplainer", {})

api.nvim_create_autocmd({ "UIEnter" }, {
  desc = "Display a notification when launching Neovim",
  group = group_notify,
  callback = function()
    vim.notify("I use Neovim (BTW)", vim.log.levels.INFO, {
      title = "BTW",
      render = "wrapped-compact",
      hide_from_history = true,
    })
  end,
})

api.nvim_create_autocmd({ "BufEnter", "FocusGained", "InsertLeave", "WinEnter" }, {
  desc = "Automatically toggle relative line numbers",
  group = group_linenumbers,
  callback = function()
    if not ignored_filetypes_linenumbers[bufferoption.filetype] then
      option.relativenumber = true
    end
  end,
})

api.nvim_create_autocmd({ "BufLeave", "FocusLost", "InsertEnter", "WinLeave" }, {
  desc = "Automatically toggle absolute line numbers",
  group = group_linenumbers,
  callback = function()
    if not ignored_filetypes_linenumbers[bufferoption.filetype] then
      option.relativenumber = false
    end
  end,
})

api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufEnter", "BufRead" }, {
  desc = "Set yaml filetype on .thymerc",
  group = group_syntax,
  pattern = ".thymerc",
  callback = function()
    option.filetype = "yaml"
  end,
})

api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufEnter", "BufRead" }, {
  desc = "Set ruby syntax on Brewfile",
  group = group_syntax,
  pattern = "Brewfile",
  callback = function()
    bufferoption.syntax = "ruby"
  end,
})

api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufEnter", "BufRead" }, {
  desc = "Set vim syntax on vifm files",
  group = group_syntax,
  pattern = { "vifmrc", "*.vifm" },
  callback = function()
    bufferoption.syntax = "vim"
  end,
})

api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufEnter", "BufRead" }, {
  desc = "Set gitconfig syntax on config_global",
  group = group_syntax,
  pattern = "config_global",
  callback = function()
    bufferoption.syntax = "gitconfig"
  end,
})

api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufEnter", "BufRead" }, {
  desc = "Set gitignore syntax on gitignore_global",
  group = group_syntax,
  pattern = "gitignore_global",
  callback = function()
    bufferoption.syntax = "gitignore"
  end,
})

api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufEnter", "BufRead" }, {
  desc = "Set env syntax on env files",
  group = group_syntax,
  pattern = { ".env", ".env.*" },
  callback = function()
    bufferoption.syntax = "env"
  end,
})

api.nvim_create_autocmd("FileType", {
  desc = "Disable cursorline in Telescope",
  group = group_highlights,
  pattern = "TelescopePrompt",
  callback = function()
    vim.wo.cursorline = false
    require("auto-cursorline").disable({
      buffer = true,
    })
  end,
})

-- api.nvim_create_autocmd("TextYankPost", {
--   desc = "Highlight yanked text",
--   group = group_highlights,
--   callback = function()
--     vim.highlight.on_yank({ higroup = "HighlightedyankRegion", timeout = 100 })
--   end,
-- })

api.nvim_create_autocmd("BufRead", {
  desc = "Map dd to delete current entry in the quickfix list",
  group = group_quickfix,
  pattern = "quickfix",
  callback = function()
    if bufferoption.filetype == "qf" then
      api.nvim_buf_set_keymap(0, "n", "dd", ":.Reject<CR>", keymap_opts)
    end
  end,
})

api.nvim_create_autocmd("BufRead", {
  desc = "Map d to delete visual selection in the quickfix list",
  group = group_quickfix,
  pattern = "quickfix",
  callback = function()
    if bufferoption.filetype == "qf" then
      api.nvim_buf_set_keymap(0, "v", "d", ":'<,'>Reject<CR>", keymap_opts)
    end
  end,
})

api.nvim_create_autocmd("BufRead", {
  desc = "Map u to restore deleted items in the quickfix list",
  group = group_quickfix,
  pattern = "quickfix",
  callback = function()
    if bufferoption.filetype == "qf" then
      api.nvim_buf_set_keymap(0, "n", "u", ":Restore<CR>", keymap_opts)
    end
  end,
})

api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
  desc = "Set conceallevel for norg files",
  group = group_norg,
  pattern = { "*.norg" },
  command = "set conceallevel=3",
})

api.nvim_create_autocmd("FileType", {
  desc = "Set keymaps for norg files",
  group = group_norg,
  pattern = { "norg" },
  callback = function(event)
    local mapkey = require("utils").mapkey
    local opts = { buffer = event.buf }

    mapkey("n", "<leader>jF", "gqipgg=G", "Format paragraph", opts)
    mapkey("i", "<C-f>", [[<cmd>Telescope neorg insert_file_link<CR>]], "Insert file link", opts)
    mapkey("i", "<C-l>", [[<cmd>Telescope neorg insert_link<CR>]], "Insert link", opts)
    mapkey("n", "-", "<Plug>(neorg.qol.todo-items.todo.task-cycle)", "Cycle list item", opts)
  end,
})

api.nvim_create_user_command("MasonUpdateCustom", function()
  vim.cmd("MasonUpdate")

  api.nvim_exec2("doautocmd User MasonUpdateDone", {})
end, {})

api.nvim_create_autocmd("User", {
  desc = "Update TS parsers & Mason registries after Lazy install",
  group = group_lazy,
  pattern = "LazyInstall",
  callback = function()
    vim.cmd(":TSUpdate")
    vim.cmd(":MasonUpdateCustom")
  end,
})

api.nvim_create_autocmd("User", {
  desc = "Hide barbecue when entering DiffView",
  pattern = "DiffviewViewEnter",
  callback = function()
    require("barbecue.ui").toggle(false)
  end,
})

api.nvim_create_autocmd("User", {
  desc = "Hide barbecue when entering DiffView",
  pattern = "DiffviewViewLeave",
  callback = function()
    require("barbecue.ui").toggle(true)
  end,
})

vim.api.nvim_create_autocmd("User", {
  desc = "Update Mason packages only after MasonUpdate has been run",
  group = group_lazy,
  pattern = "MasonUpdateDone",
  callback = function()
    vim.cmd(":MasonToolsUpdate")
  end,
})

vim.api.nvim_create_autocmd("CursorMoved", {
  desc = "Hide regexplainer when the cursor moves",
  group = group_regexplainer,
  callback = function()
    require("regexplainer").hide()
  end,
})
