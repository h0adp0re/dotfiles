local option = vim.opt
local windowoption = vim.wo
local glyphs = require("constants.glyphs").vim

option.updatetime = 100 -- Set vim's updatetime to 100ms
option.undofile = true -- Keep an undo file (undo changes after closing)
option.sessionoptions:append({ "tabpages", "globals" }) -- Store tabpages and globals in session
option.clipboard:append("unnamed") -- Enable yanking to system clipboard
option.switchbuf:append("uselast") -- Load the quickfix and location list results in the last active window
option.completeopt = "" -- Disable built-in completion
option.mousemodel = "extend" -- Disable popup-menu on right-click

if vim.fn.has("mouse") == 1 then
  option.mouse:append("a") -- Mouse selections don't use xterm mode
end

-- Handle diffmode
if option.diff:get() then
  option.diffopt:remove("internal")
  option.diffopt:append("vertical")
end

-- Scrolling & wrapping
option.scrolloff = 8 -- Set cursor scroll offset
option.linebreak = true -- Set soft wrapping
option.wrap = true -- Turn on line wrapping
option.wrapmargin = 8 -- Wrap lines when coming within n characters from side

-- Case insensitive searching UNLESS /C or capital in search
option.ignorecase = true
option.smartcase = true

-- Appearance
option.guicursor = "n-v-i-c:block-Cursor"
option.termguicolors = true -- Enable true colors support
option.showmode = false -- Disable vim's statusline
option.colorcolumn = "80" -- Visual indicator at 80 col mark
windowoption.signcolumn = "yes" -- Always show the signcolumn
windowoption.number = true -- Show line numbers
-- option.background = "light" -- Background color | "dark", "light"

-- Fold behavior
windowoption.foldmethod = "expr"
windowoption.foldexpr = "nvim_treesitter#foldexpr()"
windowoption.foldnestmax = 4
windowoption.foldminlines = 1
windowoption.foldenable = false -- Disable automatic folding

-- Fold appearance
option.foldtext =
  [[substitute(getline(v:foldstart),'\\t',repeat('\ ',&tabstop),'g').'...'.trim(getline(v:foldend)) . '   ' . (v:foldend - v:foldstart + 1) . ' ⎬']]

-- Invisible characters
option.list = true -- Show invisible characters
option.listchars = glyphs.listchars
option.fillchars = glyphs.fillchars
option.showbreak = glyphs.showbreak -- Character at the start of lines that have been wrapped

-- Tabline
option.showtabline = 1 -- Show tabline when there are at least 2 tabs
option.tabline = "%!v:lua.custom_tabline()"
