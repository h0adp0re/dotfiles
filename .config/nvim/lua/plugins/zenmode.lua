local mapkey = require("utils").mapkey

mapkey("n", "<leader>Z", "<cmd>ZenMode<CR>")

return {
  {
    "folke/twilight.nvim",
    cmd = {
      "Twilight",
      "TwilightEnable",
      "TwilightDisable",
    },
  },
  {
    "folke/zen-mode.nvim",
    cmd = { "ZenMode" },
    opts = {
      window = {
        backdrop = 1,
      },
      plugins = {
        twilight = { enabled = true }, -- start Twilight when zen mode opens
        gitsigns = { enabled = true }, -- disable git signs
        tmux = { enabled = true }, -- disable the tmux statusline
        todo = { enabled = true }, -- disable todo-comments.nvim highlights
      },
      on_open = function()
        vim.opt.colorcolumn = "0"
      end,
      on_close = function()
        vim.opt.colorcolumn = "80"
      end,
    },
  },
}
