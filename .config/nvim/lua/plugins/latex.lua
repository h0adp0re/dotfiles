local mapkey = require("utils").mapkey

mapkey("n", "m", [[<cmd>lua require("nabla").popup({ border = "rounded" })<CR>]], "Render LaTeX")

return {
  "jbyuki/nabla.nvim",
}
