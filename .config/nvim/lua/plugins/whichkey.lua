local mapkey = require("utils").mapkey
local glyphs = require("constants.glyphs").whichkey

-- Native mappings {{{
mapkey("n", "<leader>k", "<cmd>noh<CR>", "Remove search highlighting")

-- Make Y consistent with D and C
mapkey("", "Y", "y$", "Yank until the end of the line", {})

mapkey("v", "K", ":m '<-2<CR>gv=gv", "Move highlighted lines up", {})
mapkey("v", "J", ":m '>+1<CR>gv=gv", "Move highlighted lines down", {})

mapkey("n", "<C-q>", "<Plug>(qf_qf_toggle)", "Toggle quickfix list", {})

mapkey("c", "<C-p>", "<Up>", "Move backwards in command history", {})
mapkey("c", "<C-n>", "<Down>", "Move forwards in command history", {})

-- Prevent the use of arrow keys in normal mode
mapkey("n", "<Up>", '<cmd>echoerr "Use k instead"<CR>')
mapkey("n", "<Down>", '<cmd>echoerr "Use j instead"<CR>')
mapkey("n", "<Right>", '<cmd>echoerr "Use l instead"<CR>')
mapkey("n", "<Left>", '<cmd>echoerr "Use h instead"<CR>')

-- Prevent the use of arrow keys in insert mode
mapkey("i", "<Up>", "<NOP>")
mapkey("i", "<Down>", "<NOP>")
mapkey("i", "<Right>", "<NOP>")
mapkey("i", "<Left>", "<NOP>")

-- Movement for operators e.g. `din(`
mapkey("o", "in(", "<cmd>normal! f(vi(<CR>", "In next parentheses")
-- Movement for operators e.g. `dil(`
mapkey("o", "il(", "<cmd>normal! F)vi(<CR>", "In previous parentheses")
-- Movement for operators e.g. `din"`
mapkey("o", 'in"', '<cmd>normal! f"vi"<CR>', "In next quotes")
-- Movement for operators e.g. `dil"`
mapkey("o", 'il"', '<cmd>normal! F"vi"<CR>', "In previous quotes")
-- }}}

-- +buffers {{{
mapkey("n", "<leader>bd", "<cmd>bp<bar>sp<bar>bn<bar>bd<CR>", "Close buffer without closing window")
mapkey("n", "<leader>bb", "<cmd>b#<CR>", "Go to previous buffer")
-- }}}

-- +diagnostic {{{
mapkey("n", "<leader>dT", "<cmd>Trouble todo toggle<CR>", "Todo")
mapkey("n", "<leader>dd", "<cmd>Trouble diagnostics toggle<CR>", "Diagnostic details")
mapkey("n", "<leader>di", "<cmd>Trouble lsp_incoming_calls toggle<CR>", "LSP incoming calls")
mapkey("n", "<leader>do", "<cmd>Trouble lsp toggle focus=true win.position=right win.size=60<CR>", "LSP symbols")
-- }}}

-- +debug {{{
mapkey("n", "<leader>Dq", [[<cmd>lua require("dap").terminate()<CR>]], "Terminate session")
mapkey("n", "<leader>DR", [[<cmd>lua require("dap").run_last()<CR>]], "Re-run last adapter")
mapkey("n", "<leader>Dc", [[<cmd>lua require("dap").run_to_cursor()<CR>]], "Run to cursor")
mapkey("n", "<leader>Dj", [[<cmd>lua require("dap").step_into()<CR>]], "Step into")
mapkey("n", "<leader>Dl", [[<cmd>lua require("dap").step_over()<CR>]], "Step over")
mapkey("n", "<leader>Dk", [[<cmd>lua require("dap").step_out()<CR>]], "Step out")
mapkey("n", "<leader>Dh", [[<cmd>lua require("dap").step_back()<CR>]], "Step back")
mapkey("n", "<leader>De", [[<cmd>lua require("dapui").eval()<CR>]], "Eval")
mapkey("n", "<leader>DA", [[<cmd>lua require("dapui").elements.watches.add()<CR>]], "Add watch")
mapkey("n", "<leader>Dp", [[<cmd>lua require("dap.ui.widgets").preview()<CR>]], "Preview")
mapkey("n", "<leader>Dr", [[<cmd>lua require("dap").repl.toggle()<CR>]], "Toggle REPL")
mapkey("n", "<leader>Dx", [[<cmd>lua require("dap").clear_breakpoints()<CR>]], "Clear breakpoints")
mapkey("n", "<leader>Db", [[<cmd>lua require("dap").toggle_breakpoint()<CR>]], "Toggle breakpoint")
mapkey("n", "<leader>DE", [[<cmd>lua require("dap").set_exception_breakpoints()<CR>]], "Set exception breakpoint")
mapkey(
  "n",
  "<leader>DB",
  [[<cmd>lua require("dap").toggle_breakpoint(vim.fn.input('Condition: '))<CR>]],
  "Toggle breakpoint | condition"
)
mapkey(
  "n",
  "<leader>DL",
  [[<cmd>lua require("dap").toggle_breakpoint(nil, nil, vim.fn.input('Log Message: '))<CR>]],
  "Toggle breakpoint | log point"
)
mapkey("n", "<leader>Du", [[<cmd>lua require("dapui").toggle()<CR>]], "UI")
-- }}}

-- +explorer {{{
mapkey("n", "<leader>ee", "<cmd>Lexplore<CR>", "netrw")
mapkey("n", "<leader>em", "<cmd>Neomutt<CR>", "neomutt")
mapkey("n", "<leader>en", "<cmd>Nnn %:p:h<CR>", "nnn")
mapkey("n", "<leader>er", "<cmd>Ranger %:p:h<CR>", "ranger")
mapkey("n", "<leader>et", "<cmd>TaskWarriorTUI<CR>", "TaskWarrior")
mapkey("n", "<leader>ev", "<cmd>Vifm<CR>", "vifm")
-- }}}

-- +find {{{
mapkey("n", "<leader>fU", "<cmd>UrlView lazy<CR>", "URLs of plugins")
mapkey("n", "<leader>fu", "<cmd>UrlView<CR>", "URLs in buffer")
-- }}}

-- +insert {{{
mapkey("n", "<leader>ii", "<Plug>SidewaysArgumentInsertBefore", "Insert before current list item", {})
mapkey("n", "<leader>ia", "<Plug>SidewaysArgumentAppendAfter", "Insert after current list item", {})
mapkey("n", "<leader>iI", "<Plug>SidewaysArgumentInsertFirst", "Insert as first list item", {})
mapkey("n", "<leader>iA", "<Plug>SidewaysArgumentAppendLast", "Insert as last list item", {})
-- }}}

-- +list {{{
mapkey("n", "<leader>lr", "<cmd>AutolistRecalculate<CR>", "Recalculate list")
mapkey("n", "<leader>lj", "<cmd>AutolistCycleNext<CR>", "Cycle list type")
-- }}}

-- +move {{{
mapkey("n", "<leader>mj", "<cmd>lua MiniSplitjoin.join()<CR>", "Join a block into a single-line statement")
mapkey("n", "<leader>mk", "<cmd>lua MiniSplitjoin.split()<CR>", "Split a one-liner into multiple lines")
mapkey("n", "<leader>mh", "<cmd>SidewaysLeft<CR>", "Move item left")
mapkey("n", "<leader>ml", "<cmd>SidewaysRight<CR>", "Move item right")
-- }}}

-- +navigate {{{
mapkey("n", "<leader>nh", "<cmd>SidewaysJumpLeft<CR>", "Move left by one list item")
mapkey("n", "<leader>nl", "<cmd>SidewaysJumpRight<CR>", "Move right by one list item")
-- }}}

-- +docs {{{
mapkey("n", "<leader>qg", "<cmd>Neogen<CR>", "Generate docs")
-- }}}

-- +refactor {{{
mapkey({ "x", "n" }, "<leader>rs", [[<cmd>lua require("refactoring").select_refactor()<CR>]], "Select refactoring")
mapkey("n", "<leader>rP", [[<cmd>lua require("refactoring").debug.printf({ below = false })<CR>]], "Insert print above")
mapkey("n", "<leader>rp", [[<cmd>lua require("refactoring").debug.printf()<CR>]], "Insert print below")
mapkey({ "x", "n" }, "<leader>rv", [[<cmd>lua require("refactoring").debug.print_var()<CR>]], "Print variable")
mapkey("n", "<leader>rc", [[<cmd>lua require("refactoring").debug.cleanup()<CR>]], "Cleanup prints")
mapkey("n", "<leader>rr", '<cmd>%s//\\=@"/g<CR>', "Replace last searched with last yanked")
mapkey(
  "n",
  "<leader>rw",
  ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>",
  "Start searching & replacing word under cursor"
)
-- }}}

-- +scratch {{{
mapkey("n", "<leader>sn", [[<cmd>lua require("attempt").new_select()<CR>]], "New buffer, select extension")
mapkey("n", "<leader>si", [[<cmd>lua require("attempt").new_input_ext()<CR>]], "New buffer, input extension")
mapkey("n", "<leader>sr", [[<cmd>lua require("attempt").run()<CR>]], "Run buffer")
mapkey("n", "<leader>sd", [[<cmd>lua require("attempt").delete_buf()<CR>]], "Delete buffer")
mapkey("n", "<leader>sc", [[<cmd>lua require("attempt").rename_buf()<CR>]], "Rename buffer")
-- map_lua("n", "<leader>sl", [[<cmd>lua require("attempt").open_select()<CR>]], "List buffers")
mapkey("n", "<leader>sl", [[<cmd>Telescope attempt<CR>]], "List buffers")
-- }}}

-- +window {{{
mapkey("n", "<leader>w2", "<C-W>v", "layout-double-columns")
mapkey("n", "<leader>w=", "<C-W>=", "balance-window")
mapkey("n", "<leader>w?", "<cmd>Windows<CR>", "fzf-window")
mapkey("n", "<leader>wH", "<C-W>5<", "expand-window-left")
mapkey("n", "<leader>wJ", "<cmd>resize +5<CR>", "expand-window-below")
mapkey("n", "<leader>wK", "<cmd>resize -5<CR>", "expand-window-up")
mapkey("n", "<leader>wL", "<C-W>5>", "expand-window-right")
mapkey("n", "<leader>w_", "<C-W>s", "split-window-below")
mapkey("n", "<leader>wd", "<C-W>c", "delete-window")
mapkey("n", "<leader>wh", "<C-W>h", "window-left")
mapkey("n", "<leader>wj", "<C-W>j", "window-below")
mapkey("n", "<leader>wk", "<C-W>k", "window-up")
mapkey("n", "<leader>wl", "<C-W>l", "window-right")
mapkey("n", "<leader>wq", "<cmd>wqa<CR>", "Write all buffers and [q]uit")
mapkey("n", "<leader>ws", "<C-W>s", "split-window-up")
mapkey("n", "<leader>wt", "<cmd>tabclose<CR>", "Close tab")
mapkey("n", "<leader>wv", "<C-W>v", "split-window-left")
mapkey("n", "<leader>ww", "<C-W>w", "other-window")
mapkey("n", "<leader>w|", "<C-W>v", "split-window-right")
-- }}}

-- +yank {{{
mapkey("n", "<leader>yc", "<cmd>YankyClearHistory<CR>", "Clear yank history", {})
mapkey("n", "<leader>yh", "<cmd>YankyRingHistory<CR>", "Yank history", {})
mapkey("n", "gp", function()
  vim.api.nvim_feedkeys("`[" .. vim.fn.strpart(vim.fn.getregtype(), 0, 1) .. "`]", "n", false)
end, "Switch to VISUAL using last paste", {})
-- }}}

-- +fold {{{
mapkey("n", "<leader>zf", "$zf%", "Fold to matching pair")
-- }}}

return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  opts = {
    -- preset = "helix",
    spec = {
      { "<leader>A", group = "ChatGPT" },
      { "<leader>b", group = "buffers" },
      { "<leader>d", group = "diagnostics" },
      { "<leader>D", group = "debug" },
      { "<leader>e", group = "explorer" },
      { "<leader>f", group = "find" },
      { "<leader>g", group = "git" },
      { "<leader>h", group = "hunk" },
      { "<leader>i", group = "insert" },
      { "<leader>j", group = "neorg" },
      { "<leader>jf", group = "telescope" },
      { "<leader>l", group = "list" },
      { "<leader>m", group = "move" },
      { "<leader>n", group = "navigate" },
      { "<leader>p", group = "harpoon" },
      { "<leader>q", group = "docs" },
      { "<leader>r", group = "refactor" },
      { "<leader>s", group = "scratch" },
      { "<leader>t", group = "test" },
      { "<leader>w", group = "window" },
      { "<leader>x", group = "duck" },
      { "<leader>y", group = "yank" },
      { "<leader>z", group = "fold" },
    },
    win = {
      padding = { 1, 1 }, -- extra window padding [top/bottom, right/left]
      wo = {
        winblend = 0,
      },
    },
    layout = {
      height = { min = 40, max = 80 }, -- min and max height of the columns
      width = { min = 30, max = 50 }, -- min and max width of the columns
      spacing = 3, -- spacing between columns
    },
    triggers = {
      { "<auto>", mode = "nixsotc" },
      { "a", mode = { "n", "v" } },
    },
    icons = glyphs,
    show_help = true, -- show help message on the command line when the popup is visible
  },
}
