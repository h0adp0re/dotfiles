local global = vim.g
local mapkey = require("utils").mapkey
local glyphs = require("constants.glyphs")

-- Merge conflict resolution
mapkey("n", "<C-l>", function()
  return vim.opt.diff:get() and "<cmd>diffget //2<CR>" or "<C-l>"
end, "diff from OURS", { expr = true })
mapkey("n", "<C-h>", function()
  return vim.opt.diff:get() and "<cmd>diffget //3<CR>" or "<C-h>"
end, "diff from THEIRS", { expr = true })

-- Diff navigation
mapkey("n", "<C-j>", function()
  return vim.opt.diff:get() and "]c" or "<Plug>(qf_qf_next)"
end, "Next hunk / qf entry", { expr = true })
mapkey("n", "<C-k>", function()
  return vim.opt.diff:get() and "[c" or "<Plug>(qf_qf_previous)"
end, "Previous hunk / qf entry", { expr = true })

mapkey("n", "<leader>gg", "<cmd>Git<CR>", "Status window")
mapkey("n", "<leader>gb", "<cmd>Twiggy<CR>", "Branches")
mapkey("n", "<leader>gB", "<cmd>Git blame<CR>", "Blame")
mapkey("n", "<leader>gw", "<cmd>Gwrite<CR>", "Write and stage file", {})
mapkey("n", "<leader>gp", "<cmd>Git push<CR>", "Push")
mapkey("n", "<leader>gP", "<cmd>Git push --force-with-lease<CR>", "Push --force-with-lease")
mapkey("n", "<leader>gl", "<cmd>Git pull<CR>", "Pull")
mapkey("n", "<leader>gL", "<cmd>Git log %<CR>", "Log for buffer")
mapkey("n", "<leader>go", "<cmd>GBrowse<CR>", "Open file in repo")
mapkey("v", "<leader>go", "<cmd>'<,'>GBrowse<CR>", "Open line(s) in repo")
mapkey("n", "<leader>gd", "<cmd>DiffviewOpen HEAD<CR>", "Diff HEAD")
mapkey("n", "<leader>gh", "<cmd>DiffviewFileHistory %<CR>", "File history")
mapkey("n", "<leader>gm", "<cmd>DiffviewOpen<CR>", "Merge conflicts")

--[[ fugitive buffer maps
  Staging/unstaging maps

  s         Stage (add) the file or hunk under the cursor.
  u         Unstage (reset) the file or hunk under the cursor.
  - / a     Stage or unstage the file or hunk under the cursor.
  U         Unstage everything.
  X         Discard the change under the cursor.  This uses
  =         Toggle an inline diff of the file under the cursor.

  Diff maps

  dd        Perform a |:Gdiffsplit| on the file under the cursor.
  dv        Perform a |:Gvdiffsplit| on the file under the cursor.
  ds / dh   Perform a |:Ghdiffsplit| on the file under the cursor.
  dq        Close all but one diff buffer, and |:diffoff|! the

  Navigation maps

  (         Jump to the previous file, hunk, or revision.
  )         Jump to the next file, hunk, or revision.
  i         Jump to the next file or hunk, expanding inline diffs
  gu        Jump to file [count] in the "Untracked" or "Unstaged"
  gU        Jump to file [count] in the "Unstaged" section.
  gs        Jump to file [count] in the "Staged" section.
  gp        Jump to file [count] in the "Unpushed" section.
  gP        Jump to file [count] in the "Unpulled" section.
  gr        Jump to file [count] in the "Rebasing" section.

  Commit maps

  cc        Create a commit.
  ca        Amend the last commit and edit the message.
  ce        Amend the last commit without editing the message.
  cw        Reword the last commit.
  cvc       Create a commit with -v.
  cva       Amend the last commit with -v
  c<Space>  Populate command line with ":Git commit ".
]]

return {
  {
    "https://tpope.io/vim/fugitive.git",
    lazy = false,
  },
  {
    "kmARC/vim-fubitive",
    lazy = false,
  },
  {
    "shumphrey/fugitive-gitlab.vim",
    lazy = false,
  },
  {
    "NeogitOrg/neogit",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "nvim-telescope/telescope.nvim",
    },
    cmd = {
      "Neogit",
      "NeogitCommit",
      "NeogitLogCurrent",
      "NeogitResetState",
    },
    opts = {
      disable_context_highlighting = true,
      graph_style = "unicode",
      signs = glyphs.neogit,
    },
  },
  {
    "sodapopcan/vim-twiggy",
    cmd = { "Twiggy" },
    init = function()
      global.twiggy_group_locals_by_slash = 0
      global.twiggy_local_branch_sort = "mru"
      global.twiggy_remote_branch_sort = "date"
    end,
  },
  {
    "kristijanhusak/vim-create-pr",
    lazy = false,
  },
  {
    "lewis6991/foldsigns.nvim",
    lazy = false,
  },
  {
    "lewis6991/gitsigns.nvim",
    event = "VeryLazy",
    opts = {
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns
        local nngs = require("nvim-next.integrations").gitsigns(gs)

        local function map(mode, lhs, rhs, desc, opts)
          opts = opts or { noremap = true, silent = true }
          opts.buffer = bufnr

          mapkey(mode, lhs, rhs, desc, opts)
        end

        -- Navigation
        map("n", "<leader>hj", function()
          if vim.wo.diff then
            return "<leader>hj"
          end
          vim.schedule(function()
            nngs.next_hunk()
          end)
          return "<Ignore>"
        end, "Next hunk", { expr = true })

        map("n", "<leader>hk", function()
          if vim.wo.diff then
            return "<leader>hk"
          end
          vim.schedule(function()
            nngs.prev_hunk()
          end)
          return "<Ignore>"
        end, "Previous hunk", { expr = true })

        -- Actions
        map("v", "<leader>hs", function()
          gs.stage_hunk({ vim.fn.line("."), vim.fn.line("v") })
        end)
        map("n", "<leader>hs", gs.stage_hunk, "Stage hunk")
        map({ "n", "v" }, "<leader>hr", gs.reset_hunk, "Reset hunk")
        map("n", "<leader>hS", gs.stage_buffer, "Stage buffer")
        map("n", "<leader>hu", ":echoerr 'Use `stage_hunk` on staged lines.'<CR>", "Undo stage hunk")
        map("n", "<leader>hR", gs.reset_buffer, "Reset buffer")
        map("n", "<leader>hp", gs.preview_hunk, "Preview hunk (float)")
        map("n", "<leader>hP", gs.preview_hunk_inline, "Preview hunk (inline)")
        map("n", "<leader>hb", function()
          gs.blame_line({ full = true })
        end, "Blame line")
        map("n", "<leader>hB", gs.toggle_current_line_blame, "Toggle blame line (virtual text)")
        map("n", "<leader>hd", gs.diffthis, "Diff buffer against HEAD")

        -- Text object
        map({ "o", "x" }, "ih", ":<C-u>Gitsigns select_hunk<CR>", "Inner hunk")
      end,
      signs = glyphs.gitsigns,
      signs_staged = glyphs.gitsigns,
      signs_staged_enable = true,
      signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
      numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
      linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
      word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
      watch_gitdir = {
        interval = 1000,
        follow_files = true,
      },
      attach_to_untracked = true,
      current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
      current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = "right_align", -- 'eol' | 'overlay' | 'right_align'
        delay = 0,
        ignore_whitespace = false,
      },
      current_line_blame_formatter = "<author>, <author_time:%Y-%m-%d> - <summary>",
      sign_priority = 6,
      update_debounce = 100,
      status_formatter = nil, -- Use default
      max_file_length = 40000, -- Disable if file is longer than this (in lines)
      preview_config = {
        -- Options passed to nvim_open_win
        border = "rounded",
        style = "minimal",
        relative = "cursor",
        row = 0,
        col = 1,
      },
    },
  },
  {
    "sindrets/diffview.nvim",
    lazy = false,
    cmd = {
      "Diffview",
      "DiffviewOpen",
      "DiffviewLog",
      "DiffviewClose",
      "DiffviewRefresh",
      "DiffviewFocusFiles",
      "DiffviewFileHistory",
      "DiffviewToggleFiles",
    },
    opts = function()
      local actions = require("diffview.actions")

      return {
        diff_binaries = false, -- Show diffs for binaries
        enhanced_diff_hl = true, -- See ':h diffview-config-enhanced_diff_hl'
        git_cmd = { "git" }, -- The git executable followed by default args.
        use_icons = false, -- Requires nvim-web-devicons
        signs = glyphs.diffview,
        view = {
          -- Configure the layout and behavior of different types of views.
          -- Available layouts:
          --  'diff1_plain'
          --    |'diff2_horizontal'
          --    |'diff2_vertical'
          --    |'diff3_horizontal'
          --    |'diff3_vertical'
          --    |'diff3_mixed'
          --    |'diff4_mixed'
          -- For more info, see ':h diffview-config-view.x.layout'.
          default = {
            -- Config for changed files, and staged files in diff views.
            layout = "diff2_horizontal",
          },
          merge_tool = {
            -- Config for conflicted files in diff views during a merge or rebase.
            layout = "diff3_mixed",
            disable_diagnostics = true, -- Temporarily disable diagnostics for conflict buffers while in the view.
          },
          file_history = {
            -- Config for changed files in file history views.
            layout = "diff2_horizontal",
          },
        },
        file_panel = {
          listing_style = "list", -- One of 'list' or 'tree'
          tree_options = { -- Only applies when listing_style is 'tree'
            flatten_dirs = true, -- Flatten dirs that only contain one single dir
            folder_statuses = "only_folded", -- One of 'never', 'only_folded' or 'always'.
          },
          win_config = { -- See ':h diffview-config-win_config'
            position = "left",
            width = 35,
            win_opts = {},
          },
        },
        file_history_panel = {
          log_options = { -- See ':h diffview-config-log_options'
            git = {
              single_file = {
                diff_merges = "combined",
              },
              multi_file = {
                diff_merges = "first-parent",
              },
            },
          },
          win_config = { -- See ':h diffview-config-win_config'
            position = "bottom",
            height = 16,
            win_opts = {},
          },
        },
        commit_log_panel = {
          win_config = { -- See ':h diffview-config-win_config'
            win_opts = {},
          },
        },
        default_args = { -- Default args prepended to the arg-list for the listed commands
          DiffviewOpen = {},
          DiffviewFileHistory = {},
        },
        hooks = {}, -- See ':h diffview-config-hooks'
        keymaps = {
          disable_defaults = true, -- Disable the default keymaps
          view = {
            -- The `view` bindings are active in the diff buffers, only when the current
            -- tabpage is a Diffview.
            { "n", "<Tab>", actions.select_next_entry, { desc = "Open the diff for the next file" } },
            { "n", "<S-Tab>", actions.select_prev_entry, { desc = "Open the diff for the previous file" } },
            { "n", "gf", actions.goto_file, { desc = "Open the file in a new split in the previous tabpage" } },
            { "n", "<C-w><C-f>", actions.goto_file_split, { desc = "Open the file in a new split" } },
            { "n", "<C-w>gf", actions.goto_file_tab, { desc = "Open the file in a new tabpage" } },
            { "n", "<leader>e", actions.focus_files, { desc = "Bring focus to the file panel" } },
            { "n", "<leader>b", actions.toggle_files, { desc = "Toggle the file panel" } },
            { "n", "g<C-x>", actions.cycle_layout, { desc = "Cycle through available layouts" } },
            { "n", "<C-k>", actions.prev_conflict, { desc = "In the merge-tool: jump to the previous conflict" } },
            { "n", "<C-j>", actions.next_conflict, { desc = "In the merge-tool: jump to the next conflict" } },
            { "n", "<leader>co", actions.conflict_choose("ours"), { desc = "Choose the OURS version of a conflict" } },
            {
              "n",
              "<leader>ct",
              actions.conflict_choose("theirs"),
              { desc = "Choose the THEIRS version of a conflict" },
            },
            { "n", "<leader>cb", actions.conflict_choose("base"), { desc = "Choose the BASE version of a conflict" } },
            { "n", "<leader>ca", actions.conflict_choose("all"), { desc = "Choose all the versions of a conflict" } },
            { "n", "dx", actions.conflict_choose("none"), { desc = "Delete the conflict region" } },
          },
          diff1 = {
            -- Mappings in single window diff layouts
            { "n", "g?", actions.help({ "view", "diff1" }), { desc = "Open the help panel" } },
          },
          diff2 = {
            -- Mappings in 2-way diff layouts
            { "n", "g?", actions.help({ "view", "diff2" }), { desc = "Open the help panel" } },
          },
          diff3 = {
            -- Mappings in 3-way diff layouts
            {
              { "n", "x" },
              "<C-l>",
              actions.diffget("ours"),
              { desc = "Obtain the diff hunk from the OURS version of the file" },
            },
            {
              { "n", "x" },
              "<C-h>",
              actions.diffget("theirs"),
              { desc = "Obtain the diff hunk from the THEIRS version of the file" },
            },
            { "n", "g?", actions.help({ "view", "diff3" }), { desc = "Open the help panel" } },
          },
          diff4 = {
            -- Mappings in 4-way diff layouts
            {
              { "n", "x" },
              "1do",
              actions.diffget("base"),
              { desc = "Obtain the diff hunk from the BASE version of the file" },
            },
            {
              { "n", "x" },
              "2do",
              actions.diffget("ours"),
              { desc = "Obtain the diff hunk from the OURS version of the file" },
            },
            {
              { "n", "x" },
              "3do",
              actions.diffget("theirs"),
              { desc = "Obtain the diff hunk from the THEIRS version of the file" },
            },
            { "n", "g?", actions.help({ "view", "diff4" }), { desc = "Open the help panel" } },
          },
          file_panel = {
            { "n", "j", actions.next_entry, { desc = "Bring the cursor to the next file entry" } },
            { "n", "k", actions.prev_entry, { desc = "Bring the cursor to the previous file entry" } },
            { "n", "<CR>", actions.select_entry, { desc = "Open the diff for the selected entry" } },
            { "n", "a", actions.toggle_stage_entry, { desc = "Stage / unstage the selected entry" } },
            { "n", "S", actions.stage_all, { desc = "Stage all entries" } },
            { "n", "U", actions.unstage_all, { desc = "Unstage all entries" } },
            { "n", "X", actions.restore_entry, { desc = "Restore entry to the state on the left side" } },
            { "n", "R", actions.refresh_files, { desc = "Update stats and entries in the file list" } },
            { "n", "L", actions.open_commit_log, { desc = "Open the commit log panel" } },
            { "n", "gf", actions.goto_file, { desc = "Open file" } },
            { "n", "<C-w>t", actions.goto_file_tab, { desc = "Open file in a new tab" } },
            { "n", "i", actions.listing_style, { desc = "Toggle between 'list' and 'tree' views" } },
            { "n", "f", actions.toggle_flatten_dirs, { desc = "Flatten empty subdirectories in tree listing style" } },
            { "n", "<leader>e", actions.focus_files, { desc = "Bring focus to the file panel" } },
            { "n", "<leader>b", actions.toggle_files, { desc = "Toggle the file panel" } },
            { "n", "g<C-x>", actions.cycle_layout, { desc = "Cycle available layouts" } },
            { "n", "<C-k>", actions.prev_conflict, { desc = "In the merge-tool: jump to the previous conflict" } },
            { "n", "<C-j>", actions.next_conflict, { desc = "In the merge-tool: jump to the next conflict" } },
            { "n", "g?", actions.help("file_panel"), { desc = "Open the help panel" } },
            { "n", "<leader>co", actions.conflict_choose("ours"), { desc = "Choose the OURS version of a conflict" } },
            {
              "n",
              "<leader>ct",
              actions.conflict_choose("theirs"),
              { desc = "Choose the THEIRS version of a conflict" },
            },
            { "n", "<leader>cb", actions.conflict_choose("base"), { desc = "Choose the BASE version of a conflict" } },
            { "n", "<leader>ca", actions.conflict_choose("all"), { desc = "Choose all the versions of a conflict" } },
            { "n", "dx", actions.conflict_choose("none"), { desc = "Delete the conflict region" } },
          },
          file_history_panel = {
            { "n", "g!", actions.options, { desc = "Open the option panel" } },
            { "n", "<C-A-d>", actions.open_in_diffview, { desc = "Open the entry under the cursor in a diffview" } },
            { "n", "y", actions.copy_hash, { desc = "Copy the commit hash of the entry under the cursor" } },
            { "n", "L", actions.open_commit_log, { desc = "Show commit details" } },
            { "n", "zR", actions.open_all_folds, { desc = "Expand all folds" } },
            { "n", "zM", actions.close_all_folds, { desc = "Collapse all folds" } },
            { "n", "j", actions.next_entry, { desc = "Bring the cursor to the next file entry" } },
            { "n", "k", actions.prev_entry, { desc = "Bring the cursor to the previous file entry." } },
            { "n", "<CR>", actions.select_entry, { desc = "Open the diff for the selected entry." } },
            { "n", "o", actions.select_entry, { desc = "Open the diff for the selected entry." } },
            { "n", "<C-b>", actions.scroll_view(-0.25), { desc = "Scroll the view up" } },
            { "n", "<C-f>", actions.scroll_view(0.25), { desc = "Scroll the view down" } },
            { "n", "<Tab>", actions.select_next_entry, { desc = "Open the diff for the next file" } },
            { "n", "<S-Tab>", actions.select_prev_entry, { desc = "Open the diff for the previous file" } },
            { "n", "gf", actions.goto_file, { desc = "Open the file in a new split in the previous tabpage" } },
            { "n", "<C-w><C-f>", actions.goto_file_split, { desc = "Open the file in a new split" } },
            { "n", "<C-w>gf", actions.goto_file_tab, { desc = "Open the file in a new tabpage" } },
            { "n", "<leader>e", actions.focus_files, { desc = "Bring focus to the file panel" } },
            { "n", "<leader>b", actions.toggle_files, { desc = "Toggle the file panel" } },
            { "n", "g<C-x>", actions.cycle_layout, { desc = "Cycle available layouts" } },
            { "n", "g?", actions.help("file_history_panel"), { desc = "Open the help panel" } },
          },
          option_panel = {
            { "n", "<Tab>", actions.select_entry, { desc = "Change the current option" } },
            { "n", "q", actions.close, { desc = "Close the panel" } },
            { "n", "g?", actions.help("option_panel"), { desc = "Open the help panel" } },
          },
          help_panel = {
            { "n", "q", actions.close, { desc = "Close help menu" } },
            { "n", "<Esc>", actions.close, { desc = "Close help menu" } },
          },
        },
      }
    end,
  },
}
