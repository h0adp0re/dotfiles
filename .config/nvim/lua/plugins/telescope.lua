local glyphs = require("constants.glyphs").telescope

return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  cmd = "Telescope",
  dependencies = {
    "nvim-lua/plenary.nvim",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },
    "debugloop/telescope-undo.nvim",
  },
  config = function()
    local telescope = require("telescope")
    local sorters = require("telescope.sorters")
    local previewers = require("telescope.previewers")
    local actions = require("telescope.actions")
    local actions_layout = require("telescope.actions.layout")
    local transform_mod = require("telescope.actions.mt").transform_mod
    local custom_pickers = require("plugins.config.telescope_pickers")
    local mapkey = require("utils").mapkey

    local exit_insert_mode = transform_mod({
      x = function()
        vim.cmd([[stopinsert]])
      end,
    })

    telescope.setup({
      defaults = {
        prompt_prefix = glyphs.prompt_prefix,
        selection_caret = glyphs.selection_caret,
        file_sorter = sorters.get_fzy_sorter,
        generic_sorter = sorters.get_fzy_sorter,
        file_previewer = previewers.vim_buffer_cat.new,
        file_ignore_patterns = {
          ".git/",
          ".cache",
          "%.gif",
          "%.png",
          "%.jpg",
          "%.jpeg",
          "%.pdf",
          "%.mkv",
          "%.mp4",
          "%.zip",
        },
        vimgrep_arguments = {
          "rg",
          "--color=never",
          "--no-heading",
          "--with-filename",
          "--line-number",
          "--column",
          "--smart-case",
          "--hidden",
        },
        mappings = {
          i = {
            ["<CR>"] = actions.select_default + exit_insert_mode,
            ["<Esc>"] = actions.close + exit_insert_mode,
            ["<C-k>"] = actions.move_selection_previous,
            ["<C-j>"] = actions.move_selection_next,
            ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
            ["<C-u>"] = actions.preview_scrolling_up,
            ["<C-d>"] = actions.preview_scrolling_down,
            ["<C-p>"] = actions_layout.toggle_preview,
            ["<C-n>"] = false,
            ["<C-h>"] = "which_key",
          },
          n = {
            ["<CR>"] = actions.select_default,
            ["<Esc>"] = actions.close,
            ["<C-k>"] = actions.move_selection_previous,
            ["<C-j>"] = actions.move_selection_next,
            ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
            ["<C-u>"] = actions.preview_scrolling_up,
            ["<C-d>"] = actions.preview_scrolling_down,
            ["<C-p>"] = actions_layout.toggle_preview,
            ["<C-n>"] = false,
            ["<C-h>"] = "which_key",
          },
        },
      },
      pickers = {
        live_grep = {
          mappings = {
            i = {
              ["<C-e>"] = function(bufnr)
                custom_pickers.picker_actions.set_extension(bufnr, "live_grep")
              end,
              ["<C-f>"] = function(bufnr)
                custom_pickers.picker_actions.set_folders(bufnr, "live_grep")
              end,
            },
          },
        },
        grep_string = {
          mappings = {
            i = {
              ["<C-e>"] = function(bufnr)
                custom_pickers.picker_actions.set_extension(bufnr, "grep_string")
              end,
              ["<C-f>"] = function(bufnr)
                custom_pickers.picker_actions.set_folders(bufnr, "grep_string")
              end,
            },
          },
        },
        git_status = {
          git_icons = glyphs.git_icons,
        },
      },
      extensions = {
        fzf = {
          fuzzy = true, -- false will only do exact matching
          override_generic_sorter = true, -- override the generic sorter
          override_file_sorter = true, -- override the file sorter
          case_mode = "smart_case", -- or "ignore_case" or "respect_case"
          -- the default case_mode is "smart_case"
        },
      },
    })

    -- To get fzf loaded and working with telescope, you need to call
    -- load_extension, somewhere after setup function:
    telescope.load_extension("fzf")
    telescope.load_extension("attempt")
    telescope.load_extension("notify")
    telescope.load_extension("yank_history")
    telescope.load_extension("undo")

    -- List all buffers
    mapkey("n", "<leader>fb", function()
      custom_pickers.all_buffers()
    end, "Open buffers")
    -- Find files in dotfiles
    mapkey("n", "<leader>fd", function()
      custom_pickers.search_dotfiles()
    end, "Dotfiles")
    -- Search files in the project
    mapkey("n", "<leader>ff", function()
      custom_pickers.project_files()
    end, "Files in CWD")
    mapkey("n", "<leader>fF", function()
      custom_pickers.find_siblings()
    end, "Sibling files")
    -- Command palette
    mapkey("n", "<leader>fc", "<cmd>Telescope commands<CR>", "Commands")
    -- Undo history
    mapkey("n", "<leader>fh", "<cmd>Telescope undo<CR>", "Undo history")
    -- Search lines in current buffer
    mapkey("n", "<leader>fl", "<cmd>Telescope current_buffer_fuzzy_find<CR>", "Lines in current buffer")
    -- Search the project
    mapkey("n", "<leader>fp", function()
      custom_pickers.live_grep()
    end, "Lines in project")
    -- Search symbols
    mapkey("n", "<leader>fs", "<cmd>Telescope lsp_document_symbols<CR>", "Symbols")
    -- Search keymaps
    mapkey("n", "<leader>fk", "<cmd>Telescope keymaps<CR>", "Keymaps")
    -- Search notifications
    mapkey("n", "<leader>fn", "<cmd>Telescope notify<CR>", "Notifications")
    -- Search yank history
    mapkey("n", "<leader>fy", "<cmd>Telescope yank_history<CR>", "Yank history")
    -- Search for string under cursor
    mapkey("n", "<leader>fw", function()
      custom_pickers.grep_string()
    end, "Word under cursor")
    -- List commits for current file
    mapkey("n", "<leader>gc", "<cmd>Telescope git_bcommits<CR>", "Commits for current file")
    -- Git status
    mapkey("n", "<leader>gs", "<cmd>Telescope git_status<CR>", "Git status")
    -- Vim help tags
    mapkey("n", "<leader>fv", "<cmd>Telescope help_tags<CR>", "Help tags")
  end,
}
