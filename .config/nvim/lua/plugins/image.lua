return {
  "samodostal/image.nvim",
  event = "VeryLazy",
  opts = {
    render = {
      min_padding = 3,
      show_label = true,
      use_dither = true,
    },
    events = {
      update_on_nvim_resize = true,
    },
  },
}
