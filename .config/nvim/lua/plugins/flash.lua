return {
  "folke/flash.nvim",
  event = "VeryLazy",
  opts = {
    jump = {
      autojump = true,
    },
    highlight = { backdrop = false },
    modes = {
      -- options used when flash is activated through
      -- a regular search with `/` or `?`
      search = {
        enabled = false,
      },
      -- options used when flash is activated through
      -- `f`, `F`, `t`, `T`, `;` and `,` motions
      char = {
        enabled = false,
      },
      -- options used for treesitter selections
      -- `require("flash").treesitter()`
      treesitter = {
        label = {
          rainbow = {
            enabled = true,
            shade = 3,
          },
        },
      },
    },
  },
  keys = {
    {
      "s",
      mode = { "n", "x", "o" },
      "<cmd>lua require('flash').jump()<CR>",
      desc = "Flash",
    },
    {
      "T",
      mode = { "n", "x", "o" },
      "<cmd>lua require('flash').treesitter()<CR>",
      desc = "Flash Treesitter",
    },
  },
}
