local api = vim.api
local lsp = vim.lsp
local augroup = api.nvim_create_augroup("LspFormatting", {})
local mapkey = require("utils").mapkey
local glyphs = require("constants.glyphs")

mapkey(
  "n",
  "<leader>H",
  [[<cmd>lua vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())<CR>]],
  "Toggle inlay hints"
)

mapkey("n", "<leader>B", [[<cmd>lua require("barbecue.ui").toggle()<CR>]], "Toggle breadcrumbs")

local on_attach = function(client, bufnr)
  -- Enable completion triggered by <C-x><C-o>
  api.nvim_set_option_value("omnifunc", "v:lua.vim.lsp.omnifunc", { buf = bufnr })

  local function map(mode, lhs, rhs, desc, opts)
    opts = opts or { noremap = true, silent = true }
    opts.buffer = bufnr

    mapkey(mode, lhs, rhs, desc, opts)
  end

  -- LSP Mappings.
  -- INFO: `:help vim.lsp.buf.*`
  map("n", "gd", lsp.buf.definition, "Jump to the definition of the symbol under the cursor")
  map("n", "gD", lsp.buf.type_definition, "Jump to the type definition of the symbol under the cursor")
  if client.supports_method("textDocument/hover") then
    map("n", "K", lsp.buf.hover, "Display hover information about the symbol under the cursor in a floating window")
  end
  map("n", "gs", lsp.buf.document_symbol, "List all symbols in the current buffer in the qf window")
  map(
    "n",
    "gI",
    lsp.buf.implementation,
    "List all the implementations for the symbol under the cursor in the qf window"
  )
  -- map("n", "<C-h>", lsp.buf.signature_help, "Display signature information about the symbol under the cursor in a floating window")
  map("n", "<leader>rn", lsp.buf.rename, "Rename all references")
  map("n", "<leader>a", lsp.buf.code_action, "Code action")
  map("n", "gr", lsp.buf.references, "List all the references to the symbol under the cursor in the quickfix window")

  -- Shellcheck diagnostics are provided by both bashls and shellcheck.
  if client.name == "bashls" then
    ---@diagnostic disable-next-line: duplicate-set-field
    lsp.handlers["textDocument/publishDiagnostics"] = function() end
  end

  -- -- Opt-out of semantic token highlighting by LSP client.
  -- if client.server_capabilities.semanticTokensProvider then
  --   client.server_capabilities.semanticTokensProvider = nil
  -- end

  -- Disable formatting by LSPs
  if
    client.name == "ts_ls"
    or client.name == "stylelint_lsp"
    or client.name == "sumneko_lua"
    or client.name == "volar"
  then
    -- Approach 1
    -- client.server_capabilities.documentFormattingProvider = false
    -- client.server_capabilities.documentRangeFormattingProvider = false

    -- Approach 2
    -- local util = require("vim.lsp.util")
    -- local params = util.make_formatting_params({})

    -- client.request("textDocument/formatting", params, nil, bufnr)
    -- client.request("textDocument/rangeFormatting", params, nil, bufnr)

    -- Approach 3
    lsp.handlers["textDocument/formatting"] = function() end
    lsp.handlers["textDocument/rangeFormatting"] = function() end
  end

  -- Enable formatting by null-ls
  if client.supports_method("textDocument/formatting") then
    api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
    api.nvim_create_autocmd("BufWritePre", {
      group = augroup,
      buffer = bufnr,
      ---@diagnostic disable-next-line: redefined-local, unused-local
      callback = function(client, bufnr)
        lsp.buf.format({
          -- async = true, -- Breaks formatting on save
          timeout_ms = 5000,
          ---@diagnostic disable-next-line: redefined-local
          filter = function(client)
            return client.name == "null-ls"
          end,
          bufnr = bufnr,
        })
      end,
    })
  end

  -- Highlight current word
  if client.supports_method("textDocument/documentHighlight") then
    api.nvim_create_augroup("lsp_document_highlight", {
      clear = false,
    })
    api.nvim_clear_autocmds({
      buffer = bufnr,
      group = "lsp_document_highlight",
    })
    api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
      group = "lsp_document_highlight",
      buffer = bufnr,
      callback = lsp.buf.document_highlight,
    })
    api.nvim_create_autocmd("CursorMoved", {
      group = "lsp_document_highlight",
      buffer = bufnr,
      callback = lsp.buf.clear_references,
    })
  end
end

return {
  {
    "williamboman/mason.nvim",
    lazy = false,
    build = ":MasonUpdate", -- Update registry
    dependencies = {
      "williamboman/mason-lspconfig.nvim",
      "WhoIsSethDaniel/mason-tool-installer.nvim",
      "neovim/nvim-lspconfig",
      "b0o/schemastore.nvim",
      {
        "utilyre/barbecue.nvim",
        name = "barbecue",
        version = "*",
        dependencies = {
          "SmiteshP/nvim-navic",
        },
        opts = {
          exclude_filetypes = {
            "fugitive",
            "gitcommit",
            "gitrebase",
            "netrw",
            "norg",
            "toggleterm",
          },
          context_follow_icon_color = true,
          symbols = {
            separator = glyphs.vim.listchars.extends,
          },
          kinds = glyphs.lsp_kinds,
          theme = {
            separator = { link = "NonText" },
          },
        },
      },
    },
    config = function()
      local ensure_installed = {
        "bash-language-server",
        "css-lsp",
        "css-variables-language-server",
        "docker-compose-language-service",
        "dockerfile-language-server",
        "emmet-language-server",
        "eslint-lsp",
        "eslint_d",
        "html-lsp",
        "jq-lsp",
        "json-lsp",
        "lua-language-server",
        "markdownlint",
        "marksman",
        "mutt-language-server",
        "shellcheck",
        "shfmt",
        "stylelint",
        "stylua",
        "taplo",
        "typescript-language-server",
        "typos-lsp",
        "vim-language-server",
        "vint",
        { "vue-language-server", version = "1.8.27" },
        "yaml-language-server",
        "yamlfmt",
        "yamllint",
      }

      local handlers = {
        function(server_name) -- Set up all language servers
          require("lspconfig")[server_name].setup({
            -- nvim-cmp almost supports LSP's capabilities so you should advertise it to LSP servers
            capabilities = require("cmp_nvim_lsp").default_capabilities(),
            on_attach = on_attach,
            settings = {
              json = {
                schemas = require("schemastore").json.schemas(),
                validate = { enable = true },
              },
              Lua = {
                runtime = {
                  -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                  version = "LuaJIT",
                },
                diagnostics = {
                  -- Get the language server to recognize the `vim` global
                  globals = { "vim" },
                },
                workspace = {
                  -- Make the server aware of Neovim runtime files
                  library = api.nvim_get_runtime_file("", true),
                  checkThirdParty = false,
                },
                -- Do not send telemetry data containing a randomized but unique identifier
                telemetry = {
                  enable = false,
                },
                hint = {
                  enable = true,
                },
              },
              typescript = {
                inlayHints = {
                  includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
                  includeInlayParameterNameHintsWhenArgumentMatchesName = false,
                  includeInlayFunctionParameterTypeHints = true,
                  includeInlayVariableTypeHints = true,
                  includeInlayVariableTypeHintsWhenTypeMatchesName = false,
                  includeInlayPropertyDeclarationTypeHints = true,
                  includeInlayFunctionLikeReturnTypeHints = true,
                  includeInlayEnumMemberValueHints = true,
                },
              },
              javascript = {
                inlayHints = {
                  includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
                  includeInlayParameterNameHintsWhenArgumentMatchesName = false,
                  includeInlayFunctionParameterTypeHints = true,
                  includeInlayVariableTypeHints = true,
                  includeInlayVariableTypeHintsWhenTypeMatchesName = false,
                  includeInlayPropertyDeclarationTypeHints = true,
                  includeInlayFunctionLikeReturnTypeHints = true,
                  includeInlayEnumMemberValueHints = true,
                },
              },
              volar = {
                filetypes = {
                  "typescript",
                  "javascript",
                  "javascriptreact",
                  "typescriptreact",
                  "vue",
                  "json",
                },
              },
            },
          })
        end,
      }

      -- Initialize LSP diagnostics.
      require("plugins.diagnostics")

      require("lspconfig.ui.windows").default_options.border = "rounded"

      -- Change border of documentation hover window.
      lsp.handlers["textDocument/hover"] = lsp.with(lsp.handlers.hover, {
        border = "rounded",
      })

      -- Change border of signature help hover window.
      lsp.handlers["textDocument/signatureHelp"] = lsp.with(lsp.handlers.signature_help, {
        border = "rounded",
      })

      require("mason").setup({
        ui = {
          border = "rounded",
        },
      })

      require("mason-lspconfig").setup({
        handlers = handlers,
      })

      require("mason-tool-installer").setup({
        ensure_installed = ensure_installed,
        -- automatically install / update on startup. If set to false nothing
        -- will happen on startup. You can use :MasonToolsInstall or
        -- :MasonToolsUpdate to install tools and check for updates.
        -- Default: true
        run_on_start = true,
        -- set a delay (in ms) before the installation starts. This is only
        -- effective if run_on_start is set to true.
        -- e.g.: 5000 = 5 second delay, 10000 = 10 second delay, etc...
        -- Default: 0
        start_delay = 3000, -- 3 second delay
      })
    end,
  },
  {
    "nvimtools/none-ls.nvim",
    commit = "2236d2b", -- All of the configs I use were removed in the next commit.
    lazy = false,
    init = function()
      vim.g.nonels_supress_issue58 = true
    end,
    dependencies = {
      "nvimtools/none-ls-extras.nvim",
      "gbprod/none-ls-shellcheck.nvim",
    },
    config = function()
      local null_ls = require("null-ls")
      local diagnostics = null_ls.builtins.diagnostics
      local formatting = null_ls.builtins.formatting

      local eslint_config_files = {
        "eslint.config.js",
        "eslint.config.mjs",
        "eslint.config.cjs",
        ".eslintrc.js",
        ".eslintrc.mjs",
        ".eslintrc.cjs",
        ".eslintrc.yaml",
        ".eslintrc.yml",
        ".eslintrc.json",
      }

      local stylelint_config_files = {
        "stylelint.config.js",
        "stylelint.config.mjs",
        "stylelint.config.cjs",
        ".stylelintrc.js",
        ".stylelintrc.mjs",
        ".stylelintrc.cjs",
        ".stylelintrc.json",
        ".stylelintrc.yml",
        ".stylelintrc.yaml",
        ".stylelintrc",
      }

      null_ls.setup({
        diagnostics_format = "#{m} (#{c})",
        notify_format = "[null-ls] %s",
        debounce = 300,
        update_in_insert = false,
        border = "rounded",
        sources = {
          require("none-ls.diagnostics.eslint").with({
            filetypes = { "markdown" },
            condition = function(utils)
              return utils.root_has_file(eslint_config_files)
            end,
          }),
          require("none-ls.formatting.eslint").with({
            filetypes = { "markdown" },
            condition = function(utils)
              return utils.root_has_file(eslint_config_files)
            end,
          }),
          require("none-ls.code_actions.eslint").with({
            filetypes = { "markdown" },
            condition = function(utils)
              return utils.root_has_file(eslint_config_files)
            end,
          }),
          -- require('none-ls.diagnostics.eslint_d'),
          require("none-ls.formatting.eslint_d"),
          -- require('none-ls.code_actions.eslint_d'),
          diagnostics.markdownlint,
          formatting.markdownlint,
          require("none-ls.diagnostics.yamllint"),
          formatting.yamlfmt,
          formatting.taplo,
          require("none-ls-shellcheck.diagnostics").with({
            diagnostics_format = "#{m} (#{c})",
          }),
          require("none-ls-shellcheck.code_actions"),
          formatting.shfmt.with({
            args = { "--simplify" },
          }),
          diagnostics.stylelint.with({
            extra_filetypes = { "vue" },
            -- stylelint includes the error code in the message
            diagnostics_format = "#{m}",
            prefer_local = "node_modules/.bin",
            condition = function(utils)
              return utils.root_has_file(stylelint_config_files)
            end,
          }),
          formatting.stylelint.with({
            extra_filetypes = { "vue" },
          }),
          formatting.stylua,
        },
        -- you can reuse a shared lspconfig on_attach callback here
        on_attach = on_attach,
      })
    end,
  },
  -- {
  --   "stevearc/conform.nvim",
  --   lazy = false,
  --   keys = {
  --     {
  --       "<leader>F",
  --       function()
  --         require("conform").format({ async = true, lsp_fallback = true })
  --       end,
  --       mode = "",
  --       desc = "Format buffer",
  --     },
  --   },
  --   opts = {
  --     formatters_by_ft = {
  --       lua = { "stylua" },
  --       toml = { "taplo" },
  --       md = { "markdownlint" },
  --       sh = { "shfmt" },
  --       css = { "stylelint" },
  --       scss = { "stylelint" },
  --       javascript = { "eslint_d" },
  --       typescript = { "eslint_d" },
  --       vue = { "volar" },
  --       ["*"] = { "injected" },
  --     },
  --     format_on_save = { timeout_ms = 500, lsp_fallback = true },
  --   },
  -- },
}
