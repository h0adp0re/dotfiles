local mapkey = require("utils").mapkey
local list_patterns = {
  unordered = "[-]",
  digit = "%d+[.]",
}

return {
  "gaoDean/autolist.nvim",
  -- event = "VeryLazy",
  ft = {
    "markdown",
  },
  opts = {
    cycle = {
      "-",
      "1.",
    },
    lists = {
      markdown = {
        list_patterns.unordered,
        list_patterns.digit,
      },
    },
  },
  config = function(opts)
    require("autolist").setup(opts)

    -- mapkey("i", "<Tab>", "<cmd>AutolistTab<CR>")
    -- mapkey("i", "<S-Tab>", "<cmd>AutolistShiftTab<CR>")
    mapkey("i", "<CR>", "<CR><cmd>AutolistNewBullet<CR>")
    mapkey("n", "o", "o<cmd>AutolistNewBullet<CR>")
    mapkey("n", "O", "O<cmd>AutolistNewBulletBefore<CR>")
    mapkey("n", "<CR>", "<cmd>AutolistToggleCheckbox<CR>")

    -- Functions to recalculate list on edit
    mapkey("n", ">>", ">><cmd>AutolistRecalculate<CR>")
    mapkey("n", "<<", "<<<cmd>AutolistRecalculate<CR>")
    -- mapkey("n", "dd", "dd<cmd>AutolistRecalculate<CR>")
    mapkey("v", "d", "d<cmd>AutolistRecalculate<CR>")
  end,
}
