local glyphs = require("constants.glyphs").indentline

return {
  "lukas-reineke/indent-blankline.nvim",
  event = "VeryLazy",
  main = "ibl",
  opts = {
    indent = {
      char = glyphs.indentline,
    },
    scope = {
      enabled = true,
      show_start = false,
      show_end = false,
      highlight = "Normal",
    },
    exclude = {
      filetypes = {
        "help",
        "qf",
        "vim-plug",
        "fugitive",
        "twiggy",
        "undotree",
        "man",
        "markdown",
        "norg",
        "txt",
        "",
      },
    },
  },
}
