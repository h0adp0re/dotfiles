local mapkey = require("utils").mapkey

-- Create shortcuts for list item outer and inner text objects
mapkey("o", "aa", "<Plug>SidewaysArgumentTextobjA", "Around list item", {})
mapkey("x", "aa", "<Plug>SidewaysArgumentTextobjA", "Around list item", {})
mapkey("o", "ia", "<Plug>SidewaysArgumentTextobjI", "Inside list item", {})
mapkey("x", "ia", "<Plug>SidewaysArgumentTextobjI", "Inside list item", {})

return {
  "AndrewRadev/sideways.vim",
  lazy = false,
}
