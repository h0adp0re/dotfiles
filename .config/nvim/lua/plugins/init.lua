local global = vim.g

-- netrw {{{
global.netrw_banner = 0
global.netrw_liststyle = 3
global.netrw_sort_options = "i"
global.netrw_browse_split = 2
global.netrw_winsize = 25
-- }}}

return {
  "nvim-lua/plenary.nvim",
  "nvim-lua/popup.nvim",
  {
    "tzachar/highlight-undo.nvim",
    lazy = false,
    opts = {
      duration = 500,
    },
  },
  {
    "preservim/vim-markdown",
    ft = { "markdown" },
    init = function()
      -- Disable automatic keymaps of vim-markdown
      global.vim_markdown_no_default_key_mappings = 1

      -- Do not automatically insert bulletpoints nor indent them
      -- let autolist.nvim handle that
      global.vim_markdown_auto_insert_bullets = 0
      global.vim_markdown_new_list_item_indent = 0

      -- Disable folding in markdown
      global.vim_markdown_folding_disabled = 1

      -- Disable concealing markdown syntax
      global.vim_markdown_conceal = 0
      global.vim_markdown_conceal_code_blocks = 0
    end,
  },
  {
    "godlygeek/tabular",
    ft = { "markdown" },
  },
  {
    "dmmulroy/ts-error-translator.nvim",
    lazy = false,
    opts = {},
  },
  {
    "fei6409/log-highlight.nvim",
    lazy = false,
    opts = {},
  },
  {
    "overleaf/vim-env-syntax",
    lazy = false,
  },
  {
    "romainl/vim-qf",
    lazy = false,
  },
  {
    "echasnovski/mini.splitjoin",
    version = "*",
    event = "VeryLazy",
    opts = {
      mappings = {
        toggle = "gS",
      },
    },
  },
  {
    "https://tpope.io/vim/surround.git",
    lazy = false,
  },
  {
    "ThePrimeagen/refactoring.nvim",
    lazy = false,
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
  },
  "ledger/vim-ledger",
  "tridactyl/vim-tridactyl",
  {
    "tweekmonster/startuptime.vim",
    cmd = { "StartupTime" },
  },
  {
    "folke/lazydev.nvim",
    ft = "lua",
  },
  {
    "vhyrro/luarocks.nvim",
    priority = 2000, -- Very high priority is required, luarocks.nvim should run as the first plugin in your config.
    config = true,
  },
  {
    "folke/snacks.nvim",
    priority = 1000,
    lazy = false,
    opts = {
      bigfile = {
        enabled = true,
        size = 0.5 * 1024 * 1024, -- 512KB
      },
      rename = { enabled = true },
      git = { enabled = true },
    },
    keys = {
      {
        "<leader>Gb",
        function()
          Snacks.git.blame_line()
        end,
        desc = "Git Blame Line",
      },
      {
        "<leader>rf",
        function()
          Snacks.rename.rename_file()
        end,
        desc = "Rename file",
      },
    },
  },
  {
    "ghostbuster91/nvim-next",
    config = function()
      local nvim_next_builtins = require("nvim-next.builtins")

      require("nvim-next").setup({
        default_mappings = {
          repeat_style = "original",
        },
        items = {
          nvim_next_builtins.f,
          nvim_next_builtins.t,
        },
      })
    end,
  },
  {
    "tamton-aquib/duck.nvim",
    -- stylua: ignore
    keys = {
      { "<leader>xx", mode = "n", function() require("duck").hatch() end, desc = "Hatch" },
      { "<leader>xc", mode = "n", function() require("duck").cook() end, desc = "Cook" },
      { "<leader>xC", mode = "n", function() require("duck").cook_all() end, desc = "Cook all" },
    },
  },
  {
    "chrisgrieser/nvim-spider",
    lazy = false,
    opts = {
      skipInsignificantPunctuation = false,
      subwordMovement = true,
    },
    keys = {
      {
        "b",
        "<cmd>lua require('spider').motion('b')<CR>",
        mode = { "n", "o", "x" },
      },
      {
        "e",
        "<cmd>lua require('spider').motion('e')<CR>",
        mode = { "n", "o", "x" },
      },
      {
        "w",
        "<cmd>lua require('spider').motion('w')<CR>",
        mode = { "n", "o", "x" },
      },
    },
  },
  {
    "ysmb-wtsg/in-and-out.nvim",
    keys = {
      {
        "<C-j>",
        function()
          require("in-and-out").in_and_out()
        end,
        mode = "i",
      },
    },
  },
  {
    "gregorias/coerce.nvim",
    tag = "v3.0.0",
    lazy = false,
    opts = {},
  },
  {
    "Sam-programs/cmdline-hl.nvim",
    event = "VimEnter",
    opts = {
      type_signs = {
        [":"] = { ":", "@character" },
        ["/"] = { "/", "@character" },
        ["?"] = { "?", "@character" },
        ["="] = { "=", "@character" },
      },
      custom_types = {
        -- ["command-name"] = {
        --   [icon],[icon_hl], default to `:` icon and highlight
        --   [lang], defaults to vim
        --   [showcmd], defaults to false
        --   [pat], defaults to "%w*%s*(.*)"
        --   [code], defaults to nil
        -- }
        -- lang is the treesitter language to use for the commands
        -- showcmd is true if the command should be displayed or to only show the icon
        -- pat is used to extract the part of the command that needs highlighting
        -- the part is matched against the raw command you don't need to worry about ranges
        -- e.g. in '<,>'s/foo/bar/
        -- pat is checked against s/foo/bar
        -- you could also use the 'code' function to extract the part that needs highlighting
        -- ["command-name"]  = false -- to disable a type
        ["lua"] = { icon = ":lua ", icon_hl = "@character", lang = "lua" },
        ["help"] = { icon = ":help ", icon_hl = "@character" },
        ["substitute"] = { pat = "%w(.*)", lang = "regex", show_cmd = true },
      },
      range_hl = "FloatBorder",
    },
  },
  {
    "ryvnf/readline.vim",
    event = "CmdlineEnter",
  },
  {
    "LudoPinelli/comment-box.nvim",
    cmd = {
      "CBd",
      "CBy",
      "CBcatalog",
      "CBacbox",
      "CBalbox",
      "CBcabox",
      "CBccbox",
      "CBclbox",
      "CBcrbox",
      "CBlabox",
      "CBlcbox",
      "CBllbox",
      "CBlrbox",
      "CBrabox",
      "CBrcbox",
      "CBrlbox",
      "CBrrbox",
      "CBline",
      "CBlline",
      "CBlcline",
      "CBllline",
      "CBlrline",
      "CBrcline",
      "CBcline",
      "CBrline",
      "CBccline",
      "CBclline",
      "CBcrline",
      "CBrlline",
      "CBrrline",
    },
  },
  {
    "delphinus/auto-cursorline.nvim",
    event = "VeryLazy",
    opts = {
      wait_ms = 500,
    },
  },
  {
    "0xAdk/full_visual_line.nvim",
    lazy = false,
    opts = {},
  },
  {
    "axieax/urlview.nvim",
    event = "VeryLazy",
    opts = {},
  },
  "jbyuki/venn.nvim",
  {
    "eandrju/cellular-automaton.nvim",
    cmd = "CellularAutomaton",
  },
  {
    "rktjmp/playtime.nvim",
    lazy = false,
    opts = {},
  },
  {
    "NStefan002/2048.nvim",
    cmd = "Play2048",
    config = true,
  },
}
