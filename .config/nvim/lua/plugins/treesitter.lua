return {
  "nvim-treesitter/nvim-treesitter",
  build = function()
    require("nvim-treesitter.install").update()()
  end,
  dependencies = {
    "nvim-treesitter/nvim-treesitter-refactor",
    {
      "nvim-treesitter/nvim-treesitter-context",
      opts = {
        mode = "topline",
      },
    },
    "nvim-treesitter/nvim-treesitter-textobjects",
    "RRethy/nvim-treesitter-textsubjects",
    "RRethy/nvim-treesitter-endwise",
    "windwp/nvim-ts-autotag",
  },
  opts = {
    ensure_installed = {
      "awk",
      "bash",
      "comment",
      "css",
      "diff",
      "dockerfile",
      "git_config",
      "gitattributes",
      "gitcommit",
      "gitignore",
      "html",
      "java",
      "javascript",
      "jq",
      "jsdoc",
      "json",
      "jsonc",
      "json5",
      "latex",
      "lua",
      "luadoc",
      "luap",
      "markdown",
      "muttrc",
      "printf",
      "regex",
      "scss",
      "tmux",
      "toml",
      "typescript",
      "vim",
      "vimdoc",
      "vue",
      "xml",
      "yaml",
    },
    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,
    -- Automatically install missing parsers when entering buffer
    auto_install = false,
    highlight = {
      enable = true,
      disable = {
        "help",
      },
      additional_vim_regex_highlighting = {
        "markdown",
      },
    },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "gnn",
        node_incremental = "grn",
        scope_incremental = "grc",
        node_decremental = "grm",
      },
    },
    textsubjects = {
      enable = true,
      prev_selection = ",",
      keymaps = {
        ["."] = "textsubjects-smart",
        [";"] = "textsubjects-container-outer",
        ["i;"] = "textsubjects-container-inner",
      },
    },
    textobjects = {
      select = {
        enable = true,
        -- Automatically jump forward to textobj, similar to targets.vim
        lookahead = false,
        -- https://github.com/nvim-treesitter/nvim-treesitter-textobjects#built-in-textobjects
        keymaps = {
          ["ab"] = { query = "@block.outer", desc = "Outer block" },
          ["ib"] = { query = "@block.inner", desc = "Inner block" },
          ["ac"] = { query = "@class.outer", desc = "Outer class" },
          ["ic"] = { query = "@class.inner", desc = "Inner class" },
          ["af"] = { query = "@function.outer", desc = "Outer function" },
          ["if"] = { query = "@function.inner", desc = "Inner function" },
          ["am"] = { query = "@parameter.outer", desc = "Outer parameter" },
          ["im"] = { query = "@parameter.inner", desc = "Inner parameter" },
          ["ix"] = { query = "@assignment.rhs", desc = "RHS of an assignment" },
        },
        -- You can choose the select mode (default is charwise 'v')
        selection_modes = {
          ["@parameter.outer"] = "v", -- charwise
          ["@function.outer"] = "V", -- linewise
          ["@class.outer"] = "<c-v>", -- blockwise
        },
        -- If you set this to `true` (default is `false`) then any textobject is
        -- extended to include preceding xor succeeding whitespace. Succeeding
        -- whitespace has priority in order to act similarly to eg the built-in
        -- `ap`.
        include_surrounding_whitespace = false,
      },
      swap = {
        enable = true,
        swap_next = {
          ["<leader>>"] = "@block.outer",
        },
        swap_previous = {
          ["<leader><"] = "@block.outer",
        },
      },
    },
    nvim_next = {
      enable = true,
      textobjects = {
        --instead of defining the move section in the textobjects scope we move it under nvim_next
        move = {
          enable = true,
          set_jumps = true, -- whether to set jumps in the jumplist
          goto_next_start = {
            ["]m"] = "@function.outer",
            -- ["]]"] = { query = "@class.outer", desc = "Next class start" },
            --
            -- You can use regex matching (i.e. lua pattern) and/or pass a list in a "query" key to group multiple queries.
            ["]o"] = "@loop.*",
            -- ["]o"] = { query = { "@loop.inner", "@loop.outer" } }
            --
            -- You can pass a query group to use query from `queries/<lang>/<query_group>.scm file in your runtime path.
            -- Below example nvim-treesitter's `locals.scm` and `folds.scm`. They also provide highlights.scm and indent.scm.
            -- ["]s"] = { query = "@local.scope", query_group = "locals", desc = "Next scope" },
            -- ["]z"] = { query = "@fold", query_group = "folds", desc = "Next fold" },
          },
          goto_next_end = {
            ["]M"] = "@function.outer",
            -- ["]["] = "@class.outer",
          },
          goto_previous_start = {
            ["[m"] = "@function.outer",
            -- ["[["] = "@class.outer",
          },
          goto_previous_end = {
            ["[M"] = "@function.outer",
            -- ["[]"] = "@class.outer",
          },
          -- Below will go to either the start or the end, whichever is closer.
          -- Use if you want more granular movements
          -- Make it even more gradual by adding multiple queries and regex.
          goto_next = {
            ["]d"] = "@conditional.outer",
          },
          goto_previous = {
            ["[d"] = "@conditional.outer",
          },
        },
      },
    },
    -- indent = {
    --   enable = true,
    -- },
    refactor = {
      highlight_definitions = { enable = true },
      navigation = {
        enable = true,
        keymaps = {
          goto_definition_lsp_fallback = "gd",
          list_definitions = "gnD",
          list_definitions_toc = "gO",
          goto_next_usage = "<a-*>",
          goto_previous_usage = "<a-#>",
        },
      },
    },
    -- Auto close and auto rename HTML tags
    autotag = {
      enable = true,
    },
    -- Helps to end certain structures automatically
    endwise = {
      enable = true,
    },
  },
  config = function(_, opts)
    require("nvim-next.integrations").treesitter_textobjects()

    if type(opts.ensure_installed) == "table" then
      ---@type table<string, boolean>
      local added = {}

      opts.ensure_installed = vim.tbl_filter(function(lang)
        if added[lang] then
          return false
        end

        added[lang] = true

        return true
      end, opts.ensure_installed)
    end

    require("nvim-treesitter.configs").setup(opts)
  end,
}
