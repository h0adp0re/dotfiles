return {
  "m-demare/attempt.nvim",
  event = "VeryLazy",
  opts = {
    autosave = false,
    list_buffers = false, -- This will make them show on other pickers (like :Telescope buffers)
    ext_options = { "js", "ts", "py", "sh", "md", "txt" }, -- Options to choose from
    run = {
      js = { "w !node" },
      py = { "w !python" },
      sh = { "w !bash" },
    },
  },
}
