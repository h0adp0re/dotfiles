return {
  "bennypowers/nvim-regexplainer",
  event = "VeryLazy",
  dependencies = {
    "nvim-treesitter/nvim-treesitter",
    "MunifTanjim/nui.nvim",
  },
  opts = {
    mode = "narrative",
    auto = false,
    filetypes = {
      "js",
      "cjs",
      "mjs",
      "ts",
      "cts",
      "mts",
    },
    display = "popup",
    popup = {
      border = {
        padding = { 0, 1 },
        style = "rounded",
      },
    },
    mappings = {
      toggle = "<M-e>", -- Mnemonic: explain
    },
    narrative = {
      separator = "\n",
    },
  },
}
