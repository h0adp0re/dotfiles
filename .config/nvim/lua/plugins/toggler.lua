local mapkey = require("utils").mapkey

mapkey({ "n", "v" }, "€", [[<cmd>lua require("nvim-toggler").toggle()<CR>]], "Toggle value", {})

return {
  "nguyenvukhang/nvim-toggler",
  event = "VeryLazy",
  opts = {
    inverses = {
      ["vim"] = "emacs",
    },
    remove_default_keybinds = true,
  },
}
