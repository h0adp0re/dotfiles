local diagnostic = vim.diagnostic
local mapkey = require("utils").mapkey
local glyphs = require("constants.glyphs").diagnostics
local nndiag = require("nvim-next.integrations").diagnostic()

local diagnostic_config = {
  underline = true,
  signs = {
    severity = 0, -- Disable signs in the signcolumn.
    text = {
      [diagnostic.severity.ERROR] = glyphs.diagnostics.error,
      [diagnostic.severity.WARN] = glyphs.diagnostics.warn,
      [diagnostic.severity.INFO] = glyphs.diagnostics.info,
      [diagnostic.severity.HINT] = glyphs.diagnostics.hint,
    },
  },
  virtual_text = {
    prefix = glyphs.virtual_text_prefix,
    spacing = 4,
    source = true,
  },
  float = {
    border = "rounded",
  },
  update_in_insert = false,
  severity_sort = true,
}
local diag_opts = {
  float = false,
  -- severity = { min = diagnostic.severity.HINT },
}

-- Diagnostics Mappings.
-- INFO: `:help vim.diagnostic.*`
mapkey("n", "<leader>df", diagnostic.open_float, "Diagnostics float")
mapkey("n", "<leader>dk", nndiag.goto_prev(diag_opts), "Previous diagnostic message")
mapkey("n", "<leader>dj", nndiag.goto_next(diag_opts), "Next diagnostic message")
mapkey("n", "<leader>dq", diagnostic.setqflist, "Diagnostics in the quickfix list")
mapkey(
  "n",
  "<leader>dt",
  (function()
    local diag_status = 1 -- 1 is show; 0 is hide

    return function()
      if diag_status == 1 then
        diag_status = 0
        vim.diagnostic.hide()
      else
        diag_status = 1
        vim.diagnostic.show()
      end
    end
  end)(),
  "Toggle diagnostics",
  {}
)

diagnostic.config(diagnostic_config)

return {
  -- {
  --   "mfussenegger/nvim-lint",
  --   lazy = false,
  --   config = function()
  --     require("lint").linters_by_ft = {
  --       markdown = { "markdownlint", "marksman" },
  --       css = { "stylelint" },
  --       scss = { "stylelint" },
  --     }
  --   end,
  -- },
}
