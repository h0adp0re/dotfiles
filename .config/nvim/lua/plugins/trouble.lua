local mapkey = require("utils").mapkey
local glyphs = require("constants.glyphs").trouble

mapkey(
  "n",
  "<leader>o",
  "<cmd>Trouble lsp_document_symbols toggle focus=true win.position=right win.size=50<CR>",
  "Document Symbols"
)

return {
  "folke/trouble.nvim",
  event = "VeryLazy",
  opts = {
    auto_close = false, -- auto close when there are no items
    auto_open = false, -- auto open when there are items
    auto_preview = true, -- automatically open preview when on an item
    auto_refresh = true, -- auto refresh when open
    auto_jump = false, -- auto jump to the item when there's only one
    focus = false, -- Focus the window when opened
    restore = true, -- restores the last location in the list when opening
    follow = true, -- Follow the current item
    icons = glyphs,
    indent_guides = true, -- show indent guides
    multiline = true, -- render multi-line messages
  },
}
