local mapkey = require("utils").mapkey

mapkey({ "n", "x" }, "p", "<Plug>(YankyPutAfter)", "Put after", {})
mapkey({ "n", "x" }, "P", "<Plug>(YankyPutBefore)", "Put before", {})
mapkey("n", "<C-p>", "<Plug>(YankyCycleForward)", "Cycle put forward", {})
mapkey("n", "<C-n>", "<Plug>(YankyCycleBackward)", "Cycle put backward", {})

return {
  "gbprod/yanky.nvim",
  event = "VeryLazy",
  dependencies = {
    "nvim-telescope/telescope.nvim",
  },
  opts = {
    ring = {
      history_length = 100,
      storage = "shada",
      sync_with_numbered_registers = true,
      cancel_event = "update",
    },
    system_clipboard = {
      sync_with_ring = true,
    },
    highlight = {
      on_put = true,
      on_yank = true,
      timer = 100,
    },
    preserve_cursor_position = {
      enabled = true,
    },
    -- picker = {
    --   telescope = {
    --     mappings = {
    --       default = require("yanky.telescope.mapping").put("p"),
    --       i = {
    --         ["<CR>"] = require("yanky.telescope.mapping").put("p"),
    --         ["<C-x>"] = require("yanky.telescope.mapping").delete(),
    --         ["<C-r>"] = require("yanky.telescope.mapping").set_register(require("yanky.utils").get_default_register()),
    --       },
    --     },
    --   },
    -- },
  },
}
