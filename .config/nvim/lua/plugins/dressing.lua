return {
  "stevearc/dressing.nvim",
  event = "VeryLazy",
  opts = {
    input = {
      enabled = true, -- Set to false to disable the vim.ui.input implementation
      default_prompt = "Input:", -- Default prompt string
      prompt_align = "left", -- Can be 'left', 'right', or 'center'
      start_mode = "insert",
      -- These are passed to nvim_open_win
      border = "rounded",
      relative = "cursor", -- 'editor' and 'win' will default to being centered
      prefer_width = 40, -- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
      width = nil, -- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
      -- min_width and max_width can be a list of mixed types.
      -- min_width = {20, 0.2} means "the greater of 20 columns or 20% of total"
      min_width = { 20, 0.2 },
      max_width = { 140, 0.9 },
      win_options = {
        winblend = 0, -- Window transparency (0-100)
        winhighlight = "", -- Change default highlight groups (see :help winhl)
      },
      -- Set to `false` to disable
      mappings = {
        n = {
          ["<Esc>"] = "Close",
          ["<CR>"] = "Confirm",
        },
        i = {
          ["<C-c>"] = "Close",
          ["<CR>"] = "Confirm",
          ["<C-k>"] = "HistoryPrev",
          ["<C-j>"] = "HistoryNext",
          ["<Esc>"] = "Close",
        },
      },
      override = function(conf)
        -- This is the config that will be passed to nvim_open_win.
        -- Change values here to customize the layout
        return conf
      end,
      get_config = nil, -- see :help dressing_get_config
    },
    select = {
      enabled = true, -- Set to false to disable the vim.ui.select implementation
      backend = { "telescope", "fzf", "builtin" }, -- Priority list of preferred vim.select implementations
      trim_prompt = true, -- Trim trailing `:` from prompt
      -- Options for telescope selector
      -- These are passed into the telescope picker directly. Can be used like:
      -- telescope = require('telescope.themes').get_ivy({...})
      telescope = nil,
      -- Options for fzf selector
      fzf = {
        window = {
          width = 0.5,
          height = 0.4,
        },
      },
      -- Options for built-in selector
      builtin = {
        -- These are passed to nvim_open_win
        border = "rounded",
        relative = "editor", -- 'editor' and 'win' will default to being centered
        -- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
        -- the min_ and max_ options can be a list of mixed types.
        -- max_width = {140, 0.8} means "the lesser of 140 columns or 80% of total"
        width = nil,
        max_width = { 140, 0.8 },
        min_width = { 40, 0.2 },
        height = nil,
        max_height = 0.9,
        min_height = { 10, 0.2 },
        win_options = {
          winblend = 0, -- Window transparency (0-100)
          winhighlight = "", -- Change default highlight groups (see :help winhl)
        },
        -- Set to `false` to disable
        mappings = {
          ["<Esc>"] = "Close",
          ["<C-c>"] = "Close",
          ["<CR>"] = "Confirm",
        },
        override = function(conf)
          -- This is the config that will be passed to nvim_open_win.
          -- Change values here to customize the layout
          return conf
        end,
      },
      format_item_override = {}, -- Used to override format_item. See :help dressing-format
      get_config = nil, -- see :help dressing_get_config
    },
  },
}
