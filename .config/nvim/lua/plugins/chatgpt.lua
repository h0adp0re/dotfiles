local mapkey = require("utils").mapkey

mapkey("n", "<leader>Ac", "<cmd>ChatGPT<CR>", "Interactive chat")
mapkey("n", "<leader>Aa", "<cmd>ChatGPTActAs<CR>", "Act as...")
mapkey("v", "<leader>Ae", "<cmd>ChatGPTEditWithInstructions<CR>", "Edit with instructions")
mapkey("n", "<leader>Ao", "<cmd>ChatGPTRun optimize_code<CR>", "Optimize code")
mapkey("v", "<leader>Ar", "<cmd>ChatGPTRun code_readability_analysis<CR>", "Code readability analysis")
mapkey("v", "<leader>Af", "<cmd>ChatGPTRun fix_bugs<CR>", "Fix bugs")
mapkey("v", "<leader>Ax", "<cmd>ChatGPTRun explain_code<CR>", "Explain code")

return {
  "jackMort/ChatGPT.nvim",
  event = "VeryLazy",
  opts = {
    api_key_cmd = "security find-generic-password -ws openai.api.key",
  },
  dependencies = {
    "MunifTanjim/nui.nvim",
    "nvim-lua/plenary.nvim",
    "folke/trouble.nvim",
    "nvim-telescope/telescope.nvim",
  },
}
