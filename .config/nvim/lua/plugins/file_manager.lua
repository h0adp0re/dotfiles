return {
  "is0n/fm-nvim",
  event = "VeryLazy",
  opts = {
    ui = {
      float = {
        border = "rounded",
        height = 0.8,
        width = 0.8,
      },
    },
    cmds = {
      nnn_cmd = "nnn -cdEHS",
      vifm_cmd = "vifm",
      ranger_cmd = "ranger",
      neomutt_cmd = "neomutt",
      taskwarrior_cmd = "taskwarrior-tui",
    },
    mappings = {
      vert_split = "<C-v>",
      horz_split = "<C-h>",
      tabedit = "<C-t>",
      edit = "<C-e>",
      ESC = "<Esc>",
    },
  },
}
