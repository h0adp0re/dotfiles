local glyphs = require("constants.glyphs").dap
local js_based_languages = {
  "javascript",
  "typescript",
  "vue",
}

return {
  "mfussenegger/nvim-dap",
  lazy = false,
  dependencies = {
    {
      "jay-babu/mason-nvim-dap.nvim",
      dependencies = "mason.nvim",
      config = function()
        require("mason-nvim-dap").setup({
          -- A list of adapters to install if they're not already installed.
          -- This setting has no relation with the `automatic_installation` setting.
          ensure_installed = {
            "bash", -- "bash-debug-adapter"
            "js", -- "js-debug-adapter"
          },
          -- Whether adapters that are set up (via dap) should be automatically
          -- installed if they're not already installed.
          -- This setting has no relation with the `ensure_installed` setting.
          -- Can either be:
          --   - false: Daps are not automatically installed.
          --   - true: All adapters set up via dap are automatically installed.
          --   - { exclude: string[] }: All adapters set up via mason-nvim-dap,
          --     except the ones provided in the list, are automatically installed.
          --
          --       Example: automatic_installation = { exclude = { "python", "delve" } }
          automatic_installation = true,
        })
      end,
    },
    {
      "rcarriga/nvim-dap-ui",
      dependencies = {
        "nvim-neotest/nvim-nio",
      },
      config = function()
        local dap = require("dap")
        local dapui = require("dapui")

        dapui.setup({
          layouts = {
            {
              elements = {
                { id = "scopes", size = 0.5 },
                { id = "breakpoints", size = 0.25 },
                { id = "watches", size = 0.25 },
              },
              size = 40,
              position = "left",
            },
            {
              elements = { "repl" },
              size = 10,
              position = "bottom",
            },
          },
          floating = {
            border = "rounded",
          },
        })

        dap.listeners.before.attach.dapui_config = function()
          dapui.open()
        end
        dap.listeners.before.launch.dapui_config = function()
          dapui.open()
        end
        dap.listeners.before.event_terminated.dapui_config = function()
          dapui.close()
        end
        dap.listeners.before.event_exited.dapui_config = function()
          dapui.close()
        end
      end,
    },
    {
      "theHamsta/nvim-dap-virtual-text",
      opts = {
        enabled = true,
      },
    },
  },
  keys = {
    {
      "<leader>Da",
      function()
        if vim.fn.filereadable(".vscode/launch.json") then
          local dap_vscode = require("dap.ext.vscode")

          dap_vscode.load_launchjs(nil, {
            ["pwa-node"] = js_based_languages,
            ["chrome"] = js_based_languages,
            ["pwa-chrome"] = js_based_languages,
          })
        end

        require("dap").continue()
      end,
      desc = "Attach/Continue",
    },
  },
  config = function()
    local dap = require("dap")

    dap.adapters["pwa-node"] = {
      type = "server",
      host = "localhost",
      port = "${port}",
      executable = {
        command = "js-debug-adapter",
        args = { "${port}" },
      },
    }

    dap.adapters["pwa-chrome"] = dap.adapters["pwa-node"]

    for _, language in ipairs(js_based_languages) do
      dap.configurations[language] = {
        -- Debug single Node.js files
        {
          type = "pwa-node",
          request = "launch",
          name = "Launch file",
          program = "${file}",
          cwd = "${workspaceFolder}",
          sourceMaps = true,
        },
        -- Debug Node.js processes (add --inspect when running the process)
        {
          type = "pwa-node",
          request = "attach",
          name = "Attach",
          processId = require("dap.utils").pick_process,
          cwd = "${workspaceFolder}",
          sourceMaps = true,
        },
        -- Debug web applications (client side)
        {
          type = "pwa-chrome",
          request = "launch",
          name = "Launch & Debug Chrome",
          url = function()
            local co = coroutine.running()

            return coroutine.create(function()
              vim.ui.input({
                prompt = "Enter URL: ",
                default = "http://localhost:8080",
              }, function(url)
                if url == nil or url == "" then
                  return
                else
                  coroutine.resume(co, url)
                end
              end)
            end)
          end,
          webRoot = "${workspaceFolder}",
          skipFiles = { "<node_internals>/**/*.js" },
          protocol = "inspector",
          sourceMaps = true,
          userDataDir = false,
        },
        -- Divider for the launch.json derived configs
        {
          name = "----- ↓ launch.json configs ↓ -----",
          type = "",
          request = "launch",
        },
      }
    end

    vim.fn.sign_define("DapBreakpoint", {
      text = glyphs.breakpoint,
      texthl = "DapBreakpoint",
      linehl = "",
      numhl = "DapBreakpoint",
    })
    vim.fn.sign_define("DapBreakpointCondition", {
      text = glyphs.breakpoint_condition,
      texthl = "DapBreakpoint",
      linehl = "",
      numhl = "DapBreakpoint",
    })
    vim.fn.sign_define("DapBreakpointRejected", {
      text = glyphs.breakpoint_rejected,
      texthl = "DapBreakpoint",
      linehl = "",
      numhl = "DapBreakpoint",
    })
    vim.fn.sign_define("DapLogPoint", {
      text = glyphs.log_point,
      texthl = "DapLogPoint",
      linehl = "",
      numhl = "DapLogPoint",
    })
    vim.fn.sign_define("DapStopped", {
      text = glyphs.stopped,
      texthl = "DapStopped",
      linehl = "",
      numhl = "DapStopped",
    })

    require("nvim-dap-virtual-text").setup()
  end,
}
