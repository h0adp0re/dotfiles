return {
  "windwp/nvim-autopairs",
  event = "VeryLazy",
  opts = {
    disable_filetype = { "TelescopePrompt" },
    disable_in_macro = false, -- disable when recording or executing a macro
    disable_in_visualblock = false, -- disable when insert after visual block mode
    ignored_next_char = [=[[%w%%%'%[%"%.]]=],
    enable_moveright = true,
    enable_afterquote = true, -- add bracket pairs after quote
    enable_check_bracket_line = true, --- check bracket in same line
    enable_bracket_in_quote = true, --
    enable_abbr = false, -- trigger abbreviation
    break_undo = true, -- switch for basic rule break undo sequence
    check_ts = true,
    map_cr = true,
    map_bs = true, -- map the <BS> key
    map_c_h = false, -- Map the <C-h> key to delete a pair
    map_c_w = true, -- map <C-w> to delete a pair if possible
    fast_wrap = {
      map = "<C-e>",
      chars = { "{", "[", "(", '"', "'" },
      pattern = [=[[%'%"%)%>%]%)%}%,]]=],
      end_key = "$",
      keys = "qwertyuiopzxcvbnmasdfghjkl",
      check_comma = true,
      highlight = "Search",
      highlight_grey = "Comment",
    },
  },
  config = function(opts)
    local npairs = require('nvim-autopairs')
    local Rule = require('nvim-autopairs.rule')
    local ts_conds = require("nvim-autopairs.ts-conds")

    npairs.setup(opts)

    npairs.add_rules {
      Rule('{{', '  }', 'vue')
        :set_end_pair_length(2)
        :with_pair(ts_conds.is_ts_node 'text'),
    }
  end
}
