local mapkey = require("utils").mapkey
local global = vim.g

mapkey("n", "<leader>C", [[<cmd>lua require("theme-picker").open_theme_picker()<CR>]], "Pick colorscheme")

return {
  {
    "panghu-huang/theme-picker.nvim",
    lazy = false,
    opts = {
      -- Options passed to Telescope
      picker = {
        prompt_title = "Select Colorscheme",
        results_title = "",
        layout_config = {
          width = 0.35,
          height = 0.5,
        },
      },
      themes = {
        {
          name = "Gruvbox Material",
          colorscheme = "gruvbox-material",
        },
        {
          name = "Rosé Pine",
          colorscheme = "rose-pine",
        },
        {
          name = "Catppuccin",
          colorscheme = "catppuccin",
        },
        {
          name = "Kanagawa",
          colorscheme = "kanagawa",
        },
        {
          name = "Tokyo Night (Moon)",
          colorscheme = "tokyonight-moon",
        },
        {
          name = "Nord",
          colorscheme = "nord",
        },
      },
    },
  },
  {
    "sainnhe/gruvbox-material",
    init = function()
      global.gruvbox_material_background = "medium"
      global.gruvbox_material_enable_bold = 1
      global.gruvbox_material_enable_italic = 1
      global.gruvbox_material_visual = "grey background"
      global.gruvbox_material_menu_selection_background = "green"
      global.gruvbox_material_sign_column_background = "none"
      global.gruvbox_material_diagnostic_line_highlight = 1
      global.gruvbox_material_diagnostic_virtual_text = "colored"
      global.gruvbox_material_current_word = "grey background"
      global.gruvbox_material_statusline_style = "default"
      global.gruvbox_material_better_performance = 1
      global.gruvbox_material_palette = "material"

      vim.cmd.colorscheme("gruvbox-material")
    end,
  },
  {
    "rose-pine/neovim",
    name = "rose-pine",
    config = function()
      require("rose-pine").setup({
        variant = "main", -- auto, main, moon, or dawn
        dark_variant = "main", -- main, moon, or dawn
        dim_inactive_windows = false,
        extend_background_behind_borders = true,

        styles = {
          bold = true,
          italic = true,
          transparency = true,
        },

        groups = {
          border = "muted",
          link = "iris",
          panel = "surface",

          error = "love",
          hint = "iris",
          info = "foam",
          note = "pine",
          todo = "rose",
          warn = "gold",

          git_add = "foam",
          git_change = "rose",
          git_delete = "love",
          git_dirty = "rose",
          git_ignore = "muted",
          git_merge = "iris",
          git_rename = "pine",
          git_stage = "iris",
          git_text = "rose",
          git_untracked = "subtle",

          h1 = "iris",
          h2 = "foam",
          h3 = "rose",
          h4 = "gold",
          h5 = "pine",
          h6 = "foam",
        },
      })

      vim.cmd.colorscheme("rose-pine")
    end,
  },
  {
    "catppuccin/nvim",
    name = "catppuccin",
    config = function()
      require("catppuccin").setup({
        custom_highlights = function()
          return {
            NormalFloat = { bg = "none" },
            FloatBorder = { bg = "none" },
            HlSearchNear = { link = "IncSearch" },
            HlSearchLensNear = { link = "IncSearch" },
          }
        end,
      })

      vim.cmd.colorscheme("catppuccin")
    end,
  },
  {
    "rebelot/kanagawa.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("kanagawa").setup({
        compile = true,
        transparent = true,
        theme = "dragon",
        background = {
          dark = "dragon",
        },
        overrides = function(colors)
          local theme = colors.theme

          return {
            TreesitterContext = { link = "CursorLine" },
            TreesitterContextBottom = { link = "CursorLine" },
            TreesitterContextLineNumber = { link = "Normal" },
            NeorgContext = { link = "TreesitterContext" },
            CmpItemKindSnippet = { link = "CmpItemKindOperator" },

            NormalFloat = { bg = "none" },
            FloatBorder = { fg = theme.ui.special, bg = "none" },
            FloatTitle = { fg = theme.ui.fg_dim, bold = true, bg = "none" },
            TelescopeBorder = { link = "FloatBorder" },
            TelescopeTitle = { link = "FloatTitle" },
            HarpoonBorder = { link = "FloatBorder" },
            HarpoonTitle = { link = "FloatTitle" },

            CursorLineNr = { link = "NormalFloat" },

            LazyNormal = { link = "NormalFloat" },
            MasonNormal = { link = "NormalFloat" },
            ["@string.special.url"] = { link = "@markup.link" },
          }
        end,
      })

      vim.cmd.colorscheme("kanagawa")
    end,
  },
  {
    "folke/tokyonight.nvim",
    opts = {
      transparent = false,
      sidebars = {
        "qf",
        "help",
        "fugitive",
        "neotest-summary",
      },
      lualine_bold = true,
      styles = {
        floats = "transparent",
      },
      on_highlights = function(hl, c)
        hl.CursorLineNr = {
          fg = c.blue,
        }
      end,
    },
    config = function()
      -- vim.cmd.colorscheme("tokyonight-day")
      -- vim.cmd.colorscheme("tokyonight-night")
      -- vim.cmd.colorscheme("tokyonight-storm")
      vim.cmd.colorscheme("tokyonight-moon")
    end,
  },
  {
    "shaunsingh/nord.nvim",
    config = function()
      global.nord_contrast = true
      global.nord_borders = true
      global.nord_disable_background = true
      global.nord_cursorline_transparent = false
      global.nord_uniform_diff_background = true

      vim.cmd.colorscheme("nord")
    end,
  },
}
