local glyphs = require("constants.glyphs").neotest

return {
  "nvim-neotest/neotest",
  lazy = false,
  dependencies = {
    "nvim-neotest/nvim-nio",
    "nvim-lua/plenary.nvim",
    "nvim-treesitter/nvim-treesitter",
    "marilari88/neotest-vitest",
    "rcasia/neotest-bash",
  },
  config = function()
    local mapkey = require("utils").mapkey
    local neotest = require("neotest")

    neotest.setup({
      adapters = {
        require("neotest-vitest")({
          is_test_file = function(file_path)
            if string.match(file_path, "ic12") then
              if string.match(file_path, "ic12/src") then
                return string.find(file_path, "spec%.%a%a$") ~= nil
              end
            else
              return string.find(file_path, "spec%.%a%a$") ~= nil
            end
          end,
        }),
        require("neotest-bash"),
      },
      quickfix = {
        open = false,
      },
      icons = glyphs,
      summary = {
        enabled = true,
        animated = false,
        expand_errors = true,
        follow = true,
        mappings = {
          attach = "a",
          clear_marked = "M",
          clear_target = "T",
          expand = { "<CR>", "<2-LeftMouse>" },
          expand_all = "e",
          jumpto = "i",
          mark = "m",
          next_failed = "J",
          output = "o",
          prev_failed = "K",
          run = "r",
          run_marked = "R",
          short = "O",
          stop = "u",
          target = "t",
        },
        open = "botright vsplit | vertical resize 56 | set nowrap",
      },
    })

    mapkey("n", "<leader>ts", function()
      neotest.summary.toggle()
    end, "Toggle summary panel")
    mapkey("n", "<leader>to", function()
      neotest.output_panel.toggle()
    end, "Toggle output panel")
    mapkey("n", "<leader>tt", function()
      neotest.run.run()
    end, "Run nearest test")
    mapkey("n", "<leader>td", function()
      neotest.run.run({ strategy = "dap" })
    end, "Debug test")
    mapkey("n", "<leader>tf", function()
      neotest.run.run(vim.fn.expand("%"))
    end, "Run file")
    mapkey("n", "<leader>tj", function()
      neotest.jump.next()
    end, "Jump to next test")
    mapkey("n", "<leader>tk", function()
      neotest.jump.prev()
    end, "Jump to previous test")
  end,
}
