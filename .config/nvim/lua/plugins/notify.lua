local glyphs = require("constants.glyphs")

return {
  {
    "j-hui/fidget.nvim",
    event = "VeryLazy",
    opts = {
      progress = {
        display = {
          done_icon = glyphs.fidget.done,
          progress_icon = {
            pattern = "dots_footsteps",
          },
        },
      },
      notification = {
        view = {
          group_separator = "",
        },
        window = {
          winblend = 0,
        },
      },
    },
  },
  {
    "rcarriga/nvim-notify",
    event = "VeryLazy",
    opts = {
      icons = glyphs.notify,
      stages = "no_animation",
      max_width = 70,
    },
    init = function()
      vim.notify = require("notify")
    end,
  },
}
