function _G.close_all_floating_wins()
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    local config = vim.api.nvim_win_get_config(win)
    if config.relative ~= "" then
      vim.api.nvim_win_close(win, false)
    end
  end
end

return {
  "rmagatti/auto-session",
  lazy = false,
  opts = {
    auto_save_enabled = true,
    auto_restore_enabled = true,
    pre_save_cmds = { _G.close_all_floating_wins },
  },
}
