local glyphs = require("constants.glyphs").todo

-- NOTE: Something something
-- INFO: Something something
-- IDEA: Something something

-- TODO: Something something

-- HACK: Something something

-- WARN: Something something
-- WARNING: Something something
-- XXX: Something something

-- PERF: Something Something
-- PERFORMANCE: Something Something
-- OPTIM: Something Something
-- OPTIMIZE: Something Something

-- FIX: Something something
-- FIXME: Something something
-- BUG: Something something
-- FIXIT: Something something
-- ISSUE: Something something

return {
  "folke/todo-comments.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  event = "VeryLazy",
  opts = {
    -- Show icons in the signs column.
    signs = false,
    keywords = {
      NOTE = { icon = glyphs.NOTE, color = "info", alt = { "INFO", "IDEA" } },
      TODO = { icon = glyphs.TODO, color = "hint" },
      HACK = { icon = glyphs.HACK, color = "warning" },
      WARN = { icon = glyphs.HACK, color = "warning", alt = { "WARNING", "XXX" } },
      PERF = { icon = glyphs.PERF, alt = { "PERFORMANCE", "OPTIM", "OPTIMIZE" } },
      FIX = { icon = glyphs.FIX, color = "error", alt = { "FIXME", "FIXIT", "BUG", "ISSUE" } },
    },
    -- List of named colors where we try to extract the guifg from the
    -- list of hl groups or use the hex color if hl not found as a fallback.
    colors = {
      error = { "LspDiagnosticsDefaultError", "ErrorMsg", "#EA6962" },
      warning = { "LspDiagnosticsDefaultWarning", "WarningMsg", "#D8A657" },
      info = { "LspDiagnosticsDefaultInformation", "Blue", "#7DAEA3" },
      hint = { "LspDiagnosticsDefaultHint", "Green", "#A9B665" },
      default = { "Orange", "GruvboxOrange", "#E78A4E" },
    },
  },
}
