local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta

ls.add_snippets("norg", {
  s(
    {
      name = "norg code",
      trig = "@code",
    },
    fmta(
      [[
      @code <>
        <>
      @end
      ]],
      {
        i(1),
        i(2),
      }
    )
  ),
})
