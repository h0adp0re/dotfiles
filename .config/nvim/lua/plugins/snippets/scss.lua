local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta

ls.add_snippets("scss", {
  s(
    {
      name = "@debug",
      trig = "@debug",
    },
    fmta([[@debug <>;]], {
      i(1, "&"),
    })
  ),

  s(
    {
      name = "rem function",
      trig = "rem",
    },
    fmta([[rem(<>)]], {
      i(1),
    })
  ),

  s(
    {
      name = "scss variable",
      trig = "$var",
    },
    fmta([[$<>: <>;]], {
      i(1, "variable"),
      i(2, "value"),
    })
  ),

  s(
    {
      name = "@import",
      trig = "@import",
    },
    fmta([[@import '<>';]], {
      i(1),
    })
  ),

  s(
    {
      name = "@mixin",
      trig = "@mixin",
    },
    fmta(
      [[
      @mixin <>(<>) {
        <>
      };
      ]],
      {
        i(1, "name"),
        i(2, "args"),
        i(3),
      }
    )
  ),

  s(
    {
      name = "@include",
      trig = "@include",
    },
    fmta([[@include <>(<>);]], {
      i(1, "mixin"),
      i(2),
    })
  ),

  s(
    {
      name = "@extend",
      trig = "@extend",
    },
    fmta([[@extend <>;]], {
      i(1, "selector"),
    })
  ),

  s(
    {
      name = "@function",
      trig = "@function",
    },
    fmta(
      [[
      @function <>(<>) {
        <>
      };
      ]],
      {
        i(1, "name"),
        i(2, "args"),
        i(3),
      }
    )
  ),

  s(
    {
      name = "@if",
      trig = "@if",
    },
    fmta(
      [[
      @if <> {
        <>
      };
      ]],
      {
        i(1, "condition"),
        i(2),
      }
    )
  ),

  s(
    {
      name = "@if ... @else",
      trig = "@ife",
    },
    fmta(
      [[
      @if <> {
        <>
      } @else {
        <>
      };
      ]],
      {
        i(1, "condition"),
        i(2),
        i(3),
      }
    )
  ),

  s(
    {
      name = "@else if",
      trig = "@eif",
    },
    fmta(
      [[
      @else if <> {
        <>
      };
      ]],
      {
        i(1, "condition"),
        i(2),
      }
    )
  ),

  s(
    {
      name = "@for",
      trig = "@for",
    },
    fmta(
      [[
      @for <> from <> through <> {
        <>
      };
      ]],
      {
        i(1, "$i"),
        i(2, "1"),
        i(3, "3"),
        i(4),
      }
    )
  ),

  s(
    {
      name = "@each",
      trig = "@each",
    },
    fmta(
      [[
      @each <> in <> {
        <>
      };
      ]],
      {
        i(1, "$item"),
        i(2, "items"),
        i(3),
      }
    )
  ),

  s(
    {
      name = "@while",
      trig = "@while",
    },
    fmta(
      [[
      @while <> from <> <> {
        <>
      };
      ]],
      {
        i(1, "$i"),
        i(2, ">"),
        i(3, "0"),
        i(4),
      }
    )
  ),
})
