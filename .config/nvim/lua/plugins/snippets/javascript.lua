local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local r = ls.restore_node
local d = ls.dynamic_node
local sn = ls.snippet_node
local fmta = require("luasnip.extras.fmt").fmta
local rep = require("luasnip.extras").rep

ls.add_snippets("javascript", {
  s(
    {
      name = "const definition",
      trig = "const",
    },
    fmta([[const <> = <>;]], {
      i(1, "name"),
      i(2, "value"),
    })
  ),

  s(
    {
      name = "let definition",
      trig = "let",
    },
    fmta([[let <> = <>;]], {
      i(1, "name"),
      i(2, "value"),
    })
  ),

  s(
    {
      name = "import statement",
      trig = "import",
    },
    c(1, {
      fmta([[import <> from '<>';]], {
        r(2, "thing"),
        r(1, "module"),
      }),
      fmta([[import { <> } from '<>';]], {
        r(2, "thing"),
        r(1, "module"),
      }),
      fmta([[import { <> as <> } from '<>';]], {
        r(2, "thing"),
        r(3, "name"),
        r(1, "module"),
      }),
    }, {
      restore_cursor = true,
    }),
    {
      stored = {
        module = i(1, "module"),
        thing = i(2, "thing"),
        name = i(3, "name"),
      },
    }
  ),

  s(
    {
      name = "return statement",
      trig = "ret",
    },
    fmta([[return<>;]], {
      i(1, ""),
    })
  ),

  s(
    {
      name = "ternary",
      trig = "te",
    },
    fmta(
      [[
      <>
        ? <>
        : <>;
      ]],
      {
        i(1, "condition"),
        i(2, "true"),
        i(3, "false"),
      }
    )
  ),

  s(
    {
      name = "ternary assignment",
      trig = "ta",
    },
    fmta(
      [[
      const <> = <>
        ? <>
        : <>;
      ]],
      {
        i(1, "name"),
        i(2, "condition"),
        i(3, "true"),
        i(4, "false"),
      }
    )
  ),

  s(
    {
      name = "if statement",
      trig = "if",
    },
    fmta(
      [[
      if (<>) {
        <>
      }
      ]],
      {
        i(1, "condition"),
        i(2, "return true;"),
      }
    )
  ),

  s(
    {
      name = "if ... else statement",
      trig = "ife",
    },
    fmta(
      [[
      if (<>) {
        <>
      } else {
        <>
      }
      ]],
      {
        i(1, "condition"),
        i(2, "return true;"),
        i(3, "return false;"),
      }
    )
  ),

  s(
    {
      name = "else if statement",
      trig = "elif",
    },
    fmta(
      [[
      else if (<>) {
        <>
      }
      ]],
      {
        i(1, "condition"),
        i(2, "return true;"),
      }
    )
  ),

  s(
    {
      name = "else statement",
      trig = "else",
    },
    fmta(
      [[
      else {
        <>
      }
      ]],
      {
        i(1, "return false;"),
      }
    )
  ),

  s(
    {
      name = "try ... catch statement",
      trig = "trycatch",
    },
    fmta(
      [[
      try {
        <>
      } catch (<>) {
        <>
      }
      ]],
      {
        i(1, "console.log('success');"),
        i(2, "error"),
        i(3, "console.error(error);"),
      }
    )
  ),

  s(
    {
      name = "try ... catch ... finally statement",
      trig = "trycatchfinally",
    },
    fmta(
      [[
      try {
        <>
      } catch (<>) {
        <>
      } finally {
        <>
      }
      ]],
      {
        i(1, "console.log('success');"),
        i(2, "error"),
        i(3, "console.error(error);"),
        i(4, "console.log('finally');"),
      }
    )
  ),

  -- for (let ${1:i} = 0; $1 < ${2:arr.length}; $1++) {${3:console.log(i)}}
  s(
    {
      name = "for loop",
      trig = "for",
    },
    fmta(
      [[
      for (let <> = <>; <> << <>; <>++) {
        <>
      }
      ]],
      {
        i(1, "i"),
        i(2, "0"),
        rep(1),
        i(3, "arr.length"),
        rep(1),
        -- TODO: Get the `i` dynamically from `1`
        -- i(4, "console.log(i);"),
        d(4, function(args)
          return sn(
            nil,
            fmta([[console.log(<>);]], {
              i(1, args[1]),
            })
          )
        end, { 1 }),
      }
    )
  ),

  -- Mnemonic: (o)f -> not objects
  s(
    {
      name = "for ... of loop",
      trig = "for of",
    },
    fmta(
      [[
      for (const <> of <>) {
        <>
      }
      ]],
      {
        i(1, "iteration"),
        i(2, "iterable"),
        -- TODO: Get the `iteration` dynamically from `1`
        i(3, "console.log(iteration);"),
      }
    )
  ),

  -- Mnemonic: (i)n -> not iterables
  s(
    {
      name = "for ... in loop",
      trig = "for in",
    },
    fmta(
      [[
      for (const <> in <>) {
        <>
      }
      ]],
      {
        i(1, "iteration"),
        i(2, "object"),
        -- TODO: Get the `iteration` dynamically from `1`
        i(3, "console.log(iteration);"),
      }
    )
  ),

  s(
    {
      name = "arrow function",
      trig = "afn",
    },
    c(1, {
      -- Inline arrow fn
      fmta([[ (<>) =>> <> ]], {
        r(1, "args"),
        r(2, "body"),
      }),
      -- Multiline braceless arrow fn
      fmta(
        [[
        (<>) =>>
          <>
        ]],
        {
          r(1, "args"),
          r(2, "body"),
        }
      ),
      -- Multiline arrow fn
      fmta(
        [[
        (<>) =>> {
          <>
        }
        ]],
        {
          r(1, "args"),
          r(2, "body"),
        }
      ),
    }, {
      restore_cursor = true,
    }),
    {
      stored = {
        args = i(1, ""),
        body = i(2, ""),
      },
    }
  ),

  s(
    {
      name = "console.log",
      trig = "log",
    },
    c(1, {
      fmta([[console.log(<>);]], {
        r(1, "value"),
      }),
      fmta([[console.log('🆑 <>', <>);]], {
        r(1, "label"),
        r(2, "value"),
      }),
      fmta([[console.log('🚧 <>', <>);]], {
        r(1, "label"),
        r(2, "value"),
      }),
      fmta([[console.log('✅ <>', <>);]], {
        r(1, "label"),
        r(2, "value"),
      }),
    }),
    {
      stored = {
        label = i(1, ""),
        value = i(2, ""),
      },
    }
  ),

  s(
    {
      name = "multiline comment",
      trig = "/**",
    },
    fmta(
      [[
      /**
       * <>
       */
      ]],
      {
        i(1, "comment"),
      }
    )
  ),
})
