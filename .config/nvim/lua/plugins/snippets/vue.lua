local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local i = ls.insert_node
local d = ls.dynamic_node
local fmta = require("luasnip.extras.fmt").fmta

ls.add_snippets("vue", {
  s(
    {
      name = "Vue event handler",
      trig = "veh",
    },
    fmta([[@<>="<>"]], {
      i(1, "click"),
      i(2, "handleClick"),
    })
  ),

  s(
    {
      name = "Import from icSrc",
      trig = "vimport",
    },
    fmta([[import Bb<> from 'icSrc/<>/<>';]], {
      i(1, "Component"),
      i(2),
      d(3, function(args)
        return sn(nil, i(1, args[1]))
      end, { 1 }),
    })
  ),
})
