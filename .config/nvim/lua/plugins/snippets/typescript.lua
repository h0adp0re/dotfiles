local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta

ls.add_snippets("typescript", {
  s(
    {
      name = "type definition",
      trig = "type",
    },
    fmta(
      [[
      type <> = {
        <>
      }
      ]],
      {
        i(1),
        i(2),
      }
    )
  ),
  s(
    {
      name = "interface definition",
      trig = "interface",
    },
    fmta(
      [[
      interface <> {
        <>
      }
      ]],
      {
        i(1),
        i(2),
      }
    )
  ),
})
