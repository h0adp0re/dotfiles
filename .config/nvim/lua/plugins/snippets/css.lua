local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta

ls.add_snippets("css", {
  s({
    name = "!important",
    trig = "!",
  }, {
    t(" !important"),
  }),

  s(
    {
      name = "@media query",
      trig = "@media",
    },
    fmta(
      [[
      @media (<>) {
        <>
      }
      ]],
      {
        i(1),
        i(2),
      }
    )
  ),
})
