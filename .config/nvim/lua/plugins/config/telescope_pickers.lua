-- Based on:
-- https://github.com/JoosepAlviste/dotfiles/blob/master/config/nvim/lua/j/telescope_custom_pickers.lua

local Path = require("plenary.path")
local scan = require("plenary.scandir")
local os_sep = Path.path.sep
local actions_set = require("telescope.actions.set")
local actions_state = require("telescope.actions.state")
local transform_mod = require("telescope.actions.mt").transform_mod
local telescope_actions = require("telescope.actions")
local sorters = require("telescope.sorters")
local conf = require("telescope.config").values
local finders = require("telescope.finders")
local make_entry = require("telescope.make_entry")
local builtin = require("telescope.builtin")
local pickers = require("telescope.pickers")

local M = {}

M.all_buffers = function(options)
  local opts = options or {}

  opts.file_ignore_patterns = {}
  opts.sort_lastused = true
  opts.show_all_buffers = true
  opts.ignore_current_buffer = true
  opts.sorter = sorters.get_substr_matcher()
  opts.attach_mappings = function(prompt_bufnr, map)
    local delete_buf = function()
      local current_picker = actions_state.get_current_picker(prompt_bufnr)
      local multi_selections = current_picker:get_multi_selection()

      if next(multi_selections) == nil then
        local selection = actions_state.get_selected_entry()

        telescope_actions.close(prompt_bufnr)
        vim.api.nvim_buf_delete(selection.bufnr, { force = true })
      else
        telescope_actions.close(prompt_bufnr)
        for _, selection in ipairs(multi_selections) do
          vim.api.nvim_buf_delete(selection.bufnr, { force = true })
        end
      end
    end

    map("i", "<C-x>", delete_buf)
    map("n", "<C-x>", delete_buf)

    return true
  end

  builtin.buffers(opts)
end

M.project_files = function(options)
  local opts = options or {}
  local is_git_repo = pcall(builtin.git_files, opts)

  if not is_git_repo then
    builtin.find_files(opts)
  end
end

M.search_dotfiles = function()
  builtin.git_files({
    prompt_title = "├ dotfiles ┤",
    cwd = "$HOME/.dotfiles",
  })
end

M.find_siblings = function()
  builtin.find_files({
    prompt_title = "Sibling Files",
    cwd = vim.fn.expand("%:p:h"),
  })
end

---Keep track of the active extension and folders for `live_grep`
local grep_filters = {
  ---@type nil|string
  extension = nil,
  ---@type nil|string[]
  directories = nil,
}

---Run the picker with the active filters (extension and folders)
local function run_picker(current_input, picker)
  local opts = {
    additional_args = grep_filters.extension and function()
      return { "-g", "*." .. grep_filters.extension }
    end,
    search_dirs = grep_filters.directories,
    default_text = current_input,
  }

  -- TODO: Resume old one with same options somehow
  if picker == "live_grep" then
    builtin.live_grep(opts)
  elseif picker == "grep_string" then
    builtin.grep_string(opts)
  end
end

M.picker_actions = transform_mod({
  ---Ask for a file extension and open a new `live_grep` filtering by it
  set_extension = function(prompt_bufnr, picker)
    local current_picker = actions_state.get_current_picker(prompt_bufnr)
    local current_input = actions_state.get_current_line()

    vim.ui.input({ prompt = "Filter by extension" }, function(input)
      if input == nil then
        return
      end

      grep_filters.extension = input

      telescope_actions._close(prompt_bufnr, current_picker.initial_mode == "insert")
      run_picker(current_input, picker)
    end)
  end,
  ---Ask the user for a folder and open a new `live_grep` filtering by it
  set_folders = function(prompt_bufnr, picker)
    local current_picker = actions_state.get_current_picker(prompt_bufnr)
    local current_input = actions_state.get_current_line()

    local data = {}
    ---@diagnostic disable-next-line: undefined-field
    scan.scan_dir(vim.uv.cwd(), {
      hidden = true,
      only_dirs = true,
      respect_gitignore = true,
      on_insert = function(entry)
        table.insert(data, entry .. os_sep)
      end,
    })
    table.insert(data, 1, "." .. os_sep)

    telescope_actions._close(prompt_bufnr, current_picker.initial_mode == "insert")
    pickers
      .new({}, {
        prompt_title = "Filter by folder",
        finder = finders.new_table({ results = data, entry_maker = make_entry.gen_from_file({}) }),
        previewer = false,
        sorter = conf.file_sorter({}),
        attach_mappings = function(prompt_bufnr)
          actions_set.select:replace(function()
            local current_picker = actions_state.get_current_picker(prompt_bufnr)

            local dirs = {}
            local selections = current_picker:get_multi_selection()
            if vim.tbl_isempty(selections) then
              table.insert(dirs, actions_state.get_selected_entry().value)
            else
              for _, selection in ipairs(selections) do
                table.insert(dirs, selection.value)
              end
            end
            grep_filters.directories = dirs

            telescope_actions.close(prompt_bufnr)
            run_picker(current_input, picker)
          end)
          return true
        end,
      })
      :find()
  end,
})

---Small wrapper over `live_grep` to first reset our active filters
M.live_grep = function()
  grep_filters.extension = nil
  grep_filters.directories = nil

  builtin.live_grep()
end

---Small wrapper over `grep_string` to first reset our active filters
M.grep_string = function()
  grep_filters.extension = nil
  grep_filters.directories = nil

  builtin.grep_string()
end

return M
