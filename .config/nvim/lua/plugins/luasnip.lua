return {
  "L3MON4D3/LuaSnip",
  build = "make install_jsregexp",
  event = "VeryLazy",
  config = function()
    local ls = require("luasnip")
    local types = require("luasnip.util.types")
    local loader = require("luasnip.loaders.from_lua")

    ls.setup({
      update_events = "TextChanged,TextChangedI",
      ext_opts = {
        [types.choiceNode] = {
          active = {
            virt_text = { { "●", "DiagnosticOk" } },
          },
        },
      },
    })

    loader.load({
      paths = {
        "~/.config/nvim/lua/plugins/snippets/",
      },
    })

    ls.filetype_extend("typescript", { "javascript" })
    ls.filetype_extend("scss", { "css" })
    ls.filetype_extend("vue", { "html", "javascript", "css", "scss" })
  end,
}
