local get_hl = require("utils").get_hl
local options = {
  separators_enabled = false,
}
local glyphs = require("constants.glyphs").statusline

if not options.separators_enabled then
  glyphs.separator.component.left = ""
  glyphs.separator.component.right = ""
  glyphs.separator.section.left = ""
  glyphs.separator.section.right = ""
end

local function diff_source()
  local gitsigns = vim.b.gitsigns_status_dict

  if gitsigns then
    return {
      added = gitsigns.added,
      modified = gitsigns.changed,
      removed = gitsigns.removed,
    }
  end
end

return {
  "nvim-lualine/lualine.nvim",
  lazy = false,
  opts = {
    options = {
      icons_enabled = true,
      theme = "auto",
      component_separators = {
        left = glyphs.separator.component.left,
        right = glyphs.separator.component.right,
      },
      section_separators = {
        left = glyphs.separator.section.left,
        right = glyphs.separator.section.right,
      },
      disabled_filetypes = {
        statusline = {},
        winbar = {},
      },
      ignore_focus = {},
      always_divide_middle = true,
      globalstatus = false,
      refresh = {
        statusline = 300,
      },
    },
    sections = {
      lualine_a = { "mode" },
      lualine_b = {
        {
          "filename",
          symbols = {
            modified = glyphs.file.modified,
            readonly = " " .. glyphs.file.readonly,
          },
        },
      },
      lualine_c = {
        { "FugitiveHead", icon = glyphs.git.branch },
        {
          "diff",
          colored = false, -- Displays a colored diff status if set to true
          symbols = {
            added = glyphs.diff.added,
            modified = glyphs.diff.modified,
            removed = glyphs.diff.removed,
          }, -- Changes the symbols used by the diff.
          source = diff_source, -- A function that works as a data source for diff.
          -- It must return a table as such:
          --   { added = add_count, modified = modified_count, removed = removed_count }
          -- or nil on failure. count <= 0 won't be displayed.
        },
      },
      lualine_x = {
        {
          "diagnostics",
          sources = { "nvim_diagnostic" },
          sections = { "error", "warn", "info", "hint" },
          diagnostics_color = {
            error = { fg = get_hl("RedSign").fg },
            warn = { fg = get_hl("YellowSign").fg },
            info = { fg = get_hl("BlueSign").fg },
            hint = { fg = get_hl("GreenSign").fg },
          },
          symbols = {
            error = " " .. glyphs.diagnostics.error .. " ",
            warn = " " .. glyphs.diagnostics.warn .. " ",
            info = " " .. glyphs.diagnostics.info .. " ",
            hint = " " .. glyphs.diagnostics.hint .. " ",
          },
          colored = true, -- Displays diagnostics status in color if set to true.
          update_in_insert = false, -- Update diagnostics in insert mode.
          always_visible = false, -- Show diagnostics even if there are none.
        },
      },
      lualine_y = { "filesize", "encoding", "filetype" },
      lualine_z = { "location", "progress" },
    },
    inactive_sections = {
      lualine_a = {},
      lualine_b = {},
      lualine_c = {
        {
          "filename",
          symbols = {
            modified = glyphs.file.modified,
            readonly = " " .. glyphs.file.readonly,
          },
        },
        {
          "diff",
          colored = false,
          symbols = {
            added = glyphs.diff.added,
            modified = glyphs.diff.modified,
            removed = glyphs.diff.removed,
          },
          source = diff_source,
        },
      },
      lualine_x = {},
      lualine_y = { "encoding", "filetype" },
      lualine_z = { "location" },
    },
  },
}
