local glyphs = require("constants.glyphs").lsp_signature

return {
  "ray-x/lsp_signature.nvim",
  lazy = false,
  opts = {
    bind = true, -- This is mandatory, otherwise border config won't get registered.
    -- If you want to hook lspsaga or other signature handler, pls set to false
    floating_window = false, -- show hint in a floating window, set to false for virtual text only mode
    floating_window_off_x = 1, -- adjust float windows x position.
    floating_window_off_y = 0, -- adjust float windows y position. e.g -2 move window up 2 lines; 2 move down 2 lines
    hint_enable = true, -- virtual hint enable
    hint_prefix = glyphs.hint_prefix,
    hi_parameter = "LspSignatureActiveParameter", -- how your parameter will be highlight
    always_trigger = false, -- sometime show signature on new line or in middle of parameter can be confusing, set it to false for #58
    padding = " ", -- character to pad on left and right of signature can be ' ', or '|'  etc
    toggle_key = "<C-h>", -- toggle signature on and off in insert mode
  },
}
