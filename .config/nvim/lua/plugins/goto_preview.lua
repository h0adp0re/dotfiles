local glyphs = require("constants.glyphs").goto_preview

return {
  "rmagatti/goto-preview",
  event = "VeryLazy",
  opts = {
    width = 80, -- Width of the floating window
    height = 16, -- Height of the floating window
    border = glyphs.border, -- Border characters of the floating window
    -- Bind default mappings:
    -- gpd - goto_preview_definition
    -- gpt - goto_preview_type_definition
    -- gpi - goto_preview_implementation
    -- gpD - goto_preview_declaration
    -- gP  - close_all_win
    -- gpr - goto_preview_references (telescope)
    default_mappings = true,
    opacity = 0, -- 0-100 opacity level of the floating window where 100 is fully transparent.
    resizing_mappings = false, -- Binds arrow keys to resizing the floating window.
    focus_on_open = true, -- Focus the floating window when opening it.
  },
}
