local glyphs = require("constants.glyphs").lsp_kinds

return {
  "hrsh7th/nvim-cmp",
  event = "VeryLazy",
  dependencies = {
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lua",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    "andersevenrud/cmp-tmux",
    "davidsierradz/cmp-conventionalcommits",
    "saadparwaiz1/cmp_luasnip",
    "chrisgrieser/cmp_yanky",
    "onsails/lspkind.nvim",
  },
  config = function()
    local cmp = require("cmp")
    local luasnip = require("luasnip")
    local lspkind = require("lspkind")
    local cmdline_mappings = {
      ["<C-e>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.abort()
        else
          fallback()
        end
      end, { "c" }),
      ["<C-j>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_next_item()
        else
          fallback()
        end
      end, { "c" }),
      ["<C-k>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_prev_item()
        else
          fallback()
        end
      end, { "c" }),
      ["<CR>"] = cmp.mapping.confirm({ select = true }),
    }
    local cmdline_formatting = {
      format = lspkind.cmp_format({
        mode = "symbol",
      }),
    }

    -- `:` cmdline setup
    cmp.setup.cmdline(":", {
      mapping = cmdline_mappings,
      sources = cmp.config.sources({
        {
          name = "cmdline",
          keyword_length = 2,
          group_index = 1,
        },
      }, {
        {
          name = "path",
          keyword_length = 1,
          group_index = 2,
        },
      }),
      formatting = cmdline_formatting,
    })

    -- `/` cmdline setup
    cmp.setup.cmdline("/", {
      mapping = cmdline_mappings,
      sources = cmp.config.sources({
        { name = "buffer" },
      }),
      formatting = cmdline_formatting,
    })

    cmp.setup({
      snippet = {
        expand = function(args)
          luasnip.lsp_expand(args.body)
        end,
      },
      window = {
        completion = cmp.config.window.bordered({
          scrolloff = 4,
          winhighlight = "FloatBorder:CmpBorder,CursorLine:Visual",
        }),
        documentation = cmp.config.window.bordered({
          winhighlight = "FloatBorder:CmpBorder",
        }),
      },
      mapping = cmp.mapping.preset.insert({
        ["<C-u>"] = cmp.mapping.scroll_docs(-4),
        ["<C-d>"] = cmp.mapping.scroll_docs(4),
        ["<C-r>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.abort()
          else
            fallback()
          end
        end, { "i", "s" }),
        ["<C-l>"] = cmp.mapping(function(fallback)
          if luasnip.choice_active() then
            luasnip.change_choice(1)
          else
            fallback()
          end
        end, { "i", "s" }),
        ["<C-j>"] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_next_item()
          elseif luasnip.expand_or_locally_jumpable() then
            luasnip.expand_or_jump()
          elseif require("neogen").jumpable() then
            require("neogen").jump_next()
          else
            fallback()
          end
        end, { "i", "s" }),
        ["<C-k>"] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_prev_item()
          elseif luasnip.jumpable(-1) then
            luasnip.jump(-1)
          elseif require("neogen").jumpable(-1) then
            require("neogen").jump_prev()
          else
            fallback()
          end
        end, { "i", "s" }),
        -- Accept currently selected item.
        -- Set `select` to `false` to only confirm explicitly selected items.
        ["<CR>"] = cmp.mapping.confirm({ select = true }),
      }),
      sources = cmp.config.sources({
        {
          name = "lazydev",
        },
        {
          -- FIX: Committing via `git commit` — conventionalcommits isn't recognized:
          -- ft isn't set correctly by Git?
          name = "conventionalcommits",
          keyword_length = 1,
        },
        {
          name = "path",
          keyword_length = 1,
        },
        {
          name = "luasnip",
          keyword_length = 1,
        },
        {
          name = "nvim_lsp",
          keyword_length = 1,
        },
        {
          name = "nvim_lua",
          keyword_length = 1,
        },
        {
          name = "neorg",
          keyword_length = 1,
        },
        {
          name = "buffer",
          keyword_length = 4,
          max_item_count = 10,
        },
        {
          name = "tmux",
          keyword_length = 4,
          max_item_count = 10,
          option = {
            all_panes = true,
            capture_history = false,
          },
        },
        {
          name = "cmp_yanky",
          keyword_length = 3,
          max_item_count = 5,
        },
      }),
      formatting = {
        fields = { "abbr", "kind", "menu" },
        format = lspkind.cmp_format({
          mode = "symbol_text",
          maxwidth = 50,
          ellipsis_char = "…",
          menu = {
            conventionalcommits = "[CC]",
            path = "[Path]",
            luasnip = "[LuaSnip]",
            nvim_lsp = "[LSP]",
            nvim_lua = "[Lua]",
            neorg = "[Neorg]",
            buffer = "[Buffer]",
            tmux = "[TMUX]",
            -- cmp_yanky = "[Yank]",
          },
          symbol_map = glyphs,
          before = function(entry, vim_item)
            if entry.source.name == "cmp_yanky" then
              vim_item.kind = "Yank"
            end

            return vim_item
          end,
        }),
      },
      experimental = { ghost_text = true },
    })
  end,
}
