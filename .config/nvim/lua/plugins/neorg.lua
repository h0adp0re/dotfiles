local mapkey = require("utils").mapkey

mapkey("n", "<leader>ji", "<cmd>Neorg journal toc open<CR>", "Journal index")
mapkey("n", "<leader>jj", "<cmd>Neorg<CR>", "Commands")
mapkey("n", "<leader>jr", "<cmd>Neorg return<CR>", "Return")
mapkey("n", "<leader>jt", "<cmd>Neorg journal today<CR>", "Today's journal")
mapkey("n", "<leader>jy", "<cmd>Neorg journal yesterday<CR>", "Yesterday's journal")
mapkey("n", "<leader>jfw", "<Plug>(neorg.telescope.switch_workspace)", "Switch workspace")
mapkey("n", "<leader>jfh", "<Plug>(neorg.telescope.search_headings)", "Search headings")
mapkey("n", "<leader>jff", "<Plug>(neorg.telescope.find_norg_files)", "Find norg files")
mapkey("n", "<leader>jfb", "<Plug>(neorg.telescope.backlinks.file_backlinks)", "Find backlinks")
mapkey("n", "<leader>jfB", "<Plug>(neorg.telescope.backlinks.header_backlinks)", "Find header backlinks")
mapkey("n", "<leader>jfl", "<Plug>(neorg.telescope.find_linkable)", "Find linkable")

return {
  "nvim-neorg/neorg",
  version = "*",
  ft = "norg",
  cmd = {
    "Neorg",
  },
  dependencies = {
    "nvim-neorg/lua-utils.nvim",
    "nvim-neotest/nvim-nio",
    "MunifTanjim/nui.nvim",
    "nvim-lua/plenary.nvim",
    "pysan3/pathlib.nvim",
    "nvim-neorg/neorg-telescope",
    "max397574/neorg-contexts",
    "benlubas/neorg-conceal-wrap",
  },
  opts = {
    load = {
      ["core.defaults"] = {},
      ["core.concealer"] = {
        config = {
          icons = {
            code_block = {
              conceal = true,
            },
          },
        },
      },
      ["core.dirman"] = {
        config = {
          workspaces = {
            notes = "~/notes",
          },
          default_workspace = "notes",
        },
      },
      ["core.esupports.metagen"] = {
        config = {
          type = "auto",
        },
      },
      ["core.summary"] = {},
      ["core.completion"] = {
        config = {
          engine = "nvim-cmp",
        },
      },
      ["core.integrations.telescope"] = {
        config = {
          insert_file_link = {
            -- Whether to show the title preview in telescope.
            -- Affects performance with a large number of files.
            show_title_preview = true,
          },
        },
      },
      ["external.context"] = {},
      ["external.conceal-wrap"] = {},
    },
  },
}
