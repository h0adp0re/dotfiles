local mapkey = require("utils").mapkey

mapkey("n", "<leader>u", "<cmd>UndotreeToggle<CR>", "Toggle undotree")

return {
  "mbbill/undotree",
  cmd = { "UndotreeToggle", "UndotreeShow" },
  init = function()
    -- Focus undotree when it's toggled
    vim.g.undotree_SetFocusWhenToggle = 1
  end,
}
