local mapkey = require("utils").mapkey

mapkey("n", "n", [[<cmd>execute('normal! ' . v:count1 . 'n')<CR><cmd>lua require("hlslens").start()<CR>]])
mapkey("n", "N", [[<cmd>execute('normal! ' . v:count1 . 'N')<CR><cmd>lua require("hlslens").start()<CR>]])
mapkey("n", "*", [[*<cmd>lua require("hlslens").start()<CR>]], "Next hlsearch match")
mapkey("n", "#", [[#<cmd>lua require("hlslens").start()<CR>]], "Previous hlsearch match")
mapkey("n", "g*", [[g*<cmd>lua require("hlslens").start()<CR>]], "Next hlsearch match")
mapkey("n", "g#", [[g#<cmd>lua require("hlslens").start()<CR>]], "Previous hlsearch match")

return {
  "kevinhwang91/nvim-hlslens",
  lazy = false,
  opts = {
    calm_down = true,
  },
}
