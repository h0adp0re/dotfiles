-- Persistent files I use in this repo (just in case)
-- .config/nvim/init.lua
-- .config/nvim/lua/sets.lua
-- .config/nvim/lua/plugins/whichkey.lua
-- .config/nvim/lua/plugins/lsp.lua
-- .config/nvim/lua/plugins/init.lua
-- .config/nvim/lua/autocmds.lua
-- .config/nvim/lua/colors.lua
-- .config/nvim/lua/utils.lua

return {
  "ThePrimeagen/harpoon",
  branch = "harpoon2",
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  opts = {
    settings = {
      save_on_toggle = true,
    },
  },
  init = function()
    local harpoon = require("harpoon")
    local mapkey = require("utils").mapkey

    local function git_branch()
      local pipe = io.popen("git branch --show-current")

      if pipe then
        local c = pipe:read("*l"):match("^%s*(.-)%s*$")

        pipe:close()

        return c
      end

      return "default list"
    end

    -- Harpoon 2 mappings
    mapkey("n", "<leader>pp", function()
      harpoon:list(git_branch()):add()
    end, "Add file to harpoon")
    mapkey("n", "<leader>pl", function()
      harpoon.ui:toggle_quick_menu(harpoon:list(git_branch()))
    end, "List marks")
    mapkey("n", "<leader>pj", function()
      harpoon:list(git_branch()):next()
    end, "Jump to next mark")
    mapkey("n", "<leader>pk", function()
      harpoon:list(git_branch()):prev()
    end, "Jump to previous mark")
    mapkey("n", "<leader>pa", function()
      harpoon:list(git_branch()):select(1)
    end, "Jump to mark 1")
    mapkey("n", "<leader>ps", function()
      harpoon:list(git_branch()):select(2)
    end, "Jump to mark 2")
    mapkey("n", "<leader>pd", function()
      harpoon:list(git_branch()):select(3)
    end, "Jump to mark 3")
    mapkey("n", "<leader>pf", function()
      harpoon:list(git_branch()):select(4)
    end, "Jump to mark 4")
    mapkey("n", "<leader>pq", function()
      harpoon:list(git_branch()):select(5)
    end, "Jump to mark 5")
    mapkey("n", "<leader>pw", function()
      harpoon:list(git_branch()):select(6)
    end, "Jump to mark 6")
    mapkey("n", "<leader>pe", function()
      harpoon:list(git_branch()):select(7)
    end, "Jump to mark 7")
    mapkey("n", "<leader>pr", function()
      harpoon:list(git_branch()):select(8)
    end, "Jump to mark 8")
  end,
}
