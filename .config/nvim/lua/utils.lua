local api = vim.api

local M = {}

--- Create a set (a table from a list)
---@param list table
---@return table
M.set = function(list)
  local set = {}

  for _, l in ipairs(list) do
    set[l] = true
  end

  return set
end

--- Create a keymapping
---@param mode string|table
---@param lhs string
---@param rhs string|function
---@param desc string|nil
---@opts[opt={ noremap = true, silent = true }] opts table|nil
M.mapkey = function(mode, lhs, rhs, desc, opts)
  opts = opts or { noremap = true, silent = true }
  opts.desc = desc

  vim.keymap.set(mode, lhs, rhs, opts)
end

--- Get a highlight group's colors
---@param hl_name string
---@return table|boolean
M.get_hl = function(hl_name)
  local is_ok, hl_def = pcall(api.nvim_get_hl, 0, { name = hl_name, link = false })

  if not is_ok then
    return false
  end

  for _, key in pairs({ "fg", "bg", "sp" }) do
    if hl_def[key] then
      hl_def[key] = string.format("#%06x", hl_def[key])
    end
  end

  return hl_def
end

--- Set a highlight group's colors
---@param hl_name string
---@param opts table
M.set_hl = function(hl_name, opts)
  api.nvim_set_hl(0, hl_name, opts)
end

--- Modify a highlight group in place, even if it was created via linking
---@param hl_name string
---@param opts table
M.mod_hl = function(hl_name, opts)
  local is_ok, hl_def = pcall(api.nvim_get_hl, 0, { name = hl_name, link = false })

  if is_ok then
    for k, v in pairs(opts) do
      hl_def[k] = v
    end

    api.nvim_set_hl(0, hl_name, hl_def)
  end
end

return M
