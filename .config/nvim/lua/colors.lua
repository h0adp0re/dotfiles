local get_hl = require("utils").get_hl
local set_hl = require("utils").set_hl
local mod_hl = require("utils").mod_hl

if vim.g.colors_name == "gruvbox-material" then
  -- Gruvbox Material highlight modifications
  local gbmHighlightMods = {
    -- Quickfix active line
    ["QuickfixLine"] = {
      bg = get_hl("Visual").bg,
      fg = get_hl("Yellow").fg,
    },
    -- DiffText
    ["DiffText"] = {
      bg = "#076678",
      fg = "#e0e9d3",
    },
    -- Cursor line number
    ["CursorLineNr"] = {
      fg = get_hl("Yellow").fg,
    },
    -- Search
    ["Search"] = {
      bg = get_hl("Normal").fg,
    },
    ["IncSearch"] = {
      bg = get_hl("Yellow").fg,
    },
    -- nvim-hlslens
    ["HlSearchLens"] = {
      bg = get_hl("Normal").fg,
      bold = true,
    },
    ["HlSearchLensNear"] = {
      bg = get_hl("Yellow").fg,
      bold = true,
    },
    ["HlSearchNear"] = {
      bg = get_hl("Yellow").fg,
    },
  }
  for group, opts in pairs(gbmHighlightMods) do
    mod_hl(group, opts)
  end

  -- Gruvbox Material highlight sets
  local gbmHighlightSets = {
    -- Float window
    ["FloatBorder"] = {
      fg = "Grey35",
    },
    ["FloatTitle"] = {
      fg = "Grey54",
    },
    -- Telescope
    ["TelescopeSelection"] = {
      bg = get_hl("Visual").bg,
      bold = true,
    },
    ["TelescopeSelectionCaret"] = {
      bg = get_hl("Visual").bg,
      fg = get_hl("Aqua").fg,
    },
    ["TelescopePromptPrefix"] = {
      fg = get_hl("Yellow").fg,
    },
    ["@string.special.url.comment"] = {
      bg = get_hl("InfoLine").bg,
      fg = get_hl("InfoFloat").fg,
    },
    ["DebugPC"] = {
      bg = get_hl("WarningLine").bg,
    },
    ["DapStopped"] = {
      fg = get_hl("WarningFloat").fg,
    },
    ["DapBreakpoint"] = {
      fg = get_hl("Red").fg,
    },
    ["DapLogPoint"] = {
      fg = get_hl("Blue").fg,
    },
    ["DapUIStopNC"] = {
      fg = get_hl("Red").fg,
    },
    ["DapUIRestartNC"] = {
      fg = get_hl("Green").fg,
    },
    ["DapUIPlayPauseNC"] = {
      fg = get_hl("Green").fg,
    },
    ["DapUIStepOverNC"] = {
      fg = get_hl("Blue").fg,
    },
    ["DapUIStepIntoNC"] = {
      fg = get_hl("Blue").fg,
    },
    ["DapUIStepBackNC"] = {
      fg = get_hl("Blue").fg,
    },
    ["DapUIStepOutNC"] = {
      fg = get_hl("Blue").fg,
    },
    ["DapUIUnavailableNC"] = {
      fg = get_hl("Whitespace").fg,
    },
  }
  for group, opts in pairs(gbmHighlightSets) do
    set_hl(group, opts)
  end

  local gbmHighlightLinks = {
    -- Float window
    ["NormalFloat"] = "Normal",
    ["TroublePreview"] = "Visual",
    ["LspInfoBorder"] = "FloatBorder",
    ["HarpoonBorder"] = "FloatBorder",
    ["HarpoonTitle"] = "FloatTitle",
    ["TreesitterContext"] = "CursorLine",
    ["TreesitterContextLineNumber"] = "Normal",
    -- Telescope
    ["TelescopeBorder"] = "FloatBorder",
    ["TelescopeTitle"] = "FloatTitle",
    ["TelescopePromptCounter"] = "FloatTitle",
    -- Treesitter
    ["@error"] = "Blue",
    ["@define"] = "TermCursor",
    ["@punctuation.special"] = "Red",
    ["@string"] = "Green",
    ["@string.regex"] = "Aqua",
    ["@string.escape"] = "Yellow",
    ["@function"] = "Function",
    ["@parameter"] = "Blue",
    ["@parameter.definition"] = "@parameter",
    ["@field"] = "Aqua",
    ["@property"] = "@field",
    ["@variable"] = "Yellow",
    ["@variable.member"] = "@field",
    ["@variable.builtin"] = "Blue",
    ["@variable.parameter"] = "@parameter",
    ["@constant"] = "Purple",
    ["@tag"] = "Red",
    ["@markup.heading.gitcommit"] = "@annotation",
    ["@keyword.gitcommit"] = "@type",
    ["@string.special.url.gitcommit"] = "@string",
    ["@function.diff"] = "@field",
    ["Todo"] = "TodoBgTODO",
    ["DapUIFloatBorder"] = "FloatBorder",
    ["DapUIUnavailable"] = "Whitespace",
    ["DapUIBreakpointsDisabledLine"] = "Whitespace",
    ["DapUIWinSelect"] = "BlueBold",
    ["DapUIStop"] = "Red",
    ["DapUIRestart"] = "Green",
    ["DapUIPlayPause"] = "Green",
    ["DapUIStepOver"] = "Blue",
    ["DapUIStepInto"] = "Blue",
    ["DapUIStepBack"] = "Blue",
    ["DapUIStepOut"] = "Blue",
  }
  for newgroup, oldgroup in pairs(gbmHighlightLinks) do
    mod_hl(newgroup, { link = oldgroup })
  end
elseif string.find(vim.g.colors_name, "catppuccin") ~= nil then
  -- Catppuccin highlight links
  local catppuccinHighlightLinks = {
    -- Harpoon
    ["HarpoonBorder"] = "FloatBorder",
    ["HarpoonTitle"] = "FloatTitle",
    -- TreesitterContext
    ["TreesitterContext"] = "CursorLine",
    ["TreesitterContextBottom"] = "CursorLine",
    ["TreesitterContextLineNumber"] = "Normal",
  }
  for newgroup, oldgroup in pairs(catppuccinHighlightLinks) do
    mod_hl(newgroup, { link = oldgroup })
  end
end

local highlightMods = {
  -- Bold cursor line number
  ["CursorLineNr"] = { bold = true },
  -- Bold quickfix active line
  ["QuickfixLine"] = { bold = true },
  -- Diagnostics float text background
  ["ErrorFloat"] = { bg = "NONE" },
  ["WarningFloat"] = { bg = "NONE" },
  ["InfoFloat"] = { bg = "NONE" },
  ["HintFloat"] = { bg = "NONE" },
  -- Diagnostics virtual text background
  -- ["VirtualTextError"] = { bg = get_hl("ErrorLine").bg },
  -- ["VirtualTextWarning"] = { bg = get_hl("WarningLine").bg },
  -- ["VirtualTextInfo"] = { bg = get_hl("InfoLine").bg },
  -- ["VirtualTextHint"] = { bg = get_hl("HintLine").bg },
}
for group, opts in pairs(highlightMods) do
  mod_hl(group, opts)
end

local highlightLinks = {
  -- highlight-undo.nvim
  ["HighlightUndo"] = "ErrorLine",
  ["HighlightRedo"] = "HintLine",
  -- nvim-cmp highlights
  ["CmpBorder"] = "FloatBorder",
  ["CmpItemMenu"] = "NonText",
  -- diff highlights
  ["DiffViewDiffDelete"] = "NonText",
  ["DiffViewDiffDeleteDim"] = "NonText",
  ["DiffViewDim1"] = "NonText",
  ["DiffDelete"] = "NonText",
  -- LSP highlights
  ["LspReferenceRead"] = "Visual",
  ["LspReferenceText"] = "Visual",
  ["LspReferenceWrite"] = "Visual",
  -- gitsigns.nvim highlights
  ["GitSignsAddLn"] = "Visual",
  ["GitSignsChangeDelete"] = "GitSignsDelete",
  ["GitSignsChangeDeleteNr"] = "GitSignsDelete",
  ["GitSignsChangeDeleteLn"] = "DiffChange",
  ["GitSignsStagedChangedelete"] = "GitSignsStagedDelete",
  -- whichkey.nvim highlights
  ["WhichKeyFloat"] = "StatusLine",
  ["WhichKeySeparator"] = "NonText",
  -- Neotest highlights
  ["NeotestAdapterName"] = "Aqua",
  -- ["NeotestBorder"] = "",
  ["NeotestDir"] = "Blue",
  ["NeotestExpandMarker"] = "NonText",
  ["NeotestFailed"] = "Red",
  ["NeotestFile"] = "Blue",
  -- ["NeotestFocused"] = "",
  ["NeotestIndent"] = "NonText",
  ["NeotestMarked"] = "Purple",
  ["NeotestNamespace"] = "Green",
  ["NeotestPassed"] = "Green",
  ["NeotestRunning"] = "Yellow",
  -- ["NeotestWinSelect"] = "",
  ["NeotestSkipped"] = "NonText",
  ["NeotestTarget"] = "HintLine",
  -- ["NeotestUnknown"] = "",
  -- Yank region highlight
  ["HighlightedyankRegion"] = "Visual",
  ["YankyYanked"] = "HighlightedyankRegion",
  ["YankyPut"] = "HighlightedyankRegion",
}
for newgroup, oldgroup in pairs(highlightLinks) do
  mod_hl(newgroup, { link = oldgroup })
end
