local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
local glyphs = require("constants.glyphs").lazy

-- Map leader to <Space>
vim.g.mapleader = " "

-- Map localleader to ,
vim.g.maplocalleader = "ü"

-- A builtin Lua module which byte-compiles and caches Lua files (speeds up load times).
vim.loader.enable()

---@diagnostic disable-next-line: undefined-field
if not vim.uv.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins", {
  defaults = {
    lazy = true,
  },
  install = {
    colorscheme = { "gruvbox-material" },
  },
  ui = {
    border = "rounded",
    backdrop = 100,
    icons = glyphs,
    title = " Plugins ",
  },
})

require("sets")
require("autocmds")
require("colors")
require("tabline")
require("abbreviations")

-- Environment specific settings
if vim.fn.filereadable(vim.env.HOME .. "/.config/vim/env.vim") == 1 then
  vim.cmd("so $HOME/.config/vim/env.vim")
end
