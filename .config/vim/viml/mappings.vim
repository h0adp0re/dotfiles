" Native {{{
" Map leader to space
let mapleader = "\<Space>"

" Source vimrc
nnoremap <leader>vs :so $MYVIMRC<CR>

" Close buffer without closing window
nnoremap <leader>bd :bp<bar>sp<bar>bn<bar>bd<CR>

" Go to previous buffer
nnoremap <leader>bb :b#<CR>

" Replace the last searched text with the last yanked text
nnoremap <leader>rr :%s//\=@"/g<CR>

" Fold to matching pair at the end of the line
nnoremap <leader>zf $zf%

" Remove search highlighting
nnoremap <silent> <leader>l :noh<CR>

" Append the current date at the end of the line
nnoremap <leader>00 A - <C-r>=strftime("%d.%m.%Y")<CR><Esc>

" Write a diff markdown link on the next line
nnoremap <leader>0+ o<Esc>0C[(diff)]()<Esc>i

function! TwiddleCase(str)
  if a:str ==# toupper(a:str)
    let result = tolower(a:str)
  elseif a:str ==# tolower(a:str)
    let result = substitute(a:str,'\(\<\w\+\>\)', '\u\1', 'g')
  else
    let result = toupper(a:str)
  endif
  return result
endfunction

" Toggle word case
vnoremap ~ y:call setreg('', TwiddleCase(@"), getregtype(''))<CR>gv""Pgv

" make Y consistent with D and C
map Y y$

" Toggle QF list
nmap <C-q> <Plug>(qf_qf_toggle)

" Navigate command history on the home row
cnoremap <C-k> <Up>
cnoremap <C-j> <Down>

" Prevent the use of arrow keys in normal mode
nnoremap <Up> :echoerr "Use k instead"<CR>
nnoremap <Down> :echoerr "Use j instead"<CR>
nnoremap <Right> :echoerr "Use l instead"<CR>
nnoremap <Left> :echoerr "Use h instead"<CR>

" Prevent the use of arrow keys in insert mode
inoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Right> <NOP>
inoremap <Left> <NOP>

" Movement for operators which manipulates the contents of the next parentheses (ex. `din(`)
onoremap in( :<C-u>normal! f(vi(<CR>
" Movement for operators which manipulates the contents of the previous parentheses (ex. `dil(`)
onoremap il( :<C-u>normal! F)vi(<CR>
" Movement for operators which manipulates the contents of the next quotes (ex. `din(`)
onoremap in" :<C-u>normal! f"vi"<CR>
" Movement for operators which manipulates the contents of the previous quotes (ex. `dil(`)
onoremap il" :<C-u>normal! F"vi"<CR>
" }}}

" nnn.vim {{{
nnoremap <silent> <leader>en :NnnPicker %:p:h<CR>
" }}}

" ranger.vim {{{
nnoremap <silent> <leader>er :Ranger<CR>
" }}}

" vim-fugitive {{{
nnoremap <leader>gg :Git<CR>
nmap <leader>gw :Gwrite<CR>
nnoremap <leader>gp :Git push<CR>
nnoremap <leader>gP :Git push --force-with-lease<CR>
nnoremap <leader>gl :Git pull<CR>
nnoremap <leader>gL :Git log<CR>
nnoremap <leader>go :GBrowse<CR>
vnoremap <leader>go :'<,'>GBrowse<CR>

" Get diff from right side
nmap <expr> <C-h> &diff ? '<cmd>diffget //3<CR>' : '<C-h>'
" Get diff from left side
nmap <expr> <C-l> &diff ? '<cmd>diffget //2<CR>' : '<C-l>'

" Jump to next hunk when in diff mode
" otherwise, next quickfix entry
nmap <expr> <C-j> &diff ? ']c' : '<Plug>(qf_qf_next)'
" Jump to previous hunk when in diff mode
" otherwise, previous quickfix entry
nmap <expr> <C-k> &diff ? '[c' : '<Plug>(qf_qf_previous)'

" Open git blame
nnoremap <leader>gB :Git blame<CR>

" Staging/unstaging maps

" s         Stage (add) the file or hunk under the cursor.
" u         Unstage (reset) the file or hunk under the cursor.
" - / a     Stage or unstage the file or hunk under the cursor.
" U         Unstage everything.
" X         Discard the change under the cursor.  This uses
" =         Toggle an inline diff of the file under the cursor.

" Diff maps

" dd        Perform a |:Gdiffsplit| on the file under the cursor.
" dv        Perform a |:Gvdiffsplit| on the file under the cursor.
" ds / dh   Perform a |:Ghdiffsplit| on the file under the cursor.
" dq        Close all but one diff buffer, and |:diffoff|! the

" Navigation maps

" (         Jump to the previous file, hunk, or revision.
" )         Jump to the next file, hunk, or revision.
" i         Jump to the next file or hunk, expanding inline diffs
" gu        Jump to file [count] in the "Untracked" or "Unstaged"
" gU        Jump to file [count] in the "Unstaged" section.
" gs        Jump to file [count] in the "Staged" section.
" gp        Jump to file [count] in the "Unpushed" section.
" gP        Jump to file [count] in the "Unpulled" section.
" gr        Jump to file [count] in the "Rebasing" section.

" Commit maps

" cc        Create a commit.
" ca        Amend the last commit and edit the message.
" ce        Amend the last commit without editing the message.
" cw        Reword the last commit.
" cvc       Create a commit with -v.
" cva       Amend the last commit with -v
" c<Space>  Populate command line with ":Git commit ".
" }}}

" vim-twiggy {{{
nnoremap <leader>gb :Twiggy<CR>
" }}}

" undotree {{{
" Toggle undotree
nnoremap <leader>u :UndotreeToggle<CR>
" }}}

" splitjoin.vim {{{
nnoremap <silent> <leader>mj :SplitjoinJoin<CR>
nnoremap <silent> <leader>mk :SplitjoinSplit<CR>
" }}}

" sideways.vim {{{
nnoremap <silent> <leader>mh :SidewaysLeft<CR>
nnoremap <silent> <leader>ml :SidewaysRight<CR>
nnoremap <silent> <leader>nh :SidewaysJumpLeft<CR>
nnoremap <silent> <leader>nl :SidewaysJumpRight<CR>
nmap <silent> <leader>ii <Plug>SidewaysArgumentInsertBefore
nmap <silent> <leader>ia <Plug>SidewaysArgumentAppendAfter
nmap <silent> <leader>iI <Plug>SidewaysArgumentInsertFirst
nmap <silent> <leader>iA <Plug>SidewaysArgumentAppendLast
" Create shortcuts for list item outer and inner text objects
omap aa <Plug>SidewaysArgumentTextobjA
xmap aa <Plug>SidewaysArgumentTextobjA
omap ia <Plug>SidewaysArgumentTextobjI
xmap ia <Plug>SidewaysArgumentTextobjI
" }}}

" coc.nvim {{{
" Go to definition
nmap <silent> gd <Plug>(coc-definition)

" Code actions
nnoremap <leader>a <Plug>(coc-codeaction-line)

" Use <Tab> and <S-Tab> to navigate completion list:
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Insert <Tab> when previous text is space, refresh completion if not.
inoremap <silent><expr> <Tab>
  \ coc#pum#visible() ? coc#pum#next(1):
  \ <SID>check_back_space() ? "\<Tab>" :
  \ coc#refresh()
inoremap <expr><S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Use <C-j>, <C-k>, <C-n> and <C-p> to navigate completion list:
inoremap <silent><expr> <C-j> coc#pum#visible() ? coc#pum#next(1) : "\<C-j>"
inoremap <silent><expr> <C-k> coc#pum#visible() ? coc#pum#prev(1) : "\<C-k>"
inoremap <silent><expr> <C-n> coc#pum#visible() ? coc#pum#next(0) : "\<C-n>"
inoremap <silent><expr> <C-p> coc#pum#visible() ? coc#pum#prev(0) : "\<C-p>"

" Use <C-d> and <C-u> to scroll:
inoremap <silent><expr> <C-d> coc#pum#visible() ? coc#pum#scroll(1) : "\<C-d>"
inoremap <silent><expr> <C-u> coc#pum#visible() ? coc#pum#scroll(0) : "\<C-u>"

" Use <CR> and <Esc> to confirm and cancel completion:
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
inoremap <silent><expr> <Esc> coc#pum#visible() ? coc#pum#cancel() : "\<Esc>"

" CoC CodeLens
nmap <silent> gl <Plug>(coc-codelens-action)

" coc-yank {{{
" Toggle coc-yank
nnoremap <silent> <leader>yh  :<C-u>CocList -A --normal yank<CR>
" Clear yank history
nnoremap <silent> <leader>yc  :CocCommand yank.clean<CR>
" }}}

" coc-vitest {{{
" Run Vitest for current project
command! -nargs=0 VitestProject :call CocAction('runCommand', 'vitest.projectTest')

" Run Vitest for current file
command! -nargs=0 VitestFile :call CocAction('runCommand', 'vitest.fileTest', ['%'])

" Run Vitest for single (nearest) test
command! -nargs=0 Vitest :call CocAction('runCommand', 'vitest.singleTest', ['%'])

" Run Vitest for current project
nnoremap <leader>tp :VitestProject<CR>

" Run Vitest for current file
nnoremap <leader>tf :VitestFile<CR>

" Run Vitest for single (nearest) test
nnoremap <leader>ts :Vitest<CR>
" }}}
" }}}

" vim-which-key {{{
" Map space to trigger WhichKey
nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
" }}}

" ale {{{
nnoremap <silent> <leader>dj :ALENextWrap<CR>
nnoremap <silent> <leader>dk :ALEPreviousWrap<CR>
nnoremap <silent> <leader>dd :ALEDetail<CR>
" }}}

" fzf.vim {{{
" Command palette
nnoremap <leader>fc :Commands<CR>

" Search files in the project
nnoremap <leader>ff :GFiles<CR>

" Search the project
nnoremap <leader>fp :Ag<space>

" Search lines in current buffer
nnoremap <leader>fl :BLines<CR>

" List buffers
nnoremap <leader>fb :Buffers<CR>

" Vim help tags
nnoremap <leader>vh :Helptags<CR>
" }}}

" vim-gitgutter {{{
" Cycle around to the last hunk if beginning is reached
function! GitGutterPrevHunkCycle()
  let line = line('.')
  silent! GitGutterPrevHunk
  if line('.') == line
    normal! G
    GitGutterPrevHunk
  endif
endfunction
" Cycle around to the first hunk if end is reached
function! GitGutterNextHunkCycle()
  let line = line('.')
  silent! GitGutterNextHunk
  if line('.') == line
    1
    GitGutterNextHunk
  endif
endfunction

" Navigate between hunks in current buffer
nnoremap <leader>hj :call GitGutterNextHunkCycle()<CR>
nnoremap <leader>hk :call GitGutterPrevHunkCycle()<CR>
nnoremap <leader>hu :GitGutterUndoHunk<CR>
nnoremap <leader>hs :GitGutterStageHunk<CR>
nnoremap <leader>hp :GitGutterPreviewHunk<CR>
nnoremap <leader>hz :GitGutterFold<CR>
" }}}

" vim-commentary {{{
" Toggle comment
nnoremap <silent> <leader>c :Commentary<CR>
vnoremap <silent> <leader>c :'<,'>Commentary<CR>
" }}}

" tmuxline.vim {{{
nnoremap <leader>xs :Tmuxline lightline<CR>
nnoremap <leader>xg :TmuxlineSnapshot! $HOME/.dotfiles/.config/tmux/tmuxline/gruvbox-material.tmux.conf<CR>
" }}}
