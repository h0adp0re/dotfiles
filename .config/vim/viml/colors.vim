" Enable true colors support
if (has("termguicolors"))
  set termguicolors

  " Sometimes setting 'termguicolors' is not enough
  " and one has to set the t_8f and t_8b options explicitly
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

" Explicitly tell vim that the terminal supports 256 colors
set t_Co=256

" Enable dark background theme
set background=dark " (default in nvim)

" source $HOME/.config/vim/viml/theme/gruvbox.vim
source $HOME/.config/vim/viml/theme/gruvbox-material.vim

" Remove vim background
highlight! Normal ctermbg=NONE guibg=NONE
highlight! NonText ctermbg=NONE guibg=NONE
highlight! EndOfBuffer ctermbg=NONE guibg=NONE
highlight! SignColumn ctermbg=NONE guibg=NONE

" Highlight cursor line and line numbers
highlight! CursorLineNr ctermfg=003 ctermbg=NONE guifg=#d8a657 guibg=NONE

" Quickfix active line
highlight! QuickfixLine term=bold cterm=bold ctermfg=001 ctermbg=003 gui=bold guifg=#282828 guibg=#d8a657

highlight! DiffText ctermbg=017 guibg=#076678 guifg=#e0e9d3

highlight! link NormalFloat Normal
highlight! link FloatBorder Normal
highlight! link FloatTitle Normal
highlight! link LspInfoBorder FloatBorder

highlight! link WhichKeyFloating StatusLine
highlight! link WhichKeySeperator NonText
highlight! link WhichKey Red
highlight! link WhichKeyGroup Yellow
highlight! link WhichKeyDesc Blue

highlight! LightlineRight_normal_warning cterm=bold ctermfg=235 ctermbg=214 gui=bold guifg=#282828 guibg=#d8a657
highlight! LightlineRight_active_warning cterm=bold ctermfg=235 ctermbg=214 gui=bold guifg=#282828 guibg=#d8a657
highlight! LightlineRight_insert_warning cterm=bold ctermfg=235 ctermbg=214 gui=bold guifg=#282828 guibg=#d8a657
highlight! LightlineRight_visual_warning cterm=bold ctermfg=235 ctermbg=214 gui=bold guifg=#282828 guibg=#d8a657
highlight! LightlineRight_select_warning cterm=bold ctermfg=235 ctermbg=214 gui=bold guifg=#282828 guibg=#d8a657

highlight! LightlineRight_normal_error cterm=bold ctermfg=235 ctermbg=167 gui=bold guifg=#282828 guibg=#ea6962
highlight! LightlineRight_active_error cterm=bold ctermfg=235 ctermbg=167 gui=bold guifg=#282828 guibg=#ea6962
highlight! LightlineRight_insert_error cterm=bold ctermfg=235 ctermbg=167 gui=bold guifg=#282828 guibg=#ea6962
highlight! LightlineRight_visual_error cterm=bold ctermfg=235 ctermbg=167 gui=bold guifg=#282828 guibg=#ea6962
highlight! LightlineRight_select_error cterm=bold ctermfg=235 ctermbg=167 gui=bold guifg=#282828 guibg=#ea6962
