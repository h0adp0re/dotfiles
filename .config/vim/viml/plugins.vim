call plug#begin('$HOME/.config/vim/bundle')

" Essentials {{{
Plug 'wincent/terminus'
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mcchrish/nnn.vim', { 'on': 'NnnPicker' }
Plug 'francoiscabrol/ranger.vim', { 'on': 'Ranger' }
Plug 'editorconfig/editorconfig-vim'
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
" Plug 'dense-analysis/ale'
Plug 'sheerun/vim-polyglot'
" }}}

" Git tools {{{
Plug 'tpope/vim-fugitive'
Plug 'sodapopcan/vim-twiggy'
Plug 'kmARC/vim-fubitive'
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'airblade/vim-gitgutter'
Plug 'kristijanhusak/vim-create-pr'
" }}}

" UX {{{
Plug 'mbbill/undotree', { 'on': [ 'UndotreeToggle', 'UndotreeShow' ] }
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
Plug 'godlygeek/tabular', { 'for': 'markdown' }
Plug 'romainl/vim-qf'
" Plug 'alvan/vim-closetag'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'AndrewRadev/sideways.vim'
Plug 'tpope/vim-surround'
Plug 'delphinus/vim-auto-cursorline'
Plug 'tweekmonster/startuptime.vim', { 'on': 'StartupTime' }
Plug 'itchyny/lightline.vim'
Plug 'mhinz/vim-startify'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-commentary', { 'on': 'Commentary' }
Plug 'suy/vim-context-commentstring'
Plug 'liuchengxu/vim-which-key', { 'on': [ 'WhichKey', 'WhichKey!' ] }
" }}}

" Appearance {{{
Plug 'sainnhe/gruvbox-material'
Plug 'gruvbox-community/gruvbox'
Plug 'edkolev/tmuxline.vim', { 'on': [ 'Tmuxline', 'TmuxlineSnapshot' ] }
Plug 'Yggdroot/indentLine'
" }}}

" Environment specific plugins & settings
let g:path_to_env = $HOME . '/.config/vim/env.vim'

if filereadable(g:path_to_env)
  so $HOME/.config/vim/env.vim
endif

call plug#end()

" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif
