" netrw {{{
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_sort_options = 'i'
let g:netrw_browse_split = 2
let g:netrw_winsize = 25
" }}}

" vim-polyglot {{{
" Only load needed syntax files for vue
" let g:vue_pre_processors = [ 'typescript', 'scss' ]

" Disable automatic keymaps of vim-markdown
let g:vim_markdown_no_default_key_mappings = 1

" Disable folding in markdown
let g:vim_markdown_folding_disabled = 1

" Disable concealing markdown syntax
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0
" }}}

" nnn.vim {{{
" Disable default mappings
let g:nnn#set_default_mappings = 0

" Change how nnn launches
let g:nnn#command = 'nnn -cdEHS'

" Floating window. This is the default
let g:nnn#layout = {
\   'window': {
\     'width': 0.9,
\     'height': 0.7,
\     'highlight': 'Comment',
\   },
\ }

" Custom colors
let $NNN_FCOLORS='c1e20402006006f705d60609'
let $NNN_OPENER='vim'
" }}}

" ranger.vim {{{
let g:ranger_map_keys = 0
" }}}

" vim-twiggy {{{
let g:twiggy_group_locals_by_slash = 0
let g:twiggy_local_branch_sort = 'mru'
let g:twiggy_remote_branch_sort = 'date'
" }}}

" undotree {{{
" Focus undotree when it's toggled
let g:undotree_SetFocusWhenToggle = 1
" }}}

" vim-auto-cursorline {{{
let g:auto_cursorline_wait_ms = 300
" }}}

if executable('tmux') && filereadable(expand('$ZDOTDIR/.zshrc')) && $TMUX !=# ''
" tmuxline.vim {{{
  let g:tmuxline_preset = {
  \   'a': '#S',
  \   'b': '#P',
  \   'c': [
  \     '$statusbar_weather',
  \     '$statusbar_date',
  \     '$statusbar_time'
  \   ],
  \   'win': [ '#I', '#W' ],
  \   'cwin': [ '#I', '#W', '#[fg=#{stat_color_low}]#F' ],
  \   'x': [
  \     '$statusbar_edge_element',
  \     '$statusbar_mem',
  \     '$statusbar_cpu',
  \   ],
  \   'y': '$statusbar_cpu_temp',
  \   'z': '$statusbar_hostname',
  \ }
  let g:tmuxline_separators = {
  \   'left': '$statusbar_separator_left',
  \   'left_alt': '$statusbar_separator_left_alt',
  \   'right': '$statusbar_separator_right',
  \   'right_alt' : '$statusbar_separator_right_alt',
  \   'space': ' ',
  \ }
" }}}
endif

" vim-startify {{{
" Auto-save startify sessions
let g:startify_session_persistence = 1

" Only display sessions, without a header
let g:startify_lists = [ { 'type': 'sessions' } ]

" Custom startify header
let g:startify_custom_header = [
\ '                   ⣠⣦⡀                    ',
\ '   ⣴⡿⣛⣛⣛⣛⣛⣛⣛⣛⣛⣛⣛⡻⣷⣞⣡⣧⡙⢦⣠⣾⠟⣛⣛⣛⣛⣛⣛⣛⣛⣛⣛⣛⡻⣷⡄  ',
\ '   ⣿⢸           ⢸⣿⣿⣿⣿⣿⣦⣹⣿⢸           ⢸⣿⡇  ',
\ '   ⠻⣶⣿⡏⡆      ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⡿⢻       ⢀⣴⣿⠟⠁  ',
\ '     ⣿⡇⡇      ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⠔⠁     ⢀⣴⣿⠟⠁    ',
\ '     ⣿⡇⡇      ⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⠔⠁     ⢀⣴⣿⠟⠁      ',
\ '     ⣿⡇⡇      ⣿⣿⣿⣿⣿⣿⡿⢋⠔⠁     ⢀⣴⣿⣿⡁        ',
\ '     ⣿⡇⡇      ⣿⣿⣿⣿⡿⢋⠔⠁     ⢀⣴⣿⣿⣿⣦⡙⢦⡀      ',
\ '   ⣠⠞⣿⡇⡇      ⣿⣿⡿⢋⠔⠁     ⢀⣴⣿⣿⣿⣿⣿⣿⣿⣦⡙⢦⡀    ',
\ ' ⣠⠞⣡⣾⣿⡇⡇      ⣿⢏⠔⠁     ⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡙⢦⡀  ',
\ '⠈⠻⣿⣝⢿⣿⡇⡇      ⠟⠁     ⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⣫⡾⠋   ',
\ '  ⠈⠻⣷⣿⡇⡇           ⢠⡞⠉⢹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⣫⡾⠋     ',
\ '    ⠈⣿⡇⡇         ⢀⣴⣿⣷⣶⣾⣿⣿⣿⣿⣿⣿⣿⣿⡿⣫⡾⠋       ',
\ '     ⣿⡇⡇       ⢀⣴⣿⣛⠛⠛⣻⣿⣟⡛⠛⠻⣿⡿⠛⠛⠻⣯⡾⠛⠛⣧     ',
\ '     ⣿⡇⡇     ⢀⣴⣿⣿⣿⠇ ⢠⣿⣿⡿ ⢠⣤⡤⡤ ⢠⡤⠤⡤ ⢠⡟     ',
\ '     ⣿⡇⡇   ⢀⣴⣿⣿⣿⣿⡟  ⣾⣿⣿⠃ ⡾⣫⣾⠃ ⣾⠃⢰⠃ ⣾⠃     ',
\ '     ⣿⡇⡇ ⢀⣴⣿⣿⠿⣷⣝⣿⠁ ⣸⣿⣿⡏ ⣸⣿⠋⡏ ⣸⣏ ⡏ ⣸⣯⡀     ',
\ '     ⠻⢷⣾⣿⣿⣿⠟⠁ ⠈⠻⣷⣶⢶⣶⣿⡿⣲⡶⠚⠋⠘⠒⠒⠒⠋⠘⠒⠒⠒⠚⠁     ',
\ '                ⠈⠻⣷⡝⣫⡾⠋                   ',
\ '                  ⠈⠳⠋                     ',
\ '',
\ ]
" }}}

" coc.nvim {{{
" CoC extensions
let g:coc_global_extensions = [
\   '@yaegassy/coc-vitest',
\   '@yaegassy/coc-volar',
\   '@yaegassy/coc-tailwindcss3',
\   'coc-css',
\   'coc-eslint',
\   'coc-html',
\   'coc-json',
\   'coc-markdownlint',
\   'coc-stylelintplus',
\   'coc-sh',
\   'coc-snippets',
\   'coc-tsserver',
\   'coc-vimlsp',
\   'coc-yank'
\ ]
" }}}

" ale {{{
" Display ALE errors with neovim virtual text
let g:ale_virtualtext_cursor = 1
let g:ale_virtualtext_prefix = '  ▋ '

" Git information is more valuable
let g:ale_sign_priority = 10

" ALE message template
let g:ale_echo_msg_format = '[%severity%] [%linter% %code%] %s'

" Disable ALE gutter signs
" let g:ale_set_signs = 0

" ALE gutter symbols
" let g:ale_sign_warning = "\uf529"
" let g:ale_sign_error = "\uf00d"
" let g:ale_sign_info = "\uf05a"
let g:ale_sign_warning = ""
let g:ale_sign_error = ""
let g:ale_sign_info = ""
" }}}

" vim-gitgutter {{{
" Don't let vim-gitgutter set up any mappings at all
let g:gitgutter_map_keys = 0
" Override ALE signs and others
let g:gitgutter_sign_priority = 100
let g:gitgutter_sign_allow_clobber = 1
let g:gitgutter_sign_added = '│'
let g:gitgutter_sign_modified = '│'
let g:gitgutter_sign_removed = '_'
let g:gitgutter_sign_removed_first_line = '‾'
let g:gitgutter_sign_modified_removed = '│'
" }}}

" lightline.vim {{{
let g:lightline = {}
let g:lightline.colorscheme = 'gruvbox_material'
let g:lightline.component = {
\   'lineinfo': '%3l:%-2v%<',
\ }
let g:lightline.component_function = {
\   'gitbranch': 'LightlineGitBranch',
\   'filename': 'LightlineFilename',
\   'filetype': 'LightlineFiletype',
\   'fileencoding': 'LightlineFileencoding',
\   'gitdiff': 'LightlineGitDiff',
\   'cocstatus': 'coc#status',
\ }
let g:lightline.component_expand = {
\   'linter_checking': 'LightlineALEChecking',
\   'linter_ok': 'LightlineALEOk',
\   'linter_info': 'LightlineALEInfo',
\   'linter_warnings': 'LightlineALEWarnings',
\   'linter_errors': 'LightlineALEErrors',
\ }
let g:lightline.component_type = {
\   'linter_checking': 'raw',
\   'linter_ok': 'raw',
\   'linter_info': 'left',
\   'linter_warnings': 'warning',
\   'linter_errors': 'error',
\ }
let g:lightline.active = {
\   'left': [
\     [ 'mode' ],
\     [ 'filename' ],
\     [ 'gitbranch', 'gitdiff' ],
\   ],
\   'right': [
\     [ 'lineinfo' ],
\     [ 'fileencoding', 'filetype' ],
\     [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_info', 'linter_ok' ],
\     [ 'cocstatus' ],
\   ],
\ }
let g:lightline.inactive = {
\   'left': [
\     [ 'filename' ],
\     [ 'gitdiff' ],
\   ],
\   'right': [
\     [ 'lineinfo' ],
\     [ 'fileencoding', 'filetype' ],
\   ],
\ }
let g:lightline.tab_component_function = {
\   'modro': 'LightlineTabModifiedReadonly',
\   'wincount': 'LightlineTabWindowCount',
\ }
let g:lightline.tab = {
\   'active': [ 'tabnum', 'filename', 'wincount', 'modro' ],
\   'inactive': [ 'tabnum', 'filename', 'wincount', 'modro' ],
\ }
let g:lightline.tabline = {
\   'left': [
\     [ 'tabs' ],
\   ],
\   'right': [],
\ }
let g:lightline.separator = {
\   'left': "\ue0b8",
\   'right': "\ue0be",
\ }
let g:lightline.subseparator = {
\   'left': "\ue0b9",
\   'right': "\ue0b9",
\ }
let g:lightline.tabline_separator = {
\   'left': "\ue0bc",
\   'right': "\ue0ba",
\ }
let g:lightline.tabline_subseparator = {
\   'left': "\ue0bb",
\   'right': "\ue0bb",
\ }
let g:lightline_file_modified_icon = '●'
let g:lightline_file_readonly_icon = ''
let g:lightline_git_branch_icon = ''

" Render a circle icon if the file is modified
function! LightlineFileModified()
  return &modified ? ' ' . g:lightline_file_modified_icon : ''
endfunction

" Render a padlock icon if the file is readonly
function! LightlineFileReadonly()
  return &readonly ? ' ' . g:lightline_file_readonly_icon : ''
endfunction

" Render filename with modified/readonly status
function! LightlineFilename()
  return expand('%:t') !=# '' ? expand('%:t') . LightlineFileReadonly() . LightlineFileModified() : '[No Name]'
endfunction

" Render filetype in plaintext
function! LightlineFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype : 'no ft') : ''
endfunction

" Render file encoding
function! LightlineFileencoding()
  return winwidth(0) > 70 ? &fileencoding : ''
endfunction

" Render git branch prefixed by a branch icon
function! LightlineGitBranch()
  if exists('*FugitiveHead')
    let branch = FugitiveHead(8)

    return branch !=# '' ? g:lightline_git_branch_icon . ' ' . branch : ''
  endif

  return ''
endfunction

function! StatusLineGitFlag()
  if !exists('b:statusline_git_flag')
    if !file_readable(expand('%'))
      let b:statusline_git_flag = ''
    else
      let b:statusline_git_flag = functions#GitExecInPath('git status --porcelain ' . expand('%') . " 2>/dev/null | awk '{print $1}'")[:-2]
    endif
  endif
  return b:statusline_git_flag
endfunction

" Render numerical git diff
function! LightlineGitDiff()
  let disabledFiletypes = '\v(help|qf|text|diff|gitcommit|vim-plug|startify|fugitive|twiggy|undotree)'

  if exists('*FugitiveHead') && &filetype !~# disabledFiletypes
    let branch = FugitiveHead()
    let [a,m,r] = GitGutterGetHunkSummary()

    return branch !=# '' && winwidth(0) > 70 ? printf('+%d ~%d -%d', a, m, r) : ''
  endif

  return ''
endfunction

function LightlineTabWindowCount(n)
  let winnrs = tabpagewinnr(a:n, '$')
  return '(' . winnrs . ')'
endfunction

function LightlineTabModifiedReadonly(n)
  let bufnrlist = tabpagebuflist(a:n)
  let label = ''

  for bufnr in bufnrlist
    if getbufvar(bufnr, "&modified")
      let label .= g:lightline_file_modified_icon
      break
    elseif getbufvar(bufnr, "&readonly")
      let label .= g:lightline_file_readonly_icon
      break
    endif
  endfor

  return label
endfunction

" ALE Status
let s:indicators_enabled = 0
let s:indicator_checking = " \uf110 "
let s:indicator_ok = " \uf00c "
let s:indicator_info = get(g:, 'ale_sign_info', "\uf05a") . ' '
let s:indicator_warning = get(g:, 'ale_sign_warning', "\uf529") . ' '
let s:indicator_error = get(g:, 'ale_sign_error', "\uf00d") . ' '

function! LightlineALEChecking() abort
  return ale#engine#IsCheckingBuffer(bufnr('')) ? s:indicator_checking : ''
endfunction

function! LightlineALEOk() abort
  if !LightlineALELintedHelper()
    return ''
  endif
  let l:counts = ale#statusline#Count(bufnr(''))
  return l:counts.total == 0 ? s:indicator_ok : ''
endfunction

function! LightlineALEInfo() abort
  if !LightlineALELintedHelper()
    return ''
  endif
  let l:indicator = s:indicators_enabled == 1 ? s:indicator_info : ''
  let l:counts = ale#statusline#Count(bufnr(''))
  return l:counts.info == 0 ? '' : printf(l:indicator . '%d', l:counts.info)
endfunction

function! LightlineALEWarnings() abort
  if !LightlineALELintedHelper()
    return ''
  endif
  let l:indicator = s:indicators_enabled == 1 ? s:indicator_warning : ''
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_warnings = l:counts.warning + l:counts.style_warning
  return l:all_warnings == 0 ? '' : printf(l:indicator . '%d', all_warnings)
endfunction

function! LightlineALEErrors() abort
  if !LightlineALELintedHelper()
    return ''
  endif
  let l:indicator = s:indicators_enabled == 1 ? s:indicator_error : ''
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  return l:all_errors == 0 ? '' : printf(l:indicator . '%d', all_errors)
endfunction

function! LightlineALELintedHelper() abort
  return get(g:, 'ale_enabled', 0) == 1
    \ && getbufvar(bufnr(''), 'ale_enabled', 1)
    \ && getbufvar(bufnr(''), 'ale_linted', 0) > 0
    \ && ale#engine#IsCheckingBuffer(bufnr('')) == 0
endfunction
" }}}

" vim-which-key {{{
" Minimum horizontal space between columns
" (effectively force everything into a single column)
let g:which_key_hspace = 9999

" Use a floating window
let g:which_key_use_floating_win = 1

" Disable floating window offset
let g:which_key_disable_default_offset = 1

" Disable centered layout
let g:which_key_centered = 0

" Exit vim-which-key with space
let g:which_key_exit = [ "\<C-[>", "\<Esc>", "\<Space>" ]

" Define prefix dictionary
let g:which_key_map = {}
let g:which_key_map['a'] = 'Code [a]ctions'
let g:which_key_map['b'] = {
\   'name': '+buffers',
\   'b': 'Go to previous [b]uffer',
\   'd': '[d]elete current buffer',
\ }
let g:which_key_map['c'] = 'Toggle [c]omment'
let g:which_key_map['d'] = {
\   'name': '+diagnostics',
\   'j': 'Next diagnostic message',
\   'k': 'Previous diagnostic message',
\   'd': 'Diagnostic [d]etails',
\ }
let g:which_key_map['e'] = {
\   'name': '+explorer',
\   'n': 'Use [n]nn',
\   'r': 'Use [r]anger',
\ }
let g:which_key_map['f'] = {
\   'name': '+find',
\   'c': 'Find [c]ommands',
\   'f': 'Find [f]iles in CWD',
\   'p': 'Find in [p]roject',
\   'l': 'Find [l]ines in current buffer',
\   'b': 'Find open [b]buffers',
\ }
let g:which_key_map['g'] = {
\   'name': '+git',
\   'B': '[B]lame',
\   'b': '[b]ranches',
\   'c': 'Commits for [c]urrent file',
\   'g': 'Fu[g]itive status window',
\   'l': 'Pu[l]l',
\   'L': '[L]og',
\   'o': '[o]pen (lines) in repo',
\   'p': '[p]ush',
\   'P': '[P]ush --force-with-lease',
\   's': 'Git [s]tatus',
\   'w': '[w]rite and stage file',
\ }
let g:which_key_map['h'] = {
\   'name': '+hunk',
\   'j': 'Next hunk',
\   'k': 'Previous hunk',
\   'u': '[u]ndo hunk',
\   'p': '[p]review hunk',
\   's': '[s]tage hunk',
\   'z': 'Fold all unchanged lines',
\ }
let g:which_key_map['i'] = {
\   'name': '+insert',
\   'i': 'Insert before current list item',
\   'a': 'Insert after current list item',
\   'I': 'Insert as first list item',
\   'A': 'Insert as last list item',
\ }
let g:which_key_map['l'] = 'Remove search high[l]ighting'
let g:which_key_map['m'] = {
\   'name': '+move',
\   'j': 'Join a block into a single-line statement',
\   'k': 'Split a one-liner into multiple lines',
\   'h': 'Move item left',
\   'l': 'Move item right',
\ }
let g:which_key_map['n'] = {
\   'name': '+navigate',
\   'h': 'Move cursor left by one list item',
\   'l': 'Move cursor right by one list item',
\ }
let g:which_key_map['p'] = {
\   'name': '+harpoon',
\   'a': 'Jump to mark 1',
\   's': 'Jump to mark 2',
\   'd': 'Jump to mark 3',
\   'f': 'Jump to mark 4',
\   'q': 'Jump to mark 5',
\   'w': 'Jump to mark 6',
\   'e': 'Jump to mark 7',
\   'r': 'Jump to mark 8',
\   'j': 'Jump to next mark',
\   'k': 'Jump to previous mark',
\   'm': 'View [m]arks',
\   'p': 'Add file to har[p]oon',
\   't': 'Search marks with [t]elescope',
\ }
let g:which_key_map['r'] = {
\   'name': '+search',
\   'r': '[r]eplace last searched with last yanked',
\ }
let g:which_key_map['s'] = '[s]ymbols'
let g:which_key_map['t'] = {
\   'name': '+test',
\   'p': 'Test [p]roject',
\   'f': 'Test current [f]file',
\   's': 'Test nearest [s]ingle test',
\ }
let g:which_key_map['u'] = 'Toggle [u]ndotree'
let g:which_key_map['v'] = {
\   'name': '+vimrc',
\   'f': 'Search dot[f]iles',
\   'h': '[h]elp tags',
\   's': '[s]ource .vimrc',
\ }
let g:which_key_map['w'] = {
\   'name': '+windows',
\   'q': [':wqa', 'Write all buffers and [q]uit'],
\   'w': ['<C-W>w', 'other-window'],
\   'd': ['<C-W>c', 'delete-window'],
\   '-': ['<C-W>s', 'split-window-below'],
\   '|': ['<C-W>v', 'split-window-right'],
\   '2': ['<C-W>v', 'layout-double-columns'],
\   'h': ['<C-W>h', 'window-left'],
\   'j': ['<C-W>j', 'window-below'],
\   'l': ['<C-W>l', 'window-right'],
\   'k': ['<C-W>k', 'window-up'],
\   'H': ['<C-W>5<', 'expand-window-left'],
\   'J': [':resize +5', 'expand-window-below'],
\   'L': ['<C-W>5>', 'expand-window-right'],
\   'K': [':resize -5', 'expand-window-up'],
\   '=': ['<C-W>=', 'balance-window'],
\   's': ['<C-W>s', 'split-window-up'],
\   't': [':tabclose', 'Close tab'],
\   'v': ['<C-W>v', 'split-window-left'],
\   '?': ['Windows', 'fzf-window'],
\ }
let g:which_key_map['x'] = {
\   'name': '+tmuxline',
\   's': '[s]et Tmux status bar theme',
\   'g': '[g]enerate a theme from the config',
\ }
let g:which_key_map['y'] = {
\   'name': '+yank',
\   'h': 'Yank [h]istory',
\   'c': '[c]lear yank history',
\ }
let g:which_key_map['z'] = {
\   'name': '+fold',
\   'f': '[f]old to matching pair',
\ }
" }}}
