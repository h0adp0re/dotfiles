let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_sign_column = 'bg0'
let g:gruvbox_invert_selection = 0

colorscheme gruvbox

" Gruvbox overrides
highlight! NonText ctermfg=241 ctermbg=NONE guifg=#665c54 guibg=NONE
