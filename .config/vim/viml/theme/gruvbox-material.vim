" Gruvbox-material settings
let g:gruvbox_material_background = 'medium'
let g:gruvbox_material_enable_bold = 1
let g:gruvbox_material_enable_italic = 1
let g:gruvbox_material_visual = 'grey background'
let g:gruvbox_material_menu_selection_background = 'green'
let g:gruvbox_material_sign_column_background = 'none'
let g:gruvbox_material_diagnostic_line_highlight = 1
let g:gruvbox_material_diagnostic_virtual_text = 'colored'
let g:gruvbox_material_current_word = 'grey background'
let g:gruvbox_material_statusline_style = 'default'
let g:gruvbox_material_better_performance = 1
let g:gruvbox_material_palette = 'material'

" Set the colorscheme
colorscheme gruvbox-material

highlight! TabLineSel term=bold cterm=bold ctermfg=235 ctermbg=246 gui=bold guifg=#282828 guibg=#a89984
highlight! TabLineSelSeparator ctermfg=246 ctermbg=240 guifg=#a89984 guibg=#504945
highlight! TabLineSelLastSeparator ctermfg=246 ctermbg=236 guifg=#a89984 guibg=#32302f
highlight! TabLineSeparator ctermfg=240 ctermbg=246 guifg=#504945 guibg=#a89984
highlight! TabLineLastSeparator ctermfg=240 ctermbg=236 guifg=#504945 guibg=#32302f
highlight! TabLineSeparatorMid ctermfg=246 ctermbg=240 guifg=#a89984 guibg=#504945

" Gruvbox Material overrides
highlight! link TSTag Red
highlight! link TSString Green
highlight! link TSProperty Aqua
highlight! link TSConstant Purple
highlight! link TSField TSProperty
highlight! link TSVariable Yellow
highlight! link TSStringEscape Yellow
highlight! link TSStringRegex Aqua
highlight! link TSFunction Function
highlight! link TSDefinition TermCursor
highlight! link TSParameter Blue
highlight! link TSParameterDefinition Blue
