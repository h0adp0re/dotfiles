augroup CustomEvents
  " Note, 'autocmd!' is used to clear out any existing definitions in
  " this auto-group. This prevents duplicate entries upon a live ~/vimrc
  " reload.
  autocmd!

  " Automatically toggle between relative and absolute line numbers
  autocmd BufEnter,FocusGained,InsertLeave,WinEnter * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * set norelativenumber

  " Close fugitive buffers when exiting them
  autocmd BufReadPost fugitive://* set bufhidden=delete

  " Map dd to delete current entry in the quickfix list
  autocmd BufRead quickfix nnoremap <buffer> dd :.Reject<CR>
  " Map d to delete visual selection in the quickfix list
  autocmd BufRead quickfix vnoremap <buffer> d :'<,'>Reject<CR>
  autocmd BufRead quickfix nnoremap <buffer> u :Restore<CR>

  " Set brewfile filetype on Brewfiles
  autocmd VimEnter,WinEnter,BufRead Brewfile_work set filetype=ruby
  autocmd VimEnter,WinEnter,BufRead Brewfile set filetype=ruby

  " Set yaml filetype on .thymerc
  autocmd VimEnter,WinEnter,BufRead .thymerc set filetype=yaml

  " Highlight some common keywords in comments
  let todoMatchList = 'NOTE\|INFO\|IDEA\|TODO\|HACK\|WARN\|PERF\|FIX\|BUG'
  autocmd WinEnter,VimEnter * :silent! call matchadd('Todo', todoMatchList, -1)

  " Register vim-which-key shortcut
  autocmd! User vim-which-key call which_key#register('<Space>', 'g:which_key_map')

  " Update lightline on coc events
  autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()

  " Update lightline on ALE events
  " autocmd User ALEJobStarted call lightline#update()
  " autocmd User ALELintPost call lightline#update()
  " autocmd User ALEFixPost call lightline#update()

  " Fix markdown with markdownlint on save
  autocmd BufWritePre *.md silent! :call CocAction('runCommand', 'markdownlint.fixAll')
augroup END
