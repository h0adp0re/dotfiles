" Get the defaults for vim that most users want.
source $VIMRUNTIME/defaults.vim

set backupdir=$HOME/.config/vim/backup//
set undodir=$HOME/.config/vim/undo//

set autoread " detect when a file is changed (also set by terminus and nvim)
set noswapfile " don't create swapfiles
set belloff=all " disable visual bell (default in nvim)
set backup " keep a (tilde) backup file (restore to previous version)
set undofile " keep an undo file (undo changes after closing)
set history=1000 " change history to 1000
set updatetime=100 " set vim's updatetime to 100ms
set clipboard+=unnamed " enable yanking to system clipboard
set backspace=2 " indent,eol,start (default in nvim)
set scrolloff=8 " set cursor scroll offset
set ttimeoutlen=50 " set key code sequence timeout to 50ms (default in nvim)
set foldtext=gitgutter#fold#foldtext() " show whether the folded lines have been changed
set switchbuf+=uselast " load the quickfix and location list results in the last active window
set linebreak " set soft wrapping
set autoindent " automatically set indent of new line (default in nvim)
set hidden " hide buffers instead of closing them
set showcmd " show incomplete commands
set nolazyredraw " do not update screen during automated tasks
set incsearch " set incremental search, like modern browsers
set ignorecase " case insensitive searching
set smartcase " case-sensitive if expresson contains a capital letter
set hlsearch " highlight search results (default in nvim)
set ttyfast " faster redrawing (default in nvim)
set wildmenu " enhanced command-line completion (default in nvim)
set completeopt=menu,menuone,noselect
set sessionoptions+=tabpages,globals " store tabpages and globals in session

" TODO: Figure out formatoptions

if has('mouse')
  set mouse+=a " mouse selections don't use xterm mode
endif

set number relativenumber " show line numbers
set cursorline " highlight current line
set showmatch " show matching brackets
set wrap " turn on line wrapping
set wrapmargin=8 " wrap lines when coming within n characters from side
set colorcolumn=80 " visual indicator at 80 col mark
set signcolumn=yes " always show the signcolumn
set title " set terminal title
set laststatus=2 " always the statusline (default in nvim)
set noshowmode " disable vim's statusline
set showtabline=1 " show tabline when there are at least 2 tabs

if &diff
  set diffopt-=internal
  set diffopt+=vertical
endif

set list " show invisible characters
set listchars=tab:→\ ,trail:⋅,nbsp:+,eol:¬,extends:❯,precedes:❮
set showbreak=↪ " character at the start of lines that have been wrapped

syntax on " enable syntax highlighting
