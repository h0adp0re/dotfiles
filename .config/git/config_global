[core]
	excludesfile = ~/.config/git/gitignore_global
	ignorecase = false
	pager = diff-so-fancy | less
[rebase]
	autosquash = true
[interactive]
	diffFilter = diff-so-fancy --patch
[color]
	ui = true
[color "diff-highlight"]
	oldNormal = 1
	oldHighlight = 1 bold 52
	newNormal = 2
	newHighlight = 2 bold 22
[color "diff"]
	meta = 7
	frag = 5 bold
	func = 15 bold
	commit = 3 bold
	old = 1
	new = 2
	whitespace = 1 reverse
[color "decorate"]
	branch = 2 bold
	remoteBranch = 1 bold
[color "grep"]
	filename = 4
	function = 5 bold
	linenumber = 3 bold
	column = 2 bold
	match = 15 bold reverse
	separator = 7
[color "interactive"]
	prompt = 2 bold
	header = 15 bold
	help = 3
	error = 1 bold
[color "remote"]
	hint = 4 bold
	warning = 3 bold
	success = 2 bold
	error = 1 bold
[color "status"]
	header = 15
	added = 0 bold
	updated = 2 bold
	changed = 3 bold
	untracked = 4 bold
	branch = 2 0
	nobranch = 1
	localBranch = 2 bold
	remoteBranch = 1 bold
	unmerged = 1 bold
[tig "color"]
	cursor = 2 0 bold
	title-blur = 7 0 standout
	title-focus = 3 0 bold
[init]
	templatedir = ~/.config/git/templates
[branch]
	sort = -committerdate
[pull]
	rebase = false
[merge]
	tool = nvim_fugitive
	conflictstyle = diff3
[mergetool "nvim_fugitive"]
	cmd = nvim -f -c "Gvdiffsplit!" "$MERGED" "$BASE" "$LOCAL" "$REMOTE"
	# prevent git asking whether it worked
	trustExitCode = true
[diff]
	tool = nvim_diff
[difftool "nvim_diff"]
	cmd = nvim -d "$LOCAL" "$REMOTE"
[alias]
	patch = !git --no-pager diff --no-color
	undo = !git reset HEAD^
	who = !git shortlog --summary --numbered --no-merges
	rename = !git branch --move
