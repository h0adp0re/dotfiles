#!/usr/bin/env bash

# Open a document in Preview in full-screen
#
# Usage: openPreviewInFullscreen <file_path>
#
openPreviewInFullscreen() {
  open -a Preview "$1"
  osascript -e 'tell application "Preview"
    "activate"
    tell application "System Events"
      keystroke "f" using {control down, command down}
    end tell
  end tell'
}

# Launch a drinking game.
#
# Usage: drink <option>
#
#   options:
#       office           The Office
#       parks            Parks And Recreation
#       sunny            It's Always Sunny in Philadelphia
#
drink() {
  local Normal Green Yellow On_Black gamesDirName gamesPath help_message error_message file
  local -a games

  Normal="$(tput sgr0)"
  Green="$(tput setaf 2)"
  Yellow="$(tput setaf 3)"
  On_Black="$(tput setab 0)"
  gamesDirName="drinking-games"
  gamesPath="$HOME/Library/Mobile Documents/com~apple~CloudDocs/$gamesDirName"
  games=("office" "parks" "sunny")
  help_message="\
Which game do you want?

  Usage: ${Yellow}drink <game>${Normal}

    games:
        ${Yellow}office${Normal}  The Office
        ${Yellow}parks${Normal}   Parks And Recreation
        ${Yellow}sunny${Normal}   It's Always Sunny in Philadelphia"
  error_message="This script assumes that you have ${Green}${On_Black}$gamesDirName/${Normal} in your iCloud Drive."

  if [[ ! -d $gamesPath ]]; then
    print-error "$error_message"
    return 1
  fi

  if [[ $# -eq 0 ]]; then
    echo "$help_message"
    return 1
  else
    for game in "${games[@]}"; do
      if [[ $1 == "$game" ]]; then
        file="$gamesPath/drinkinggame-$game.pdf"
      fi
    done

    if [[ -f $file ]]; then
      openPreviewInFullscreen "$file"
    else
      print-error "File doesn't exist" \
        "$file"
    fi
  fi
}
