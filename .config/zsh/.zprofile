export LANG="en_US.UTF-8"

if command -v less &> /dev/null; then
  export PAGER="less"
fi

if command -v lynx &> /dev/null; then
  export BROWSER="lynx"
fi

if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR="vim"
else
  export EDITOR="nvim"
  export VISUAL="nvim"
fi

# Define XDG base directories
export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
export XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"
export XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
export XDG_STATE_HOME="${XDG_STATE_HOME:-$HOME/.local/state}"
export XDG_RUNTIME_DIR="${XDG_RUNTIME_DIR:-$TMPDIR}"

# Notes directory
export NOTES="$HOME/notes"

# Colors for files and directories in GNU ls aka gls (see `gdircolors -p`)
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Background color codes:
# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white
# Extended color codes for terminals that support more than 16 colors:
# (the above color codes still work for these terminals)
# Text color coding:
# 38;5;COLOR_NUMBER
# Background color coding:
# 48;5;COLOR_NUMBER
# COLOR_NUMBER is from 0 to 255.
# File type codes:
# rs = reset to no color
# no = normal non-filename text
# fi = file
# ex = executable file (+x)
# ca = file with capability
# su = file that is setuid - set-user-ID (u+s)
# sg = file that is setgid - set-group-ID (u+g)
# di = directory
# st = sticky directory (+t)
# ow = other writable directory (o+w)
# tw = sticky other-writable directory (+t,o+w)
# ln = symlink
# mh = multi-hardlink
# or = orphaned symlink
# mi = missing file that an orphaned symlink points to
# pi = named pipe (FIFO)
# so = socket
# do = door
# bd = block device driver
# cd = character device driver
export LS_COLORS="di=1;34:ow=1;30;44:ln=1;36:or=1;35:mi=7;31:tw=7;34:*.DS_Store=7;37"

# Zsh correction prompt style
export SPROMPT="Correct %F{001}%B%R%b%f to %F{002}%B%r%b%f? (%F{002}%By%b%f/%F{003}%Bn%b%f/%F{001}%Ba%b%f/%F{004}%Be%b%f) "

# less
# -R: don't leave anything printed after quitting less
# https://github.com/lmintmate/zshrc#setting-for-less
export LESS="-R"
export LESSHISTFILE="$XDG_STATE_HOME/lesshst"
# Colored man pages
# https://github.com/lmintmate/zshrc#setting-for-colored-man-pages
# Less uses:
# termcap terminfo
# ks      smkx      make the keypad send commands
# ke      rmkx      make the keypad send digits
# vb      flash     emit visual bell
# mb      blink     start blink
# md      bold      start bold
# me      sgr0      turn off bold, blink and underline
# so      smso      start standout (reverse video)
# se      rmso      stop standout
# us      smul      start underline
# ue      rmul      stop underline
#         setaf     start foreground color
#         setab     start background color
export LESS_TERMCAP_md="$(tput bold; tput setaf 1)"
export LESS_TERMCAP_me="$(tput sgr0)"
export LESS_TERMCAP_mb="$(tput bold; tput setaf 2)"
export LESS_TERMCAP_us="$(tput bold; tput setab 0; tput setaf 2)"
export LESS_TERMCAP_ue="$(tput sgr0)"
export LESS_TERMCAP_so="$(tput bold; tput setab 3; tput setaf 0)"
export LESS_TERMCAP_se="$(tput sgr0)"

# zsh-autosuggestions
# https://github.com/zsh-users/zsh-autosuggestions#disabling-automatic-widget-re-binding
export ZSH_AUTOSUGGEST_MANUAL_REBIND=1

# zsh-you-should-use
export YSU_MESSAGE_FORMAT="💡 $(tput setaf 4)%alias$(tput sgr0) = $(tput setaf 3)%command$(tput sgr0)"
export YSU_MODE=ALL # show every alias that matches
export YSU_MESSAGE_POSITION="after" # show YSU after execution

# Disable Homebrew analytics
export HOMEBREW_NO_ANALYTICS=1
export HOMEBREW_INSTALL_FROM_API=1

# nvm
export NVM_DIR="$XDG_DATA_HOME/nvm"

# npm
export NPM_CONFIG_CACHE="$XDG_CACHE_HOME/npm"
export NPM_CONFIG_FUND=false
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/config"

# fzf
export FZF_COMPLETION_TRIGGER=",,"
export FZF_DEFAULT_OPTS="
  --cycle
  --scroll-off=8
  --prompt='❯ '
  --pointer='▶'
  --marker='│'
  --border='rounded'
  --border-label-pos='bottom'
  --color=hl:003,hl+:011,bg:-1,bg+:-1,fg+:015
  --color=prompt:003,spinner:005,pointer:006,marker:002
  --color=border:240,separator:240
  --color=info:245,label:245
"

# bashunit
export BASHUNIT_DEV_LOG="$XDG_CACHE_HOME/bashunit/dev.log"

# hledger
export LEDGER_FILE="$XDG_DATA_HOME/hledger.journal"

# tldr
export TLDR_CACHE_DIR="$XDG_CACHE_HOME/tldr"

# rustup
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# cargo
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# Go
export GOPATH="$XDG_DATA_HOME/go"

# gradle
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"

# gum
export GUM_FILTER_PROMPT="❯ "
export GUM_FILTER_INDICATOR="▶"
export GUM_FILTER_INDICATOR_FOREGROUND="6"
export GUM_FILTER_MATCH_FOREGROUND="11"
export GUM_FILTER_PROMPT_FOREGROUND="11"

# nnn
export NNN_OPENER="nvim"
export NNN_OPTS="cdEHS"
# Session colors
# gruvbox-material
# export NNN_COLORS="3333"
# catppuccin
export NNN_COLORS="#04020301;4231"
# Custom file colors for nnn
# block char dir exe reg hardlink symlink missing orphan fifo socket other
# gruvbox-material
# export NNN_FCOLORS="c1e20402006006f705d60609"
# catppuccin
export NNN_FCOLORS="030304020705050801060301"
# https://github.com/jarun/nnn/tree/master/plugins
export NNN_PLUG="d:dups;f:fzcd;x:togglex;c:fixname;b:cdpath"

# ddgr
export DDGR_COLORS="gCehxY"

# lynx
export LYNX_CFG="$XDG_CONFIG_HOME/lynx/lynx.cfg"
export LYNX_LSS="$XDG_CONFIG_HOME/lynx/lynx.lss"
export RL_CLCOPY_CMD="pbcopy"

# Glamour (Markdown rendering on the command line)
# https://github.com/charmbracelet/glamour/
export GLAMOUR_STYLE="$XDG_CONFIG_HOME/glamour/gruvbox-material.json"

# Taskwarrior
export TASKDATA="$XDG_CONFIG_HOME/task/data"

# Timewarrior
export TIMEWARRIORDB="$XDG_CONFIG_HOME/timewarrior"

if [[ "$(uname -m)" == "arm64" ]]; then
  # Add homebrew to $PATH
  eval "$(/opt/homebrew/bin/brew shellenv)"
else
  # Add homebrew to $PATH
  eval "$(/usr/local/Homebrew/bin/brew shellenv)"
fi

# Source work environment variables.
if [[ -r "$HOME/.dotfiles/setup-work/.config/zsh/exports.sh" ]] &&
  [[ -f "$HOME/.dotfiles/setup-work/.config/zsh/exports.sh" ]]; then
  source "$HOME/.dotfiles/setup-work/.config/zsh/exports.sh"
fi

# To use Homebrew's `curl`:
# PATH="$(brew --prefix)/opt/curl/bin:$PATH"

# All of the following commands have been installed with the prefix 'g'.
# If you need to use these commands with their normal names,
# uncomment the relevant lines:

# grep
# https://www.gnu.org/software/grep/
# PATH="$(brew --prefix)/opt/grep/libexec/gnubin:$PATH"

# find, locate, updatedb, xargs
# https://www.gnu.org/software/findutils/
# PATH="$(brew --prefix)/opt/findutils/libexec/gnubin:$PATH"

# tar
# https://www.gnu.org/software/tar/
# PATH="$(brew --prefix)/opt/gnu-tar/libexec/gnubin:$PATH"

# which
# https://savannah.gnu.org/projects/which/
# PATH="$(brew --prefix)/opt/gnu-which/libexec/gnubin:$PATH"

# make
# https://www.gnu.org/software/make/
# PATH="$(brew --prefix)/opt/make/libexec/gnubin:$PATH"

# Add go programs to $PATH
PATH="$HOME/go/bin:$PATH"

# Add custom scripts to $PATH
# Note that exporting $PATH is not necessary because it
# will universally have the export flag set already
PATH="$HOME/.dotfiles/scripts:$PATH"
PATH="$HOME/.dotfiles/setup-work/scripts:$PATH"
PATH="$HOME/.dotfiles/setup-work/scripts/jira:$PATH"
PATH="$HOME/.local/bin:$PATH"
