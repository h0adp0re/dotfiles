#!/usr/bin/env bash

function alias_help() {
  generate_help --aliases "$1" "$2" "$3"
}

# General {{{
if [[ $OSTYPE == "darwin"* ]]; then
  # Use the more universal alias `clip`
  alias clip="pbcopy"

  # Dismiss macOS system notification
  alias dismiss="macos-dismiss-notification"

  # View file in macOS QuickLook
  alias ql="macos-ql"

  # Make display go to sleep immediately
  alias afk="pmset displaysleepnow"

  # macOS aliases help
  function mah() {
    local aliases descriptions
    aliases+=(
      "clip"
      "dismiss"
      "ql"
      "afk"
      "mah"
    )
    descriptions+=(
      "Alias for pbcopy;"
      "Dismiss macOS system notification;"
      "View file in macOS QuickLook;"
      "Make display go to sleep immediately;"
      "Print this help;"
    )

    alias_help "macOS aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
else
  # Normalize `open` across Linux, macOS, and Windows.
  if grep -q Microsoft /proc/version; then
    # Ubuntu on Windows using the Linux subsystem
    alias open='explorer.exe'
  else
    alias open='xdg-open'
  fi
fi

# Ignore $ and % at the beginning of commands
# https://github.com/lmintmate/zshrc#aliases
alias \$=' '
# shellcheck disable=SC1001
alias \%=' '

# Edit zsh history file
alias hist="v \$XDG_DATA_HOME/zsh/.zhistory"

# Print zsh directory stack
alias d='dirs -v'

# Navigate zsh directory stack
alias -- -="cd -"
alias 1="cd -"
alias 2="cd -2"
alias 3="cd -3"
alias 4="cd -4"
alias 5="cd -5"
alias 6="cd -6"
alias 7="cd -7"
alias 8="cd -8"
alias 9="cd -9"

alias ~="cd ~"

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

# Zsh aliases help
function zshh() {
  local aliases descriptions
  aliases+=(
    "hist"
    "d"
    "-"
    "1"
    "2"
    "3"
    "4"
    "5"
    "6"
    "7"
    "8"
    "9"
    "~"
    ".."
    "..."
    "...."
    "....."
    "......"
    "zshh"
  )
  descriptions+=(
    "Edit zsh history file;"
    "Print zsh directory stack;"
    "Go to previous directory in the stack;"
    "Go to directory number 1 in the stack;"
    "Go to directory number 2 in the stack;"
    "Go to directory number 3 in the stack;"
    "Go to directory number 4 in the stack;"
    "Go to directory number 5 in the stack;"
    "Go to directory number 6 in the stack;"
    "Go to directory number 7 in the stack;"
    "Go to directory number 8 in the stack;"
    "Go to directory number 9 in the stack;"
    "Go home;"
    "Go up one directory;"
    "Go up two directories;"
    "Go up three directories;"
    "Go up four directories;"
    "Go up five directories;"
    "Print this help;"
  )

  alias_help "Zsh aliases" \
    "${aliases[*]}" "${descriptions[*]}"
}

# Reload shell
alias reload="exec \$SHELL"

# Search aliases
alias aliases="alias | grep --color=always"

# Recursively delete `.DS_Store` files
alias clean_ds="find . -type f -name '*.DS_Store' -ls -delete"

# `brew install bat`
if command -v bat &> /dev/null; then
  # Alias for bat
  alias cat="bat"
fi

# `brew install nvim`
if command -v nvim &> /dev/null; then
  # Alias for nvim
  alias v="nvim"
else
  alias v="vim"
fi

# `brew install nnn`
if command -v nnn &> /dev/null; then
  # Alias for nnn
  alias n="nnn ."
fi

# `brew install ranger`
if command -v ranger &> /dev/null; then
  # Alias for ranger
  alias rr="ranger"
fi

# `brew install weechat`
if command -v weechat &> /dev/null; then
  # Alias for weechat
  alias wee="weechat"
fi

# `brew install python3`
if command -v python3 &> /dev/null; then
  # Serve a folder to port 8000
  alias http="python3 -m http.server"
fi

# Utility aliases help
function uth() {
  local aliases descriptions
  aliases+=(
    "reload"
    "aliases"
    "clean_ds"
    "v"
    "n"
    "rr"
    "wee"
    "http"
    "uth"
  )
  descriptions+=(
    "Reload shell;"
    "Search aliases;"
    "Recursively delete .DS_Store files;"
    "Open nvim if installed, otherwise vim;"
    "Open nnn;"
    "Open ranger;"
    "Open WeeChat;"
    "Serve a folder to port 8000 with Python;"
    "Print this help;"
  )

  alias_help "Utility aliases" \
    "${aliases[*]}" "${descriptions[*]}"
}

# `brew install coreutils`
if command -v gls &> /dev/null; then
  # Alias for gls
  alias ls="gls"

  # Preferred ls implementation
  alias la="ls -FGlAhp --color --time-style=long-iso --group-directories-first"

  # List only directories
  alias lad="ls -GlAh --color --time-style=long-iso | grep --color=never '^d'"

  # List all by date modified
  alias lm="ls -FGlAhpt --color --time-style=long-iso"
else
  # Fallback non-GNU ls aliases
  alias la="ls -GlAhoD '%Y-%m-%d %H:%M'"
  alias lad="la | grep --color=never '^d'"
  alias lm="la -t"
fi

# ls aliases help
function lsh() {
  local aliases descriptions
  aliases+=(
    "la"
    "lad"
    "lm"
    "lsh"
  )
  descriptions+=(
    "Preferred ls implementation;"
    "List only directories;"
    "List all by date modified;"
    "Print this help;"
  )

  alias_help "ls aliases" \
    "${aliases[*]}" "${descriptions[*]}"
}
# }}}

# System {{{
# List processes by CPU usage
alias cpu="top -o cpu -stats pid,command,cpu,mem,time"

# List processes by RAM usage
alias mem="top -o mem -stats pid,command,cpu,mem,time"

# Find out why the system shut down in the last 24h
# Check here to see what each code means:
# https://georgegarside.com/blog/macos/shutdown-causes/
alias whyshutdown="print-message --check 'Checking' 'shutdown causes in 24h'\
  'Code meanings:\n  https://georgegarside.com/blog/macos/shutdown-causes/\n' &&\
  sudo -v &&\
  echo &&\
  sudo log show --predicate 'eventMessage contains \"Previous shutdown cause\"' --last 24h"

# Monitor processes
# `brew install glances`
if command -v glances &> /dev/null; then
  alias mon1="glances"
fi

# Monitor processes
# `brew install bpytop`
if command -v bpytop &> /dev/null; then
  alias mon2="bpytop"
fi

# List open ports with lsof
alias ports="lsof -i -P | grep -i 'listen'"

# List open ports with netstat
alias ports2="netstat -an -ptcp | grep LISTEN"

# Write macOS settings
alias dbefore="defaults read > \$HOME/.defaults_before"
alias dafter="defaults read > \$HOME/.defaults_after"

# Diff macOS settings changes
alias ddiff="nvim -d \$HOME/.defaults_before \$HOME/.defaults_after"

# Get public-facing IPv4 address
alias ipv4="curl -fsSL4m 5 https://api.ipify.org"

# Get public-facing IPv6 address
alias ipv6="curl -fsSL6m 5 https://api64.ipify.org"

# Get local IPv4 address
alias iplocal="ifconfig -l | xargs -n1 ipconfig getifaddr"

# System aliases help
function syh() {
  local aliases descriptions
  aliases+=(
    "cpu"
    "mem"
    "whyshutdown"
    "mon1"
    "mon2"
    "ports"
    "ports2"
    "dbefore"
    "dafter"
    "ddiff"
    "ipv4"
    "ipv6"
    "iplocal"
    "syh"
  )
  descriptions+=(
    "List processes by CPU usage;"
    "List processes by RAM usage;"
    "Find out why the system shut down in the last 24h;"
    "Monitor processes with glances;"
    "Monitor processes with bpytop;"
    "List open ports with lsof;"
    "List open ports with netstat;"
    "Write macOS settings to HOME/.defaults_before;"
    "Write macOS settings to HOME/.defaults_after;"
    "Diff macOS settings changes;"
    "Get public-facing IPv4 address;"
    "Get public-facing IPv6 address;"
    "Get local IPv4 address;"
    "Print this help;"
  )

  alias_help "System aliases" \
    "${aliases[*]}" "${descriptions[*]}"
}
# }}}

# Dotfiles {{{
# Edit $ZDOTDIR/local.sh
alias editlocal="v \$ZDOTDIR/local.sh"

# Edit $ZDOTDIR/exports.sh
alias editexports="v \$ZDOTDIR/exports.sh"

# Edit $ZDOTDIR/path.sh
alias editpath="v \$ZDOTDIR/path.sh"

# Dotfiles aliases help
function doh() {
  local aliases descriptions
  aliases+=(
    "editlocal"
    "editexports"
    "editpath"
    "doh"
  )
  descriptions+=(
    "Edit ZDOTDIR/local.sh;"
    "Edit ZDOTDIR/exports.sh;"
    "Edit ZDOTDIR/path.sh;"
    "Print this help;"
  )

  alias_help "Dotfiles aliases" \
    "${aliases[*]}" "${descriptions[*]}"
}
# }}}

# tmux {{{
# `brew install tmux`
if command -v tmux &> /dev/null; then
  # Alias for tmux attach-session
  alias tmas="tmux attach-session"

  # Alias for tmux kill-server
  alias tmks="tmux kill-server"

  # Tmux aliases help
  function tmh() {
    local aliases descriptions
    aliases+=(
      "tmas"
      "tmks"
      "tmh"
    )
    descriptions+=(
      "Alias for tmux attach-session;"
      "Alias for tmux kill-server;"
      "Print this help;"
    )

    alias_help "Tmux aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi
# }}}

# Homebrew {{{
# https://brew.sh/
if command -v brew &> /dev/null; then
  # Check brew health
  alias brewD="brew doctor"

  # Search for a formula
  alias brewS="brew search"

  # Install a formula
  alias brewI="brew install"

  # Reinstall a formula
  alias brewIr="brew reinstall"

  # Uninstall a formula
  alias brewU="brew uninstall"

  # Uninstall a formula with force
  alias brewUf="brew uninstall --force"

  # Uninstall a formula ignoring its dependency state
  alias brewUi="brew uninstall --ignore-dependencies"

  # Install a cask
  alias caskI="brew install --cask"

  # Reinstall a cask
  alias caskIr="brew reinstall --cask"

  # Uninstall a cask
  alias caskU="brew uninstall --cask"

  # Dump brew bundle to $HOME
  # Mnemonic: bundle dump
  alias budu="brew bundle dump --global --force --describe --all"

  # Install brew bundle from $HOME
  # Mnemonic: bundle install
  alias buin="brew bundle install --global"

  # Check whether Brewfile's dependencies are satisfied
  # Mnemonic: bundle check
  alias buch="brew bundle check --global"

  # Uninstall all dependencies not present in $HOME/Brewfile
  # Mnemonic: bundle clean
  alias bucl="brew bundle cleanup --global"

  # Homebrew aliases help
  function hbh() {
    local aliases descriptions
    aliases+=(
      "brewD"
      "brewS"
      "brewI"
      "brewIr"
      "brewU"
      "brewUf"
      "brewUi"
      "caskI"
      "caskIr"
      "caskU"
      "budu"
      "buin"
      "buch"
      "bucl"
      "hbh"
    )
    descriptions+=(
      "Check brew health;"
      "Search for a formula;"
      "Install a formula;"
      "Reinstall a formula;"
      "Uninstall a formula;"
      "Uninstall a formula with force;"
      "Uninstall a formula ignoring its dependency state;"
      "Install a cask;"
      "Reinstall a cask;"
      "Uninstall a cask;"
      "Dump brew bundle to HOME. Mnemonic: bundle dump;"
      "Install brew bundle from HOME. Mnemonic: bundle install;"
      "Check whether Brewfile's dependencies are satisfied. Mnemonic: bundle check;"
      "Uninstall all dependencies not present in the HOME Brewfile. Mnemonic: bundle clean;"
      "Print this help;"
    )

    alias_help "Homebrew aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi
# }}}

# npm {{{
# https://github.com/nvm-sh/nvm
if command -v npm &> /dev/null; then
  # Install dependencies globally
  alias npmg="npm i --location=global"

  # Install and save to dependencies in your package.json
  alias npmS="npm i -S"

  # Install and save to dev-dependencies in your package.json
  alias npmD="npm i -D"

  # Force npm to fetch remote resources even if a local copy exists on disk.
  alias npmF="npm i -f"

  # Check which npm modules are outdated
  alias npmO="npm outdated"

  # Update all the packages listed to the latest version
  alias npmU="npm update"

  # Check package versions
  alias npmV="npm -v"

  # List packages
  alias npmL="npm list"

  # List top-level installed packages
  alias npmL0="npm ls --depth=0"
  alias npmst="npm start"
  alias npmt="npm test"
  alias npmR="npm run"
  alias npmB="npm run build"
  alias npmP="npm publish"
  alias npmi="npm info"
  alias npmSe="npm search"
  alias npmun="npm uninstall"
  alias npmunD="npm uninstall -D"
  alias npmunG="npm uninstall -g"

  # npm dev
  # shellcheck disable=SC2139
  alias {npmd,dev}="npm run dev"

  # npm aliases help
  function nph() {
    local aliases descriptions
    aliases+=(
      "npmg"
      "npmS"
      "npmD"
      "npmF"
      "npmO"
      "npmU"
      "npmV"
      "npmL"
      "npmL0"
      "npmst"
      "npmt"
      "npmR"
      "npmB"
      "npmP"
      "npmi"
      "npmSe"
      "npmun"
      "npmunD"
      "npmunG"
      "npmd"
      "dev"
      "nph"
    )
    descriptions+=(
      "Install dependencies globally;"
      "Install and save to dependencies in your package.json;"
      "Install and save to dev-dependencies in your package.json;"
      "Force npm to fetch remote resources even if a local copy exists on disk.;"
      "Check which npm modules are outdated;"
      "Update all the packages listed to the latest version;"
      "Check package versions;"
      "List packages;"
      "List top-level installed packages;"
      "npm start;"
      "npm test;"
      "npm run;"
      "npm run build;"
      "npm publish;"
      "npm info;"
      "npm search;"
      "npm uninstall;"
      "npm uninstall dev;"
      "npm uninstall global;"
      "npm dev;"
      "npm dev;"
      "Print this help;"
    )

    alias_help "npm aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi
# }}}

# Git {{{
if command -v git &> /dev/null; then
  alias g="git"

  alias ga="git add"
  alias gaa="git add --all"

  alias gb="git branch"
  alias gba="git branch -a"
  alias gbd="git branch -d"
  alias gbD="git branch -D"
  alias gbl="git blame -b -w"
  alias gbnm="git branch --no-merged"
  alias gbr="git branch --remote"

  alias gc="git commit -v"
  alias gcn="git commit -v --no-verify"
  alias gc!="git commit -v --amend"
  alias gcn!="git commit -v --no-edit --amend"
  alias gca="git commit -v -a"
  alias gca!="git commit -v -a --amend"
  alias gcan!="git commit -v -a --no-edit --amend"
  alias gcans!="git commit -v -a -s --no-edit --amend"
  alias gcm="git commit -m"
  alias gcam="git commit -a -m"
  alias gcsm="git commit -s -m"
  alias gcas="git commit -a -s"
  alias gcasm="git commit -a -s -m"
  alias gcf="git config --list"
  alias gcl="git clone --recurse-submodules"
  alias gclean="git clean -id"
  alias gco="git checkout"
  alias gcob="git checkout -b"
  alias gcom="git checkout \$(git-main-branch)"
  alias gcon="git checkout next"
  alias gcot="git checkout test"
  alias gcp="git cherry-pick"
  alias gcpa="git cherry-pick --abort"
  alias gcpc="git cherry-pick --continue"
  alias gcs="git commit -S"
  alias gaac="gaa && gc"

  alias gd="git diff"
  alias gdca="git diff --cached"
  alias gdcw="git diff --cached --word-diff"
  alias gdct='git describe --tags $(g rev-list --tags --max-count=1)'
  alias gds="git diff --staged"
  alias gdt="git diff-tree --no-commit-id --name-only -r"
  alias gdw="git diff --word-diff"

  alias gf="git fetch"
  alias gfa="git fetch --all"
  alias gfo="git fetch origin"

  alias gfg="git ls-files | grep"

  alias gh="git help"

  alias gignore="git update-index --assume-unchanged"
  alias gignored='git ls-files -v | grep "^[[:lower:]]"'

  alias gl="git pull"
  alias gfal="git fetch --all && git pull"

  alias glg="git log --stat"
  alias glgp="git log --stat -p"
  alias glgg="git log --graph"
  alias glgga="git log --graph --decorate --all"
  alias glgm="git log --graph --max-count=10"
  alias glo="git log --oneline --decorate"
  alias glol="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'"

  alias glols="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --stat"
  alias glod="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'"
  alias glods="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --date=short"
  alias glola="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --all"
  alias glog="git log --oneline --decorate --graph"
  alias gloga="git log --oneline --decorate --graph --all"

  # Unpushed commits
  alias gup='git log --stat --pretty=format:"%C(008)%h %C(004)• %an, %ar %C(014)• %s " @{u}..'

  alias gm="git merge"
  alias gmom='git merge origin/$(git-main-branch)'
  alias gmt="git mergetool --no-prompt"
  alias gmtvim="git mergetool --no-prompt --tool=vimdiff"
  alias gmum='git merge upstream/$(git-main-branch)'
  alias gmc="git merge --continue"
  alias gma="git merge --abort"

  alias gp="git push"
  alias gpd="git push --dry-run"
  alias gpf="git push --force-with-lease"
  alias gpf!="git push --force"
  alias gpt="git push --tags"
  alias gppt="git push && gpt"
  alias gpoat="git push origin --all && git push origin --tags"
  alias gpod="git push origin --delete"
  alias gpu="git push upstream"
  alias gpv="git push -v"
  alias gps="git push --set-upstream origin \$(git rev-parse --abbrev-ref HEAD)"

  alias gr="git remote"
  alias gra="git remote add"
  alias grb="git rebase"
  alias grba="git rebase --abort"
  alias grbc="git rebase --continue"
  alias grbi="git rebase -i"
  alias grbm='git rebase $(git-main-branch)'
  alias grbo="git rebase --onto"
  alias grbs="git rebase --skip"
  alias grev="git revert"
  alias grh="git reset"
  alias grhh="git reset --hard"
  alias grs="git reset --soft HEAD~"
  alias grm="git rm"
  alias grmc="git rm --cached"
  alias grmv="git remote rename"
  alias grrm="git remote remove"
  alias grset="git remote set-url"
  alias grss="git restore --source"
  alias grst="git restore --staged"
  alias grt='cd "$(g rev-parse --show-toplevel || echo .)"'
  alias gru="git reset --"
  alias grup="git remote update"
  alias grv="git remote -v"

  # NOTE: this overrides ghostscript. To still access it, run \gs
  alias gs="git status"
  alias gss="git status -s"
  alias gsb="git status -sb"
  alias gsd="git svn dcommit"
  alias gsh="git show"
  alias gsi="git submodule init"
  alias gsps="git show --pretty=short --show-signature"
  alias gsr="git svn rebase"

  alias gsta="git stash save"
  alias gstaa="git stash apply"
  alias gstc="git stash clear"
  alias gstd="git stash drop"
  alias gstl="git stash list"
  alias gstp="git stash pop"
  alias gsts="git stash show --text"
  alias gstu="git stash save --include-untracked"
  alias gstall="git stash --all"
  alias gsu="git submodule update"
  alias gsw="git switch"
  alias gswc="git switch -c"

  alias gwch="git whatchanged -p --abbrev-commit --pretty=medium"
  alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "--wip-- [skip ci]"'

  # Print who's contributed to the project, excluding merges
  alias gwho="git shortlog -sn --no-merges"
  # Print who's contributed to the project, including merges
  alias gcount="git shortlog -sn"

  # Git aliases help
  function gih() {
    local aliases descriptions
    aliases+=(
      "g"
      "ga"
      "gaa"
      "gb"
      "gba"
      "gbd"
      "gbD"
      "gbl"
      "gbnm"
      "gbr"
      "gc"
      "gcn"
      "gc!"
      "gcn!"
      "gca"
      "gca!"
      "gcan!"
      "gcans!"
      "gcm"
      "gcam"
      "gcsm"
      "gcas"
      "gcasm"
      "gcf"
      "gcl"
      "gclean"
      "gco"
      "gcob"
      "gcom"
      "gcon"
      "gcot"
      "gcp"
      "gcpa"
      "gcpc"
      "gcs"
      "gaac"
      "gd"
      "gdca"
      "gdcw"
      "gdct"
      "gds"
      "gdt"
      "gdw"
      "gf"
      "gfa"
      "gfo"
      "gfg"
      "gh"
      "gignore"
      "gignored"
      "gl"
      "gfal"
      "glg"
      "glgp"
      "glgg"
      "glgga"
      "glgm"
      "glo"
      "glol"
      "glols"
      "glod"
      "glods"
      "glola"
      "glog"
      "gloga"
      "gup"
      "gm"
      "gmom"
      "gmot"
      "gmtvim"
      "gmum"
      "gmc"
      "gma"
      "gp"
      "gpd"
      "gpf"
      "gpf!"
      "gpt"
      "gppt"
      "gpoat"
      "gpod"
      "gpu"
      "gpc"
      "gps"
      "gr"
      "gra"
      "grb"
      "grba"
      "grbc"
      "grbi"
      "grbm"
      "grbo"
      "grbs"
      "grev"
      "grh"
      "grhh"
      "grs"
      "grm"
      "grmc"
      "grmv"
      "grrm"
      "grset"
      "grss"
      "grst"
      "grt"
      "gru"
      "grup"
      "grv"
      "gs"
      "gss"
      "gsb"
      "gsd"
      "gsh"
      "gsi"
      "gsps"
      "gsr"
      "gsta"
      "gstaa"
      "gstc"
      "gstd"
      "gstl"
      "gstp"
      "gsts"
      "gstu"
      "gstall"
      "gsu"
      "gsw"
      "gswc"
      "gwch"
      "gwip"
      "gwho"
      "gcount"
      "gih"
    )
    descriptions+=(
      "git;"
      "git add;"
      "git add --all;"
      "git branch;"
      "git branch -a;"
      "git branch -d;"
      "git branch -D;"
      "git blame -b -w;"
      "git branch --no-merged;"
      "git branch --remote;"
      "git commit -v;"
      "git commit -v --no-verify;"
      "git commit -v --amend;"
      "git commit -v --no-edit --amend;"
      "git commit -v -a;"
      "git commit -v -a --amend;"
      "git commit -v -a --no-edit --amend;"
      "git commit -v -a -s --no-edit --amend;"
      "git commit -m;"
      "git commit -a -m;"
      "git commit -s -m;"
      "git commit -a -s;"
      "git commit -a -s -m;"
      "git config --list;"
      "git clone --recurse-submodules;"
      "git clean -id;"
      "git checkout;"
      "git checkout -b;"
      "git checkout (git-main-branch);"
      "git checkout next;"
      "git checkout test;"
      "git cherry-pick;"
      "git cherry-pick --abort;"
      "git cherry-pick --continue;"
      "git commit -S;"
      "gaa && gc;"
      "git diff;"
      "git diff --cached;"
      "git diff --cached --word-diff;"
      "git describe --tags (g rev-list --tags --max-count=1);"
      "git diff --staged;"
      "git diff-tree --no-commit-id --name-only -r;"
      "git diff --word-diff;"
      "git fetch;"
      "git fetch --all;"
      "git fetch origin;"
      "git ls-files | grep;"
      "git help;"
      "git update-index --assume-unchanged;"
      "git ls-files -v | grep '^[[:lower:]]';"
      "git pull;"
      "git fetch --all && git pull;"
      "git log --stat;"
      "git log --stat -p;"
      "git log --graph;"
      "git log --graph --decorate --all;"
      "git log --graph --max-count=10;"
      "git log --oneline --decorate;"
      "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset';"
      "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --stat;"
      "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset';"
      "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --date=short;"
      "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --all;"
      "git log --oneline --decorate --graph;"
      "git log --oneline --decorate --graph --all;"
      "Unpushed commits;"
      "git merge;"
      "git merge origin/(git-main-branch);"
      "git mergetool --no-prompt;"
      "git mergetool --no-prompt --tool=vimdiff;"
      "git merge upstream/(git-main-branch);"
      "git merge --continue;"
      "git merge --abort;"
      "git push;"
      "git push --dry-run;"
      "git push --force-with-lease;"
      "git push --force;"
      "git push --tags;"
      "git push && gpt;"
      "git push origin --all && git push origin --tags;"
      "git push origin --delete;"
      "git push upstream;"
      "git push -v;"
      "git push --set-upstream origin (git rev-parse --abbrev-ref HEAD);"
      "git remote;"
      "git remote add;"
      "git rebase;"
      "git rebase --abort;"
      "git rebase --continue;"
      "git rebase -i;"
      "git rebase (git-main-branch);"
      "git rebase --onto;"
      "git rebase --skip;"
      "git revert;"
      "git reset;"
      "git reset --hard;"
      "git reset --soft HEAD~;"
      "git rm;"
      "git rm --cached;"
      "git remote rename;"
      "git remote remove;"
      "git remote set-url;"
      "git restore --source;"
      "git restore --staged;"
      "Navigate to repository top level directory;"
      "git reset --;"
      "git remote update;"
      "git remote -v;"
      "git status;"
      "git status -s;"
      "git status -sb;"
      "git svn dcommit;"
      "git show;"
      "git submodule init;"
      "git show --pretty=short --show-signature;"
      "git svn rebase;"
      "git stash save;"
      "git stash apply;"
      "git stash clear;"
      "git stash drop;"
      "git stash list;"
      "git stash pop;"
      "git stash show --text;"
      "git stash save --include-untracked;"
      "git stash --all;"
      "git submodule update;"
      "git switch;"
      "git switch -c;"
      "git whatchanged -p --abbrev-commit --pretty=medium;"
      "Commit WIP state;"
      "git shortlog -sn --no-merges;"
      "git shortlog -sn;"
      "Print this help;"
    )

    alias_help "Git aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi
# }}}

# Taskwarrior {{{
# `brew install taskwarrior`
if command -v task &> /dev/null; then
  # Alias for task
  alias ta="task"

  # Add a new task
  alias taa="task add"

  # Add a new task to project 'work'
  alias taaw="task add project:work"

  # Add a new task to project 'home'
  alias taah="task add project:home"

  # Add a new task to project 'fun'
  alias taaf="task add project:fun"

  # Modify an existing task
  alias tam="task modify"

  # Start progress on a task
  alias tast="task start"

  # Stop progress on a task
  alias tasp="task stop"

  # Mark a task done
  alias tad="task done"

  # Clear the context
  alias tacn="task context none"

  # Set context to 'work'
  alias tacw="task context work"

  # Set context to 'home'
  alias tach="task context home"

  # Set context to 'fun'
  alias tacf="task context fun"

  # View calendar report
  alias taca="task calendar"

  # View tasks tagged with 'pdo'
  alias pdo="task +pdo"

  # Taskwarrior aliases help
  function tah() {
    local aliases descriptions
    aliases+=(
      "ta"
      "taa"
      "taaw"
      "taah"
      "taaf"
      "tam"
      "tast"
      "tasp"
      "tad"
      "tacn"
      "tacw"
      "tach"
      "tacf"
      "taca"
      "pdo"
      "tah"
    )
    descriptions+=(
      "Alias for task;"
      "Add a new task;"
      "Add a new task to project 'work';"
      "Add a new task to project 'home';"
      "Add a new task to project 'fun';"
      "Modify an existing task;"
      "Start progress on a task;"
      "Stop progress on a task;"
      "Mark a task done;"
      "Clear the context;"
      "Set context to 'work';"
      "Set context to 'home';"
      "Set context to 'fun';"
      "View calendar report;"
      "View tasks tagged with 'pdo';"
      "Print this help;"
    )

    alias_help "Taskwarrior aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi

# Start taskwarrior-tui
# `brew install taskwarrior-tui`
if command -v taskwarrior-tui &> /dev/null; then
  alias tatui="taskwarrior-tui"
fi
# }}}

# Timewarrior {{{
# `brew install timewarrior`
if command -v timew &> /dev/null; then
  # Alias for timew
  alias ti="timew"

  # Summary
  alias tis="timew summary"

  # Report for today
  alias tid="timew day"

  # Report for current week
  alias tiw="timew week"

  # Report for current month
  alias tim="timew month"

  # Timewarrior aliases help
  function tih() {
    local aliases descriptions
    aliases+=(
      "ti"
      "tis"
      "tid"
      "tiw"
      "tim"
      "tist"
      "tisp"
      "tit"
      "tia"
      "tih"
    )
    descriptions+=(
      "Alias for timew;"
      "Summary;"
      "Report for today;"
      "Report for current week;"
      "Report for current month;"
      "Start tracking;"
      "Stop tracking;"
      "Tag interval;"
      "Annotate interval;"
      "Print this help;"
    )

    alias_help "Timewarrior aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi
# }}}

# Garbage {{{
# `brew install lynx`
if command -v lynx &> /dev/null; then
  # Local news
  alias err="lynx https://www.err.ee/#main"

  # Search DuckDuckGo
  alias '?'="duck"

  # Search Google
  alias '??'="google"

  if command -v tmux &> /dev/null; then
    # Cheat sheets
    alias vimhelp="tmux-lynxr https://vim.rtorr.com/"
    alias githelp="tmux-lynxr https://github.com/romkatv/powerlevel10k/#what-do-different-symbols-in-git-status-mean"
  fi

  # Lynx aliases help
  function lyh() {
    local aliases descriptions
    aliases+=(
      "err"
      "?"
      "??"
      "vimhelp"
      "githelp"
      "lyh"
    )
    descriptions+=(
      "Local news;"
      "Search DuckDuckGo;"
      "Search Google;"
      "Vim cheat sheet;"
      "Git cheat sheet;"
      "Print this help;"
    )

    alias_help "Lynx aliases" \
      "${aliases[*]}" "${descriptions[*]}"
  }
fi

# `brew install circumflex`
if command -v clx &> /dev/null; then
  # Hacker News
  alias hn="clx -n"
fi

# `brew install newsboat`
if command -v newsboat &> /dev/null; then
  # newsboat
  alias rss="newsboat"
fi

# `brew install hledger`
if command -v hledger &> /dev/null; then
  # hledger
  alias led="hledger"
fi

# `brew install cmatrix`
if command -v cmatrix &> /dev/null; then
  # Matrix
  alias matrix="cmatrix -b"
fi

# ASCII Star Wars
alias sw="nc towel.blinkenlights.nl 23"

# Miscellaneous aliases help
function mih() {
  local aliases descriptions
  aliases+=(
    "hn"
    "rss"
    "led"
    "matrix"
    "sw"
    "mih"
  )
  descriptions+=(
    "Hacker News;"
    "Newsboat;"
    "hledger;"
    "Matrix;"
    "ASCII Star Wars;"
    "Print this help;"
  )

  alias_help "Miscellaneous aliases" \
    "${aliases[*]}" "${descriptions[*]}"
}
# }}}

function aliashelp() {
  mah
  zshh
  uth
  lsh
  syh
  doh
  tmh
  hbh
  nph
  gih
  tah
  tih
  lyh
  mih
}
