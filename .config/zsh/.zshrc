#!/usr/bin/env zsh

# Enable Powerlevel10k instant prompt. Should stay close to the top of $ZDOTDIR/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# https://zsh.sourceforge.io/Doc/Release/Options.html#History
HISTFILE="$XDG_DATA_HOME/zsh/.zhistory"
HISTSIZE=40000 # history size in memory
SAVEHIST=20000 # history file size

# When a partial line is preserved, by default you will see an
# inverse+bold character at the end of the partial line: a "%" for
# a normal user or a "#" for root. If set, the shell parameter
# PROMPT_EOL_MARK can be used to customize how the end of partial lines
# are shown.
# https://zsh.sourceforge.io/Doc/Release/Options.html#Prompting
PROMPT_EOL_MARK=""

# Prevent duplicates in $PATH
typeset -U path

# Completions:
# https://github.com/zsh-users/zsh-completions/
# Additional completion definitions for Zsh.
fpath=("$ZDOTDIR/plugins/zsh-completions/src" $fpath)
# Custom completions
fpath=("$ZDOTDIR/completions" $fpath)

# Completion system settings:
# `man zshcompsys`
# https://zsh.sourceforge.io/Doc/Release/Zsh-Modules.html
zmodload -i zsh/complist # Load completions system, should be called before `compinit`.
# `compinit` optimization
# https://carlosbecker.com/posts/speeding-up-zsh/
autoload -Uz compinit up-line-or-beginning-search down-line-or-beginning-search \
  history-beginning-search-menu-space-end history-beginning-search-menu \
  edit-command-line
if [[ $(date +'%j') != $(/usr/bin/stat -f '%Sm' -t '%j' "${XDG_CACHE_HOME}/zsh/.zcompdump-${(%):-%m}-${ZSH_VERSION}") ]]; then
  compinit -d "${XDG_CACHE_HOME}/zsh/.zcompdump-${(%):-%m}-${ZSH_VERSION}"
  # Update the timestamp on compdump file.
  compdump
else
  compinit -C -d "${XDG_CACHE_HOME}/zsh/.zcompdump-${(%):-%m}-${ZSH_VERSION}"
fi

# Completion settings:
# https://github.com/ohmyzsh/ohmyzsh/blob/master/lib/completion.zsh
_comp_options+=globdots # Show hidden files/folders among completions.
unsetopt flow_control # Prevent forward history search from being overtaken.
setopt menu_complete # Autoselect the first completion entry.
setopt auto_menu # Show completion menu on successive tab press.
setopt complete_in_word # Complete from both ends of a word.
setopt always_to_end # Always place the cursor to the end of the completed word.
# setopt correct_all # Offer to correct commands AND arguments.
setopt correct # Offer to correct commands.
setopt interactive_comments # Enable comments on the command line.
# setopt list_packed # Display completions in compact columns.

# Completion styles:
# https://zsh.sourceforge.io/Doc/Release/Completion-System.html#index-format_002c-completion-style
# https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html#Visual-effects
# https://unix.stackexchange.com/a/613402/256869
# https://thevaluable.dev/zsh-completion-guide-examples/
# https://github.com/seebi/zshrc/blob/master/completion.zsh
# `zstyle` pattern
# :completion:<function>:<completer>:<command>:<argument>:<tag>
zstyle ':completion:*' completer _extensions _complete _approximate
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME:-$HOME/.cache}/zsh"
zstyle ':completion:*' use-cache on
zstyle ':completion:*' verbose yes # Show comments when present.
zstyle ':completion:*' menu select # Select completions from a menu.
zstyle ':completion:*' list-rows-first no # Don't display completions in rows.
zstyle ':completion:*' list-dirs-first yes # List directories first.
zstyle ':completion:*' group-name '' # Group completions.
zstyle ':completion:*:*:-command-:*:*' group-order \
  aliases functions builtins commands # The order of groups.
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}" # Colors for completion.
zstyle ':completion:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;32' # Special highlighting for kill completion.
zstyle ':completion:*:messages' format '%F{5}%B -- %d -- %f%b'
zstyle ':completion:*:warnings' format '%F{1}%B -- No Matches Found -- %f%b'
zstyle ':completion:*:descriptions' format '%F{5}%B%d:%f%b' # Completion group headers.
zstyle ':completion:*:*:*:*:corrections' format '%F{3}%B!- %d (errors: %e) -!%f%b'
zstyle ':completion:*:default' select-prompt '%F{5}%B -- Match %M %P -- %f%b' # Statusline for many hits.
# case-insensitive -> partial-word (cs) -> substring completion:
zstyle ':completion:*' matcher-list 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# History settings:
# https://github.com/rothgar/mastering-zsh/blob/master/docs/config/history.md
setopt append_history # Append to history file.
setopt extended_history # Record timestamp of command in HISTFILE.
setopt inc_append_history # Add commands to history immediately after execution.
setopt share_history # Share history between all sessions.
setopt hist_expire_dups_first # Delete duplicates first when trimming history.
setopt hist_find_no_dups # Don't return duplicates when searching history.
setopt hist_ignore_all_dups # Delete an old command if a new one duplicates it.
setopt hist_ignore_dups # Don't record a command if it was just recorded.
setopt hist_ignore_space # Ignore commands that start with space.
setopt hist_reduce_blanks # Remove superfluous blanks before recording entry.
setopt hist_save_no_dups # Don't write a duplicate command to the history file.
setopt hist_verify # Don't execute immediately upon history expansion.

# Zsh directory stack settings:
# https://zsh.sourceforge.io/Intro/intro_6.html
setopt auto_pushd # Push directories automatically to the directory stack.
setopt pushd_ignore_dups # Don't push duplicate directories.
setopt pushdminus # Swap the meaning of cd +1 and cd -1.

# Force the user to type `exit` or `logout`, instead of just pressing ^D.
# https://zsh.sourceforge.io/Intro/intro_16.html
setopt ignore_eof

# Load widgets:
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
zle -N history-beginning-search-menu-space-end history-beginning-search-menu
zle -N edit-command-line

# Keybindings:
# To see every current keybind, run `bindkey`.

# Prompt input mode.
bindkey -e
set -o emacs

# Or use vi mode.
# bindkey -v
# set -o vi

# Use `hjlk` in menu selection (during completion).
# Doesn't work well with interactive mode.
# https://github.com/Phantas0s/.dotfiles/blob/master/zsh/completion.zsh
bindkey -M menuselect 'h' vi-backward-char # Left
bindkey -M menuselect 'j' vi-down-line-or-history # Down
bindkey -M menuselect 'k' vi-up-line-or-history # Up
bindkey -M menuselect 'l' vi-forward-char # Right

bindkey -M menuselect '^xg' clear-screen # Clear screen
bindkey -M menuselect '^xi' vi-insert # Insert
bindkey -M menuselect '^xh' accept-and-hold # Hold
bindkey -M menuselect '^xn' accept-and-infer-next-history # Complete subdirectories
bindkey -M menuselect '^xu' undo # Undo

bindkey '^[[Z' reverse-menu-complete # Shift-Tab through completions.
# Enable redo and clearing to the left of cursor.
# https://stackoverflow.com/a/29403520
bindkey '^X^_' redo
bindkey "^X\\x7f" backward-kill-line

# Fuzzy find history forwards and backwards.
# https://github.com/lmintmate/zshrc#have-arrow-keys-search-history-while-typing-a-command
bindkey '^[[A' up-line-or-beginning-search # up key
bindkey '^[[B' down-line-or-beginning-search # down key
bindkey -M emacs '^P' up-line-or-beginning-search # Ctrl-P in emacs mode
bindkey -M emacs '^N' down-line-or-beginning-search # Ctrl-N in emacs mode
bindkey -M vicmd 'k' up-line-or-beginning-search # k in vi mode
bindkey -M vicmd 'j' down-line-or-beginning-search # j in vi mode

# Invoke a numbered history completion menu.
# https://github.com/lmintmate/zshrc#history-beginning-search-menu
bindkey '^H' history-beginning-search-menu-space-end

# Edit current command in `EDITOR`.
# https://github.com/lmintmate/zshrc#edit-command-line
bindkey '^X^E' edit-command-line

# Source aliases, functions, path, etc.:
# * $ZDOTDIR/aliases.sh is for aliases.
# * $ZDOTDIR/functions.sh is for functions.
# * $ZDOTDIR/path.sh is used to extend `$PATH`.
# * $ZDOTDIR/exports.sh is for environment specific exports.
# * $ZDOTDIR/local.sh is for other functionality you don’t want to commit.
for file in $ZDOTDIR/{path,exports,aliases,functions,local}.sh; do
  [[ -r "$file" ]] && [[ -f "$file" ]] && source "$file"
done
unset file

# zsh-nvm variables:
# They have to be defined before `nvm` is loaded.
export NVM_COMPLETION=true
export NVM_LAZY_LOAD=true
export NVM_AUTO_USE=true
export NVM_LAZY_LOAD_EXTRA_COMMANDS=('vim' 'nvim' 'bw')
export NVM_LAZY_AUTO_DIR=("$HOME/work")

activate_nvm() {
  # Check if `cwd` contains paths from the list,
  # and if `node` is a shell function (not loaded).
  if [[ $PWD =~ $NVM_LAZY_AUTO_DIR && "$(type node)" = *'a shell function'* ]]; then
    print 'Activating nvm...'

    # Trigger loading.
    node --version
    # `cd` into same directory to activate `auto_use`.
    cd $PWD
  fi
}

# Use this function if `LAZY_LOAD` is `true`.
if [[ ( $NVM_LAZY_LOAD ) ]]; then
  precmd_functions+=(activate_nvm)
fi

# zoxide:
# https://github.com/ajeetdsouza/zoxide
# Remembers which directories you use most frequently,
# so you can "jump" to them in just a few keystrokes.
eval "$(zoxide init zsh)"

# thefuck:
# https://github.com/nvbn/thefuck
# Corrects your previous command.
eval "$(thefuck --alias)"
eval "$(thefuck --alias f)"
eval "$(thefuck --alias pls)"

# fzf auto-completion & keybindings:
eval "$(fzf --zsh)"

# powerlevel10k:
# https://github.com/romkatv/powerlevel10k/
# Theme for Zsh prompt.
source "$ZDOTDIR/themes/powerlevel10k/powerlevel10k.zsh-theme"

# zsh-prompt-benchmark:
source "$ZDOTDIR/plugins/zsh-prompt-benchmark/zsh-prompt-benchmark.plugin.zsh"

# zsh-autosuggestions:
# https://github.com/zsh-users/zsh-autosuggestions/
# Suggests commands as you type based on history and completions.
source "$ZDOTDIR/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh"

# zsh-you-should-use:
# https://github.com/MichaelAquilina/zsh-you-should-use/
# Reminds if an alias exists for a command you just typed.
source "$ZDOTDIR/plugins/zsh-you-should-use/you-should-use.plugin.zsh"

# zsh-nvm:
# https://github.com/lukechilds/zsh-nvm/
# Zsh plugin for installing, updating and loading nvm.
source "$ZDOTDIR/plugins/zsh-nvm/zsh-nvm.plugin.zsh"

# zsh-syntax-highlighting (has to be the last sourced plugin):
# https://github.com/zsh-users/zsh-syntax-highlighting/
# Highlights commands whilst they are typed at a Zsh prompt into an interactive terminal.
# https://github.com/zsh-users/zsh-syntax-highlighting#why-must-zsh-syntax-highlightingzsh-be-sourced-at-the-end-of-the-zshrc-file
source "$ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh"

# powerlevel10k configuration:
# To customize prompt, run `p10k configure` or edit $ZDOTDIR/.p10k.zsh.
[[ ! -f "$ZDOTDIR/.p10k.zsh" ]] || source "$ZDOTDIR/.p10k.zsh"
