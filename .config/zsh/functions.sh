#!/usr/bin/env bash

# General {{{
# mkdir & cd into it
mkd() {
  mkdir -p -- "$1"
  cd -P -- "$1" || return
}

# Clone a repo and cd into the cloned folder
clonecd() {
  if [[ -n $2 ]]; then
    g clone "$1" "$2" &&
      cd "$2" || return
  else
    g clone "$1" &&
      cd "$(basename "$1" .git)" || return
  fi
}
# }}}

# softwareupdate {{{
oscheck() {
  if [[ $OSTYPE == "darwin"* ]]; then
    print-message --check "Checking for" "System Software Updates"
    softwareupdate --list
  else
    print-message --error "not in" "macOS"
  fi
}

osup() {
  if [[ $OSTYPE == "darwin"* ]]; then
    print-message --update "Installing" "System Software Updates"
    sudo softwareupdate --install --all --force --agree-to-license --restart --verbose
  else
    print-message --error "not in" "macOS"
  fi
}
# }}}

# mas {{{
# Update installed App Store apps
# `brew install mas`
appup() {
  if [[ $OSTYPE == "darwin"* ]]; then
    if command -v mas &> /dev/null; then
      print-message --update "Updating" "App Store apps"
      mas upgrade
    else
      print-message --error "Not installed:" "mas-cli"
      echo "  brew install mas"
    fi
  else
    print-message --error "not in" "macOS"
  fi
}

# Print installed App Store apps
# `brew install mas`
apps() {
  if [[ $OSTYPE == "darwin"* ]]; then
    if command -v mas &> /dev/null; then
      print-message --installed "Installed" "App Store apps"
      mas list
    else
      print-message --error "Not installed:" "mas-cli"
      echo "  brew install mas"
    fi
  else
    print-message --error "not in" "macOS"
  fi
}
# }}}

# Homebrew {{{
# Update Homebrew & installed Formulae
# https://brew.sh/
brewup() {
  if [[ $OSTYPE == "darwin"* ]]; then
    if command -v brew &> /dev/null; then
      print-message --update "Updating" "Homebrew"
      brew update
      print-message --update "Updating" "Formulae"
      brew outdated --formula &&
        brew upgrade --formula &&
        brew cleanup &&
        brew doctor
    else
      print-message --error "Not installed:" "Homebrew"
      echo '  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
    fi
  else
    print-message --error "not in" "macOS"
  fi
}

# Print Homebrew version & installed Formulae
# https://brew.sh/
brews() {
  if [[ $OSTYPE == "darwin"* ]]; then
    if command -v brew &> /dev/null; then
      print-message --installed "Installed" "Formulae"
      brew list --formulae
      print-message --installed "Installed" "Homebrew"
      brew -v
    else
      print-message --error "Not installed:" "Homebrew"
      echo '  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
    fi
  else
    print-message --error "not in" "macOS"
  fi
}

# Update installed Homebrew Casks
# https://brew.sh/
caskup() {
  if [[ $OSTYPE == "darwin"* ]]; then
    if command -v brew &> /dev/null; then
      print-message --update "Updating" "Casks"
      brew outdated --cask &&
        brew upgrade --cask &&
        brew cleanup &&
        brew doctor
    else
      print-message --error "Not installed:" "Homebrew"
      echo '  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
    fi
  else
    print-message --error "not in" "macOS"
  fi
}

# Print installed Homebrew Casks
# https://brew.sh/
casks() {
  if [[ $OSTYPE == "darwin"* ]]; then
    if command -v brew &> /dev/null; then
      print-message --installed "Installed" "Casks"
      brew list --cask
    else
      print-message --error "Not installed:" "Homebrew"
      echo '  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
    fi
  else
    print-message --error "not in" "macOS"
  fi
}
# }}}

# zsh {{{
# Update zsh plugins
zshplugup() {
  if command -v zsh &> /dev/null; then
    local Normal ICyan plugindirs=() dir
    Normal="$(tput sgr0)"
    ICyan="$(tput setaf 14)"

    while IFS= read -r -d $'\0'; do
      plugindirs+=("$REPLY")
    done < <(find "$ZDOTDIR/plugins" -maxdepth 1 -mindepth 1 -type d -print0 &&
      find "$ZDOTDIR/themes" -maxdepth 1 -mindepth 1 -type d -print0)
    local length=${#plugindirs[@]}
    local current=0

    print-message --update "Updating" "zsh plugins"
    for dir in "${plugindirs[@]}"; do
      current=$((current + 1))
      echo -e "${ICyan}$(basename "$dir")${Normal}"
      git -C "$dir" pull
      if [[ ! $current -eq $length ]]; then
        echo -e "\r"
      fi
    done
  else
    print-message --error "Not installed:" "zsh"
    echo "  brew install zsh"
  fi
}

# Print installed zsh plugins
zshplugs() {
  if command -v zsh &> /dev/null; then
    local zshplugindirs
    zshplugindirs=$(find "$ZDOTDIR/plugins" -maxdepth 1 -mindepth 1 -type d &&
      find "$ZDOTDIR/themes" -maxdepth 1 -mindepth 1 -type d)

    print-message --installed "Installed" "zsh plugins"
    echo "$zshplugindirs" | xargs basename | tr " " "\n"
  else
    print-message --error "Not installed:" "zsh"
    echo "  brew install zsh"
  fi
}
# }}}

# tmux {{{
# Update tmux plugins
# `brew install tmux`
tmuxplugup() {
  if command -v tmux &> /dev/null; then
    local Normal ICyan plugindirs=() dir
    Normal="$(tput sgr0)"
    ICyan="$(tput setaf 14)"

    while IFS= read -r -d $'\0'; do
      plugindirs+=("$REPLY")
    done < <(find "$XDG_CONFIG_HOME/tmux/plugins" -maxdepth 1 -mindepth 1 -type d -print0)
    local length=${#plugindirs[@]}
    local current=0

    print-message --update "Updating" "tmux plugins"
    # bash "$XDG_CONFIG_HOME/tmux/plugins/tpm/bin/update_plugins" all
    for dir in "${plugindirs[@]}"; do
      current=$((current + 1))
      echo -e "${ICyan}$(basename "$dir")${Normal}"
      (
        cd "$dir" || exit &&
          git pull &&
          git submodule update --init --recursive
      )
      if [[ ! $current -eq $length ]]; then
        echo -e "\r"
      fi
    done
  else
    print-message --error "Not installed:" "tmux"
    echo "  brew install tmux"
  fi
}

# Print installed tmux plugins
# `brew install tmux`
tmuxplugs() {
  if command -v tmux &> /dev/null; then
    local tmuxplugindirs
    tmuxplugindirs=$(find "$XDG_CONFIG_HOME/tmux/plugins" -maxdepth 1 -mindepth 1 -type d)

    print-message --installed "Installed" "tmux plugins"
    echo "$tmuxplugindirs" | xargs basename | tr " " "\n"
  else
    print-message --error "Not installed:" "tmux"
    echo "  brew install tmux"
  fi
}

# Clean tmux plugins
# `brew install tmux`
tmuxplugcl() {
  if command -v tmux &> /dev/null; then
    print-message --update "Cleaning" "tmux plugins"
    bash "$XDG_CONFIG_HOME/tmux/plugins/tpm/bin/clean_plugins"
  else
    print-message --error "Not installed:" "tmux"
    echo "  brew install tmux"
  fi
}
# }}}

# weechat {{{
# Update weechat plugins
wcplugup() {
  if command -v weechat &> /dev/null; then
    local Normal ICyan plugindirs=() dir
    Normal="$(tput sgr0)"
    ICyan="$(tput setaf 14)"

    while IFS= read -r -d $'\0'; do
      plugindirs+=("$REPLY")
    done < <(find "$XDG_CONFIG_HOME/weechat/scripts" -maxdepth 1 -mindepth 1 -type d -print0)
    local length=${#plugindirs[@]}
    local current=0

    print-message --update "Updating" "weechat plugins"
    for dir in "${plugindirs[@]}"; do
      current=$((current + 1))
      echo -e "${ICyan}$(basename "$dir")${Normal}"
      git -C "$dir" pull
      if [[ ! $current -eq $length ]]; then
        echo -e "\r"
      fi
    done
  else
    print-message --error "Not installed:" "weechat"
    echo "  brew install weechat"
  fi
}

# Print installed weechat plugins
wcplugs() {
  if command -v zsh &> /dev/null; then
    print-message --installed "Installed" "weechat plugins"
    find "$XDG_CONFIG_HOME/weechat/python/autoload" \
      -maxdepth 1 -mindepth 1 -type l -print0 |
      xargs -0 basename |
      sort
  else
    print-message --error "Not installed:" "weechat"
    echo "  brew install weechat"
  fi
}
# }}}

# nvm {{{
# Update nvm
# https://github.com/nvm-sh/nvm
nvmup() {
  if command -v nvm &> /dev/null; then
    print-message --update "Updating" "nvm"
    nvm upgrade
  else
    print-message --error "Not installed:" "nvm"
  fi
}

# Print nvm version
# https://github.com/nvm-sh/nvm
nvms() {
  if command -v nvm &> /dev/null; then
    print-message --installed "Installed" "nvm"
    nvm --version
  else
    print-message --error "Not installed:" "nvm"
  fi
}
# }}}

# node {{{
# Update Node.js via nvm
# https://github.com/nvm-sh/nvm
nodeup() {
  if command -v nvm &> /dev/null; then
    print-message --update "Updating" "Node.js"
    local LATEST_LTS
    local CURRENT_NODE
    LATEST_LTS="$(nvm version-remote --lts)"
    CURRENT_NODE="$(nvm current)"

    if [[ $LATEST_LTS == "$CURRENT_NODE" ]]; then
      echo -e "Already at newest LTS!\n$CURRENT_NODE"
    else
      nvm install 'lts/*' --reinstall-packages-from="$CURRENT_NODE" &&
        nvm uninstall "$CURRENT_NODE"
    fi
  else
    print-message --error "Not installed:" "nvm"
  fi
}

# Print active Node.js version
# https://github.com/nvm-sh/nvm
nodes() {
  if command -v node &> /dev/null; then
    print-message --installed "Installed" "Node.js"
    node --version
  else
    print-message --error "Not installed:" "node"
  fi
}

# Update npm & other global npm packages
npmup() {
  if command -v npm &> /dev/null; then
    print-message --update --no-newline-trailing "Updating" "global npm packages"
    npm install -g npm@latest &&
      npm update -g
  else
    print-message --error "Not installed:" "npm"
  fi
}

# Print global npm packages
# https://github.com/nvm-sh/nvm
npms() {
  if command -v npm &> /dev/null; then
    print-message --installed "Installed" "global npm packages"
    npm list --location=global -depth=0 | awk '{print $2}' | tail -n +2
  else
    print-message --error "Not installed:" "npm"
  fi
}
# }}}

# Update installed App Store apps, Homebrew Formulae, Casks,
# zsh plugins, tmux plugins, nvm, Node.js and global npm packages
update() {
  appup
  brewup
  caskup
  zshplugup
  tmuxplugup
  nvmup
  nodeup
  npmup
  oscheck
}

# Print installed App Store apps, Homebrew Formulae, Casks,
# zsh plugins, tmux plugins, nvm version, Node.js version and global npm packages
soft() {
  apps
  casks
  brews
  zshplugs
  tmuxplugs
  nvms
  nodes
  npms
}
# }}}
