# Dotfiles

> This project is intended to set any unix-like system up for development,
> although main focus is on macOS.
> Strongly encouraged to fork the project, make it your own.

[TOC]

## Features

- Installs the Xcode Command Line Tools
- Installs [Homebrew](http://brew.sh) (for installing command-line software)
- Installs [Homebrew Cask](https://github.com/caskroom/homebrew-cask) (for installing graphical software)
- Installs software from `Brewfile`, including but not limited to:
  - [Alacritty](https://github.com/alacritty/alacritty)
  - [Zsh](https://github.com/zsh-users/zsh)
  - [tmux](https://github.com/tmux/tmux/wiki)
  - [Neovim](https://github.com/neovim/neovim/wiki/Introduction)
  - [bat](https://github.com/sharkdp/bat)
  - [diff-so-fancy](https://github.com/so-fancy/diff-so-fancy)
  - [WeeChat](https://github.com/weechat/weechat)
- Sets (Homebrew) `zsh` as the default shell
- Installs [powerlevel10k](https://github.com/romkatv/powerlevel10k) and a few plugins for Zsh:
  - [zsh-prompt-benchmark](https://github.com/romkatv/zsh-prompt-benchmark)
  - [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions.git)
  - [zsh-completions](https://github.com/zsh-users/zsh-completions)
  - [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting.git)
  - [zsh-you-should-use](https://github.com/MichaelAquilina/zsh-you-should-use.git)
  - [zsh-nvm](https://github.com/lukechilds/zsh-nvm)
- Installs the following plugins for `tmux`:
  - [tpm](https://github.com/tmux-plugins/tpm)
  - [tmux-mode-indicator](https://github.com/MunifTanjim/tmux-mode-indicator)
  - [tmux-logging](https://github.com/tmux-plugins/tmux-logging)
  - [tmux-resurrect](https://github.com/tmux-plugins/tmux-resurrect)
  - [tmux-continuum](https://github.com/tmux-plugins/tmux-continuum)
  - [tmux-yank](https://github.com/tmux-plugins/tmux-yank)
  - [tmux-open](https://github.com/tmux-plugins/tmux-open)
  - [tmux-battery](https://github.com/tmux-plugins/tmux-battery)
  - [tmux-cpu](https://github.com/tmux-plugins/tmux-cpu)
  - [tmux-fzf-url](https://github.com/wfxr/tmux-fzf-url)
  - [tmux-thumbs](https://github.com/fcsonline/tmux-thumbs)
  - [tmux_super_fingers](https://github.com/artemave/tmux_super_fingers)
- Installs [lazy.nvim](https://github.com/folke/lazy.nvim) and a bunch of plugins for Neovim.
- Installs [nvm](https://github.com/nvm-sh/nvm)
- Installs Node.js LTS and a few global packages:
  - `npm@latest`
  - `neovim`
  - `npm-check-updates`
- Optionally changes your computer's name
- Changes a bunch of macOS settings

## Getting Started

1. Add yourself to sudoers.

    - 1.1. In case you're not an admin, switch to an admin user.

      ```sh
      su admin.username
      ```

    - 1.2. Add a sudoers file for your user. **NB! The file name can NOT contain periods.**

      ```sh
      sudo visudo -f "/etc/sudoers.d/yourusername"
      ```

    - 1.3. Everything that needs to be be in this file is the following line:

      ```config
      your.username ALL = (ALL) ALL
      ```

    - 1.4. Make sure to `exit` **the admin user** before continuing!

      ```sh
      exit
      ```

2. Install Xcode Command Line Tools.

    ```sh
    xcode-select --install
    ```

3. Clone this repo to `$HOME/.dotfiles`. The scripts assume this location.
    If you're the maintainer, see [For the Maintainer](#for-the-maintainer) for cloning.

    ```sh
    git clone https://gitlab.com/h0adp0re/dotfiles.git "$HOME/.dotfiles"
    ```

### For the Maintainer

1. Generate SSH key pair & change permissions for security.

    ```sh
    ssh-keygen -t ed25519
    chmod 600 ~/.ssh/*
    ```

2. Copy the contents of `id_ed25519.pub` to the clipboard (save it to Bitwarden as well).

    ```sh
    pbcopy < "$HOME/.ssh/id_ed25519.pub"
    ```

3. Add the public key to [GitLab](https://gitlab.com/-/profile/keys).

4. Clone this repo to `$HOME/.dotfiles`. The scripts assume this location.

    ```sh
    git clone --recurse-submodules git@gitlab.com:h0adp0re/dotfiles.git "$HOME/.dotfiles"
    ```

5. Add `user.name` & `user.email` to git config.

    ```sh
    cd "$HOME/.dotfiles"
    make maintainer
    ```

## Installation

### Fully Automatic Method

There are several ways to use this repo. The main use case is setting a fresh Mac up for
development, with no prior configuration. That can be accomplished with the
all-encompassing install script:

```sh
make install
```

### Semi-Automatic Method

Other use cases are to repair symlinks or only install certain software.

1. Directories

    ```sh
    make install/dirs
    ```

2. Symlinks

    ```sh
    make install/symlinks
    ```

3. Homebrew and brew bundle

    ```sh
    make install/software
    ```

4. Zsh and plugins

    ```sh
    make install/zsh
    ```

5. macOS Settings

    ```sh
    make install/macos
    ```

## Post-Install

### Additional Shell Aliases

By default, this repo includes and symlinks the following files for aliases and functions:

- `$HOME/.config/zsh/aliases.sh`
- `$HOME/.config/zsh/functions.sh`

Additionally, you can create the following files and they'll be imported:

- `$HOME/.config/zsh/path.sh` can be used to extend `$PATH`.
- `$HOME/.config/zsh/exports.sh` can be used for environment specific exports.
- `$HOME/.config/zsh/local.sh` can be used for other functionality you don’t want to commit.

### WeeChat Setup

Add some secrets for everything to start working.

```sh
# passphrase
bw get item WeeChat | jq --raw-output '.fields[0].value' | clip
# username
bw get item WeeChat | jq --raw-output '.fields[1].value' | clip
# libera server password
bw get item WeeChat | jq --raw-output '.fields[2].value' | clip
# bitlbee server password
bw get item WeeChat | jq --raw-output '.fields[3].value' | clip
```

```txt
/secure passphrase <passphrase>
/secure set username <username>
/secure set libera <libera password>
/secure set bitlbee <bitlbee password>
```

Finally, to get the passphrase from `bitwarden-cli` automatically:

```txt
/set sec.crypt.passphrase_command "bw get item WeeChat | jq --raw-output '.fields[0].value'"
```

### App settings

#### Bartender

- 🔹 General
  - Startup:
    - Launch Bartender at login: `on`
  - Activation:
    - Clicking on the menu bar will show/hide menu bar items: `on`
    - Moving the mouse into the menu bar will show hidden menu bar items: `on`
    - Delay before activating: `0.05 Seconds`
  - Bartender Bar: `off`
  - Autohide: `on`
  - Bartender menu bar icon: `Dot`
  - Bartender menu bar item:
    - Visible: `off`
    - Show Hidden item Divider: `off`
- 🔹 Menu Bar Layout
  - Shown menu bar items
    - `Focus Modes`
    - `Now Playing`
    - `Bluetooth`
    - `Sound`
    - `WiFi`
    - `Battery`
  - Hidden menu bar items
    - `Docker Desktop`
    - `Bitwarden`
    - `Balance Lock`
    - `AirBuddy`
    - `Amphetamine`
    - `Flux`
    - `Pastebot`
    - `Rectangle`
    - `Raycast`
    - `Input Sources`
  - Always Hidden menu bar items
    - `Remote management` & other work things
    - `Shortcuts`
- Hot Keys
  - Show Hidden items: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>I</kbd>
  - Quick Search menu bar items: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>U</kbd>

#### Rectangle

- Open Rectangle settings and import them from `$HOME/.dotfiles/mac/rectangle/Rectangle.json`

### Folder Actions

Open the "Folder Actions Setup" app.

- Add the Downloads folder to "Folders with Actions"
- Add the 4 file sorting actions to the Downloads folder
  - Downloads_documents.workflow
  - Downloads_image.workflow
  - Downloads_music.workflow
  - Downloads_video.workflow

### Not Easily Automated macOS Settings

#### 🍏 Apple ID

**Disable everything on a work computer, otherwise:**

- Enable iCloud Drive.
- Enable Photos.
- Enable Contacts.
- Enable Calendars.
- Enable Reminders.
- Enable Notes.
- Enable Safari.
- Enable Find My Mac.
- Enable Home.
- Disable everything else.

#### 🌐 Network

- 🔹 Firewall:
  - Enable it.
  - Automatically allow signed software.
  - Enable stealth mode.

#### 🔔 Notifications

- Notification Center:
  - Disable in each scenario.

- Application Notifications:
  - Disable Show notifications on lock screen for all apps.
  - Disable Play sounds for notifications for all apps.
  - Turn completely off for:
    - Browsers
    - Anything that's useless or that might get annoying

#### 🌙 Focus

- Share across devices: `off`

#### ⏳ Screen Time

- Share across devices: `off`

#### ⚙️ General

- 🔹 Software Update:
  - Enable automatic updates

- 🔹 Airdrop & Handoff:
  - Allow Handoff between this Mac and your iCloud devices: `off`
  - Airdrop: `Contacts Only`
  - Airplay receiver: `off`

- 🔹 Login Items:
  - `AirBuddy.app`
  - `Amphetamine.app`
  - `Balance Lock.app`
  - `Docker.app`
  - `Flux.app`
  - `Pastebot.app`
  - `Raycast.app`
  - `Rectangle.app`

- 🔹 Sharing:
  - **Disable everything on a private computer.**

#### 🎛 Control Center

- Control Center Modules:
  - Wi-Fi: `Show`
  - Bluetooth: `Show`
  - AirDrop: `Don't Show`
  - Focus: `When Active`
  - Stage Manager: `Show`
  - Screen Mirroring: `When Active`
  - Display: `When Active`
  - Sound: `Always`
  - Now Playing: `When Active`

- Other Modules:
  - Accessibility Shortcuts:
    - `Show in Control Center`
  - Battery:
    - `Show in Menu Bar`
    - `Show Percentage`
  - Music Recognition:
    - `Show in Control Center`
  - Hearing:
    - `Show in Control Center`
  - Keyboard Brightness:
    - `Show in Control Center`

- Menu Bar Only:
  - 🔹 Clock:
    - Date:
      - Show date: `Always`
      - Show the day of the week: `on`
    - Time:
      - Style: `Digital`
  - Spotlight: `Don't Show`
  - Siri: `Don't Show`
  - Time Machine: `Don't Show`
  - VPN: `Don't Show`

#### 🔍 Siri & Spotlight

- Ask Siri: `off`

#### ✋ Privacy & Security

- Privacy:
  - 🔹 Full Disk Access:
    - `Alacritty`
    - `AppCleaner`
    - `BlockBlock`
    - `Forklift`
    - `KnockKnock`
    - `Malwarebytes`
    - `OmniDiskSweeper`
    - `TaskExplorer`

  - 🔹 Accessibility:
    - `Alacritty`
    - `Amphetamine`
    - `Bartender`
    - `Pastebot`
    - `Raycast`
    - `Rectangle`
    - `Sonos`
    - `TeamViewer`
    - `VLC`
    - `/usr/bin/osascript`

  - 🔹 Input Monitoring:
    - `Pastebot`

  - 🔹 Screen & System Audio Recording:
    - `Bartender`

  - 🔹 App Management:
    - `Alacritty`

  - 🔹 Analytics & Improvements:
    - Disable everything.

  - 🔹 Apple Advertising:
    - Disable everything.

- Security:
  - Allow applications downloaded from: `App Store and identified developers`

  - 🔹 FileVault:
    - Enable FileVault and save the recovery key in a secure location (i.e. Bitwarden).

- Others:
  - 🔹 Extensions:
    - Review and disable irrelevant ones.

#### 🖥️ Desktop & Dock

- Widgets:
  - Default web browser: `Firefox` (`Firefox Developer Edition` for work computers)

#### 🔆 Displays

- Resolution:
  - Standard displays: `Scaled — Default`
  - Notched displays:
    Right click → "Show List" → "Show all resolutions" → `1728 × 1080`
- Automatically adjust brightness: `off`
- True Tone: `off`
- Refresh rate: `60 Hertz`

#### 🔋 Battery

- Low Power Mode: `Only on Battery`

#### 🔒 Lock Screen

- Require password after screen saver begins or display is turned off: Immediately.
- Show message when locked. Example: `<url> | <email> | <phone>`.

#### 🔐 Touch ID & Password

- Add Fingerprint(s)
- Use TouchID for everything: `on`

#### 👥 Users & Groups

- Update avatar.
- Disable guest account.

#### 📧 Internet Accounts

- Add account(s) for work email & calendars.

#### ⌨️ Keyboard

- Keyboard Shortcuts:
  - Launchpad & Dock: everything `off`
  - Mission Control
    - Mission Control: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>K</kbd>
    - Show Notification Center: <kbd>^</kbd> + <kbd>Space</kbd>
    - Turn Do Not Disturb on/off: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>O</kbd>
    - Application Windows: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>J</kbd>
    - Show Desktop: <kbd>F11</kbd>
    - Mission Control - Move left a space: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>H</kbd>
    - Mission Control - Move right a space: <kbd>^</kbd> + <kbd>⌘</kbd> + <kbd>L</kbd>
    - Mission Control - Switch to Desktop 1: <kbd>^</kbd> + <kbd>1</kbd>
    - Mission Control - Switch to Desktop 2: <kbd>^</kbd> + <kbd>2</kbd>
  - Keyboard: `on`
  - Input Sources: `off`
  - Services
    - Files and Folders
      - Encrypt with DigiDoc: `on`
      - Sign with DigiDoc: `on`
  - Accessibility: `off`
  - Modifier Keys
    - Caps Lock Key: `Escape` (for all keyboards)

#### 🖱 Trackpad

- More Gestures:
  - Disable "Notification Center"

## Keybindings

### macOS

It's good to remember some of macOS [sleep, log out, and shut down shortcuts](https://support.apple.com/en-us/HT201236#sleep)
and others from that page.

### Tmux

| Keys (case sensitive)                           | Action                                                                     |
|-------------------------------------------------|----------------------------------------------------------------------------|
| <kbd>Ctrl</kbd> + <kbd>s</kbd>                  | [Prefix](https://github.com/tmux/tmux/wiki/Getting-Started#the-prefix-key) |
| <kbd>Prefix</kbd> + <kbd>r</kbd>                | Reload tmux config                                                         |
| <kbd>Prefix</kbd> + <kbd>n</kbd>                | Utility menu                                                               |
| <kbd>Prefix</kbd> + <kbd>m</kbd>                | Move menu                                                                  |
| <kbd>Prefix</kbd> + <kbd>C-k</kbd>              | Previous session                                                           |
| <kbd>Prefix</kbd> + <kbd>C-j</kbd>              | Next session                                                               |
| <kbd>Prefix</kbd> + <kbd>C-o</kbd>              | Alternate session                                                          |
| <kbd>Prefix</kbd> + <kbd>C-x</kbd>              | Kill session                                                               |
| <kbd>Prefix</kbd> + <kbd>C-h</kbd>              | Previous window                                                            |
| <kbd>Prefix</kbd> + <kbd>C-l</kbd>              | Next window                                                                |
| <kbd>Prefix</kbd> + <kbd>c</kbd>                | New window                                                                 |
| <kbd>Prefix</kbd> + <kbd>⏎</kbd>                | Move window                                                                |
| <kbd>Prefix</kbd> + <kbd>X</kbd>                | Kill window                                                                |
| <kbd>Prefix</kbd> + <kbd>&vert;</kbd>           | Split pane horizontally                                                    |
| <kbd>Prefix</kbd> + <kbd>_</kbd>                | Split pane vertically                                                      |
| <kbd>Prefix</kbd> + <kbd>h</kbd>                | Select the pane to the left                                                |
| <kbd>Prefix</kbd> + <kbd>j</kbd>                | Select the pane to the bottom                                              |
| <kbd>Prefix</kbd> + <kbd>k</kbd>                | Select the pane to the top                                                 |
| <kbd>Prefix</kbd> + <kbd>l</kbd>                | Select the pane to the right                                               |
| <kbd>Prefix</kbd> + <kbd>H</kbd>                | Resize pane left                                                           |
| <kbd>Prefix</kbd> + <kbd>J</kbd>                | Resize pane bottom                                                         |
| <kbd>Prefix</kbd> + <kbd>K</kbd>                | Resize pane top                                                            |
| <kbd>Prefix</kbd> + <kbd>L</kbd>                | Resize pane right                                                          |
| <kbd>Prefix</kbd> + <kbd>M</kbd>                | Mark pane                                                                  |
| <kbd>Prefix</kbd> + <kbd>x</kbd>                | Kill pane                                                                  |
| <kbd>Prefix</kbd> + <kbd>0</kbd>                | Toggle status bar                                                          |
| <kbd>Prefix</kbd> + <kbd>C-v</kbd>              | Copy mode                                                                  |
| <kbd>v</kbd> (in copy mode)                     | Start selection                                                            |
| <kbd>Shift</kbd> + <kbd>v</kbd> (in copy mode)  | Start line selection                                                       |
| <kbd>Ctrl</kbd> + <kbd>v</kbd> (in copy mode)   | Toggle between selection and block-selection                               |
| <kbd>y</kbd> (in copy mode)                     | Copy selection to clipboard (`tmux-yank`)                                  |
| <kbd>Shift</kbd> + <kbd>y</kbd> (in copy mode)  | Copy selection to clipboard and paste it to the command line (`tmux-yank`) |
| <kbd>Shift</kbd> + <kbd>s</kbd> (in copy mode)  | Search selection in DuckDuckGo                                             |
| <kbd>Prefix</kbd> + <kbd>F</kbd>                | Open URL & path picker (`tmux-thumbs`)                                     |
| <kbd>Prefix</kbd> + <kbd>u</kbd>                | Open URL opening fuzzyfinder (`tmux-fzf-url`)                              |
| <kbd>Prefix</kbd> + <kbd>e</kbd>                | Search duckduckgo in a popup                                               |
| <kbd>Prefix</kbd> + <kbd>f</kbd>                | Open a project switching fuzzyfinder in a popup                            |
| <kbd>Prefix</kbd> + <kbd>g</kbd>                | Open a tmuxinator project switching fuzzyfinder in a popup                 |
| <kbd>Prefix</kbd> + <kbd>b</kbd>                | Open a default browser switching fuzzyfinder in a popup                    |
| <kbd>Prefix</kbd> + <kbd>i</kbd>                | Query cht.sh for the selected language + query in a popup                  |
| <kbd>Prefix</kbd> + <kbd>o</kbd>                | Open a URL from the clipboard in lynx in a popup                           |
| <kbd>Prefix</kbd> + <kbd>p</kbd>                | Open a wallpaper switching fuzzyfinder in a popup                          |
| <kbd>Prefix</kbd> + <kbd>P</kbd>                | Toggle (start/stop) logging in the current pane (`tmux-logging`)           |
| <kbd>Prefix</kbd> + <kbd>⌥</kbd> + <kbd>p</kbd> | Save visible text, in the current pane (`tmux-logging`)                    |
| <kbd>Prefix</kbd> + <kbd>⌥</kbd> + <kbd>P</kbd> | Save complete pane history to a file (`tmux-logging`)                      |
| <kbd>Prefix</kbd> + <kbd>⌥</kbd> + <kbd>c</kbd> | Clear current pane history (`tmux-logging`)                                |
| <kbd>Prefix</kbd> + <kbd>R</kbd>                | Restore tmux state (`tmux-resurrect`)                                      |
| <kbd>Prefix</kbd> + <kbd>S</kbd>                | Save tmux state (`tmux-resurrect`)                                         |
| <kbd>Prefix</kbd> + <kbd>T</kbd>                | Open TODO.md in a pane to the left                                         |
| <kbd>Prefix</kbd> + <kbd>y</kbd>                | Copy command line to clipboard (`tmux-yank`)                               |
| <kbd>Prefix</kbd> + <kbd>Y</kbd>                | Open files in an open nvim pane (`tmux_super_fingers`)                     |
| <kbd>Prefix</kbd> + <kbd>+</kbd>                | Link weechat window from dotfiles into #3                                  |
| <kbd>Prefix</kbd> + <kbd>-</kbd>                | Unlink weechat window                                                      |
| <kbd>Prefix</kbd> + <kbd>a</kbd>                | Start pomodoro timer with 2 intervals & timewarrior tracking               |
| <kbd>Prefix</kbd> + <kbd>A</kbd>                | Stop pomodoro timer & timewarrior tracking                                 |

### Vim

| Normal mode (case-sensitive)                     | Action                                                   |
|--------------------------------------------------|----------------------------------------------------------|
| <kbd>Space</kbd>                                 | Leader key                                               |
| <kbd>Leader</kbd> + <kbd>bd</kbd>                | Delete current buffer                                    |
| <kbd>Leader</kbd> + <kbd>bb</kbd>                | Go to previous buffer                                    |
| <kbd>Leader</kbd> + <kbd>bx</kbd>                | Explore buffers                                          |
| <kbd>Leader</kbd> + <kbd>rr</kbd>                | Replace the last searched text with the last yanked text |
| <kbd>Leader</kbd> + <kbd>rw</kbd>                | Search & replace word under cursor                       |
| <kbd>Leader</kbd> + <kbd>j</kbd>                 | Remove search highlighting                               |
| <kbd>Leader</kbd> + <kbd>a</kbd>                 | Code actions                                             |
| <kbd>Leader</kbd> + <kbd>zf</kbd>                | Fold to matching pair                                    |
| <kbd>Leader</kbd> + <kbd>yh</kbd>                | View yank history                                        |
| <kbd>Leader</kbd> + <kbd>yc</kbd>                | Clear yank history                                       |
| <kbd>Leader</kbd> + <kbd>t</kbd>                 | Open a terminal in a vertical split                      |
| <kbd>Leader</kbd> + <kbd>fs</kbd>                | Fuzzyfind symbols                                        |
| <kbd>Leader</kbd> + <kbd>fc</kbd>                | Command palette                                          |
| <kbd>Leader</kbd> + <kbd>ff</kbd>                | Find files in the project                                |
| <kbd>Leader</kbd> + <kbd>fh</kbd>                | Search help tags                                         |
| <kbd>Leader</kbd> + <kbd>fp</kbd>                | Search the project                                       |
| <kbd>Leader</kbd> + <kbd>fl</kbd>                | Search lines in current buffer                           |
| <kbd>Leader</kbd> + <kbd>fb</kbd>                | Fuzzyfind open buffers                                   |
| <kbd>Leader</kbd> + <kbd>gc</kbd>                | Commits for current file (neovim only)                   |
| <kbd>Leader</kbd> + <kbd>gs</kbd>                | Fuzzyfindable git status (neovim only)                   |
| <kbd>Leader</kbd> + <kbd>fh</kbd>                | Fuzzyfind help tags                                      |
| <kbd>Leader</kbd> + <kbd>dj</kbd>                | Next diagnostic error                                    |
| <kbd>Leader</kbd> + <kbd>dk</kbd>                | Previous diagnostic error                                |
| <kbd>Leader</kbd> + <kbd>dd</kbd>                | Diagnostic details                                       |
| <kbd>Leader</kbd> + <kbd>dt</kbd>                | Toggle diagnostics                                       |
| <kbd>Leader</kbd> + <kbd>en</kbd>                | Open file explorer (nnn)                                 |
| <kbd>Leader</kbd> + <kbd>er</kbd>                | Open file explorer (ranger)                              |
| <kbd>Leader</kbd> + <kbd>vs</kbd>                | Reload vimrc                                             |
| <kbd>Leader</kbd> + <kbd>vf</kbd>                | Fuzzyfindable dotfiles (neovim only)                     |
| <kbd>Leader</kbd> + <kbd>xs</kbd>                | Set tmux theme from current vim theme                    |
| <kbd>Leader</kbd> + <kbd>xg</kbd>                | Generate tmux theme from currently set styles            |
| <kbd>Leader</kbd> + <kbd>gB</kbd>                | Git blame                                                |
| <kbd>Leader</kbd> + <kbd>gb</kbd>                | Git branches                                             |
| <kbd>Leader</kbd> + <kbd>gg</kbd>                | Git status window                                        |
| <kbd>Leader</kbd> + <kbd>gl</kbd>                | Git pull                                                 |
| <kbd>Leader</kbd> + <kbd>gL</kbd>                | Git log for current file                                 |
| <kbd>Leader</kbd> + <kbd>gp</kbd>                | Git push                                                 |
| <kbd>Leader</kbd> + <kbd>gw</kbd> (in diff mode) | Write and stage file                                     |
| <kbd>Ctrl</kbd> + <kbd>j</kbd> (in diff mode)    | Jump to next hunk                                        |
| <kbd>Ctrl</kbd> + <kbd>k</kbd> (in diff mode)    | Jump to previous hunk                                    |
| <kbd>Ctrl</kbd> + <kbd>h</kbd> (in diff mode)    | Get diff from left side                                  |
| <kbd>Ctrl</kbd> + <kbd>l</kbd> (in diff mode)    | Get diff from right side                                 |
| <kbd>Leader</kbd> + <kbd>hj</kbd>                | Jump to next hunk                                        |
| <kbd>Leader</kbd> + <kbd>hk</kbd>                | Jump to previous hunk                                    |
| <kbd>Leader</kbd> + <kbd>hs</kbd>                | Stage hunk                                               |
| <kbd>Leader</kbd> + <kbd>hp</kbd>                | Preview hunk                                             |
| <kbd>Leader</kbd> + <kbd>hz</kbd>                | Fold all unchanged lines                                 |
| <kbd>Leader</kbd> + <kbd>u</kbd>                 | Toggle undotree                                          |
| <kbd>Leader</kbd> + <kbd>c</kbd>                 | Toggle comment                                           |
| <kbd>Leader</kbd> + <kbd>mj</kbd>                | Join a block into a single-line statement                |
| <kbd>Leader</kbd> + <kbd>mk</kbd>                | Split a one-liner into multiple lines                    |
| <kbd>Leader</kbd> + <kbd>mh</kbd>                | Move list item left                                      |
| <kbd>Leader</kbd> + <kbd>ml</kbd>                | Move list item right                                     |
| <kbd>Leader</kbd> + <kbd>nh</kbd>                | Move cursor left by one list item                        |
| <kbd>Leader</kbd> + <kbd>nl</kbd>                | Move cursor right by one list item                       |
| <kbd>Leader</kbd> + <kbd>ii</kbd>                | Insert before current list item                          |
| <kbd>Leader</kbd> + <kbd>ia</kbd>                | Insert after current list item                           |
| <kbd>Leader</kbd> + <kbd>iI</kbd>                | Insert as first list item                                |
| <kbd>Leader</kbd> + <kbd>iA</kbd>                | Insert as last list item                                 |
| <kbd>Leader</kbd> + <kbd>pp</kbd>                | Save current buffer to harpoon                           |
| <kbd>Leader</kbd> + <kbd>pm</kbd>                | View saved buffers                                       |
| <kbd>Leader</kbd> + <kbd>pj</kbd>                | Move to next saved buffer                                |
| <kbd>Leader</kbd> + <kbd>pk</kbd>                | Move to previous saved buffer                            |
| <kbd>Leader</kbd> + <kbd>pa</kbd>                | Jump to saved buffer 1                                   |
| <kbd>Leader</kbd> + <kbd>ps</kbd>                | Jump to saved buffer 2                                   |
| <kbd>Leader</kbd> + <kbd>pd</kbd>                | Jump to saved buffer 3                                   |
| <kbd>Leader</kbd> + <kbd>pf</kbd>                | Jump to saved buffer 4                                   |
| <kbd>Leader</kbd> + <kbd>pq</kbd>                | Jump to saved buffer 5                                   |
| <kbd>Leader</kbd> + <kbd>pw</kbd>                | Jump to saved buffer 6                                   |
| <kbd>Leader</kbd> + <kbd>pe</kbd>                | Jump to saved buffer 7                                   |
| <kbd>Leader</kbd> + <kbd>pr</kbd>                | Jump to saved buffer 8                                   |
| <kbd>Y</kbd>                                     | Yank till the end of the line                            |
| <kbd>gd</kbd>                                    | Go to definition                                         |
| <kbd>Leader</kbd> + <kbd>?</kbd>                 | Display a tip                                            |
| <kbd>Ctrl</kbd> + <kbd>k</kbd> (in command mode) | Navigate command history up                              |
| <kbd>Ctrl</kbd> + <kbd>j</kbd> (in command mode) | Navigate command history down                            |
| <kbd>Ctrl</kbd> + <kbd>q</kbd>                   | Toggle quickfix window                                   |
| <kbd>Ctrl</kbd> + <kbd>k</kbd> (in qf list)      | Previous entry in the quickfix list                      |
| <kbd>Ctrl</kbd> + <kbd>j</kbd> (in qf list)      | Next entry in the quickfix list                          |
| <kbd>dd</kbd> (in qf list)                       | Delete an entry from the quickfix list                   |
| <kbd>d</kbd> (in qf list visual mode)            | Delete selected entries from the quickfix list           |
| <kbd>u</kbd> (in qf list)                        | Restore the quickfix list to its original state          |

#### Telescope Search Syntax

Since we're using [telescope-fzf-native.nvim](https://github.com/nvim-telescope/telescope-fzf-native.nvim),
we can use fzf syntax in telescope.

| Token     | Match type                 | Description                          |
|-----------|----------------------------|--------------------------------------|
| `sbtrkt`  | fuzzy-match                | Items that match `sbtrkt`            |
| `'wild`   | exact-match (quoted)       | Items that include `wild`            |
| `^music`  | prefix-exact-match         | Items that start with `music`        |
| `.mp3$`   | suffix-exact-match         | Items that end with `.mp3`           |
| `!fire`   | inverse-exact-match        | Items that do not include `fire`     |
| `!^music` | inverse-prefix-exact-match | Items that do not start with `music` |
| `!.mp3$`  | inverse-suffix-exact-match | Items that do not end with `.mp3`    |

A single bar character term acts as an OR operator. For example, the following
query matches entries that start with `core` and end with either `go`, `rb`,
or `py`.

```null
^core go$ | rb$ | py$
```

## Tips

### No Xcode or CLT version detected!

```sh
xcode-clt-reinstall
```

### Error: Failure while executing cp

```sh
sudo chown -R $(whoami) $(brew --prefix)/*
```

### Error: Fetching /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core failed!

```sh
brew update-reset
```

### Warning: Some installed kegs have no formulae!

> This means they were either deleted or installed manually.
> You should find replacements for the following formulae:

```sh
rm -rf /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core
brew tap homebrew/core
```

### Error: Cask definition is invalid: invalid 'depends_on macos' value

```sh
/usr/bin/find "$(brew --prefix)/Caskroom/"*'/.metadata' -type f -name '*.rb' -print0 | /usr/bin/xargs -0 /usr/bin/perl -i -pe 's/depends_on macos: \[.*?\]//gsm;s/depends_on macos: .*//g'
```

[Source](https://github.com/Homebrew/homebrew-cask/issues/58046)

### Apple Watch doesn't unlock Mac or apps

1. Open "Keychain Access"
2. "View" => "Show Invisible Items"
3. Search for `auto unlock`, `auto-unlock` and `autounlock`
4. Delete all records for each search (this will reset/disable auto unlock on other Macs if you use multiple Macs)
5. Open "Finder" and navigate to `~/Library/Sharing/AutoUnlock`
6. Delete `ltk.plist` and `pairing-records.plist` (Some users have reported better success restarting macOS at this stage)
7. Open "System Preferences" and try enabling auto unlock. You may need to enable it twice, the first attempt will fail.

### What Do the Symbols in the Prompt's Git Status Mean?

Since we're using [powerlevel10k](https://github.com/romkatv/powerlevel10k), [the answer](https://github.com/romkatv/powerlevel10k/#what-do-different-symbols-in-git-status-mean) can be found in their repo.

## To Do

- [ ] Find a replacement for `caffeinate`, it hasn't worked on macOS since at least 10.5.
- [ ] Catch errors.
- [ ] Automate as many macOS settings as possible.
- [ ] Make code even more functional.
- [ ] Use something like [yadm](https://yadm.io/docs/alternates#) to have different config files on different operating systems (maybe).
- [x] Make WeeChat get secrets from `bw-cli`.
- [x] Migrate to a Lua config for Neovim.
- [x] Document/automate installing `autosort.py`, `colorize_nicks.py`, `go.py` and [`vimode.py`](https://github.com/GermainZ/weechat-vimode).
- [x] Use `tput` instead of specific escape codes.
- [x] Make the code more functional.
- [x] Back up contents of `$HOME/Library/Workflows/Applications/Folder\ Actions`.
- [x] List installed `oh-my-zsh` plugins. Depends on [this issue](https://github.com/ohmyzsh/ohmyzsh/issues/9087).
- [x] Use hooks to load Vim plugins only as necessary.
- [x] Make system parameters turn red at a certain threshold on the `tmux` status bar.

---

Sources & Inspiration:

- https://www.taniarascia.com/setting-up-a-brand-new-mac-for-development/
- https://pawelgrzybek.com/change-macos-user-preferences-via-command-line/
- https://github.com/mathiasbynens/dotfiles
- https://github.com/pawelgrzybek/dotfiles
- https://github.com/thoughtbot/dotfiles
- https://github.com/MikeMcQuaid/strap
- https://gist.github.com/vsimon/485a2d4bd493881f6c68
- https://freshman.tech/vim-javascript/
- https://www.sainnhe.dev/post/status-line-config/
- https://kimpers.com/vim-intelligent-autocompletion/
- https://github.com/ThePrimeagen/init.lua
- https://htr3n.github.io/2018/07/faster-zsh/
- https://www.cs.ru.nl/~gdal/dotfiles/.tmux.conf
- https://github.com/rwxrob/dot
