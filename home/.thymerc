timer: 1500                                # 25 minutes per pomodoro (in seconds)
timer_break: 300                           # 5 minutes per break (in seconds)
timer_warning: 300                         # show warning color at 5 minutes left (in seconds)
repeat: 2                                  # set default for -r flag, otherwise repeat indefinitely
color_default: "#{TMUX_STAT_COLOR_STRESS}" # set default timer color for tmux
color_warning: "#{TMUX_STAT_COLOR_MEDIUM}" # set warning color for tmux, set to "default" to disable
color_break: "#{TMUX_STAT_COLOR_LOW}"      # set break color for tmux
status_override: false                     # don't let thyme set tmux's status-right/left/interval
hooks:
  timewarrior_start:
    events: ["before"]
    command: "
      tist --ticket &&
      sleep 1 && tmux refresh-client -S
      "
  timewarrior_stop:
    events: ["after"]
    command: "
      timew stop &> /dev/null &&
      sleep 1 && tmux refresh-client -S
      "
  notify_before_break:
    events: ["after"]
    command: "macos-notify 'Pomodoro' 'Time for a break #{repeat_suffix}' 'Bottle'"
  notify_after_break:
    events: ["after_break"]
    command: "macos-notify 'Pomodoro' 'Break finished #{repeat_suffix}' 'Bottle'"
options:
  begin:
    flag: "-b"
    flag_long: "--begin"
    description: "Begin pomodoro & time tracking"
    command: "
      thyme -r &&
      sleep 1 && tmux refresh-client -S
      "
  end:
    flag: "-e"
    flag_long: "--end"
    description: "End pomodoro & time tracking"
    command: "
      thyme -s &&
      timew stop &> /dev/null &&
      sleep 1 && tmux refresh-client -S
      "
